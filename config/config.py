from taipy import Config
from algos import readZiso, convertGeVtoMeV, readZisoStat
from algos.zmass import readZmass, readZmassStat
from algos.correlation import decorrelateStat, decorrelateSyst, undecorrelateStat
from algos.consistent import consistentBinning
from algos.rebinLowPt import rebinlowpt
from algos.combineZmethods import combineZmethods, combineHera
from algos.compPlots import comparisonPlots_corruncorr, comparisonPlots
from algos.jpsi import readJpsi
from algos.pathFinder import pathFinder, pathFinderPrebin
from algos.chi2 import chi2

# Configuration of Data Nodes
#############################
ZisoPath_cfg = Config.configure_data_node(
    "ZisoPath", default_data="/eos/user/s/subataju/MERGE/run49-rel22/scalefactors.root"
)
ZmassPath_cfg = Config.configure_data_node(
    "ZmassPath",
    default_data="/afs/cern.ch/user/c/cbilling/tNp/efficiency-combination/filip_inputs/ZMass_all.root",
)
JpsiPath_cfg = Config.configure_data_node(
    "JpsiPath",
    default_data="/afs/cern.ch/user/c/cbilling/tNp/efficiency-combination/tp-scalefactor-taucut-eff_taucut/Jpsi_all.root",
)

workingPointID_cfg = Config.configure_data_node(
    "WorkingPointID", default_data="TightLLH_d0z0"
)

ZisoMethodName_cfg = Config.configure_data_node("ZisoMethodName", default_data="Ziso")
ZmassMethodName_cfg = Config.configure_data_node(
    "ZmassMethodName", default_data="Zmass"
)

PlotSaving_cfg = Config.configure_data_node(
    "PlotsLocation",
    default_data="plotting",
)

RebinName_cfg = Config.configure_data_node("RebinName", default_data="rebin")
LoName_cfg = Config.configure_data_node("LoName", default_data="lo")
HiName_cfg = Config.configure_data_node("HiName", default_data="hi")
AllName_cfg = Config.configure_data_node("AllName", default_data="All")

RebinPlotLoc_cfg = Config.configure_data_node("RebinPlotLoc")
LoPlotLoc_cfg = Config.configure_data_node("LoPlotLoc")
HiPlotLoc_cfg = Config.configure_data_node("HiPlotLoc")
AllPlotLoc_cfg = Config.configure_data_node("AllPlotLoc")

RebinMethod_cfg = Config.configure_data_node("RebinMethod", default_data="by_hand")

ZcombPlotLabels_cfg = Config.configure_data_node(
    "ZcombPlotLabels", default_data="Z combined,Z iso,Z mass"
)


AllcombPlotLabels_cfg = Config.configure_data_node(
    "AllcombPlotLabels", default_data="Z+J/#psi combined,Z combined,J/#psi"
)

year_cfg = Config.configure_data_node("Year", default_data=2018)
ExtraBin_cfg = Config.configure_data_node("ExtraBin", default_data="no_extraBin")
PreRebinned_cfg = Config.configure_data_node(
    "PreRebinned", default_data="no_preRebinned"
)
# make into np
ZisoSFstatsyst_cfg = Config.configure_pickle_data_node(
    "Ziso",
)
Zisox_cfg = Config.configure_pickle_data_node(
    "Ziso_x",
)
Zisoy_cfg = Config.configure_pickle_data_node(
    "Ziso_y",
)

ZisoxMeV_cfg = Config.configure_pickle_data_node(
    "Ziso_xMeV",
)

ZisoStatUncorrCorr_cfg = Config.configure_pickle_data_node(
    "ZisoStatUncorrCorr",
)

ZisoDecorr_cfg = Config.configure_pickle_data_node(
    "ZisoDecorr",
)

ZisoSystDecorr_cfg = Config.configure_pickle_data_node(
    "ZisoSystDecorr",
)
# make into np
ZmassSFstatsyst_cfg = Config.configure_pickle_data_node(
    "Zmass",
)
Zmassx_cfg = Config.configure_pickle_data_node(
    "Zmass_x",
)
Zmassy_cfg = Config.configure_pickle_data_node(
    "Zmass_y",
)

ZmassxMeV_cfg = Config.configure_pickle_data_node(
    "Zmass_xMeV",
)

ZmassStatUncorrCorr_cfg = Config.configure_pickle_data_node(
    "ZmassStatUncorrCorr",
)

ZmassDecorr_cfg = Config.configure_pickle_data_node(
    "ZmassDecorr",
)

ZmassSystDecorr_cfg = Config.configure_pickle_data_node(
    "ZmassSystDecorr",
)
ZcombSystDecorr_cfg = Config.configure_pickle_data_node(
    "ZcombSystDecorr",
)
JpsiSystDecorr_cfg = Config.configure_pickle_data_node(
    "JpsiSystDecorr",
)

Zcombx_cfg = Config.configure_pickle_data_node(
    "Zcomb_x",
)
Zcomby_cfg = Config.configure_pickle_data_node(
    "Zcomb_y",
)

ZisoRebin_cfg = Config.configure_pickle_data_node(
    "ZisoRebin",
)
Zisolo_cfg = Config.configure_pickle_data_node(
    "Zisolo",
)
Zisohi_cfg = Config.configure_pickle_data_node(
    "Zisohi",
)
ZisoRebinx_cfg = Config.configure_pickle_data_node(
    "ZisoRebinx",
)
ZisoRebiny_cfg = Config.configure_pickle_data_node(
    "ZisoRebiny",
)
Zisolox_cfg = Config.configure_pickle_data_node(
    "Zisolox",
)
Zisoloy_cfg = Config.configure_pickle_data_node(
    "Zisoloy",
)
Zisohix_cfg = Config.configure_pickle_data_node(
    "Zisohix",
)
Zisohiy_cfg = Config.configure_pickle_data_node(
    "Zisohiy",
)

ZmassRebin_cfg = Config.configure_pickle_data_node(
    "ZmassRebin",
)
Zmasslo_cfg = Config.configure_pickle_data_node(
    "Zmasslo",
)
Zmasshi_cfg = Config.configure_pickle_data_node(
    "Zmasshi",
)
ZmassRebinx_cfg = Config.configure_pickle_data_node(
    "ZmassRebinx",
)
ZmassRebiny_cfg = Config.configure_pickle_data_node(
    "ZmassRebiny",
)
Zmasslox_cfg = Config.configure_pickle_data_node(
    "Zmasslox",
)
Zmassloy_cfg = Config.configure_pickle_data_node(
    "Zmassloy",
)
Zmasshix_cfg = Config.configure_pickle_data_node(
    "Zmasshix",
)
Zmasshiy_cfg = Config.configure_pickle_data_node(
    "Zmasshiy",
)

ZcombRebin_cfg = Config.configure_pickle_data_node(
    "ZcombRebin",
)
Zcomblo_cfg = Config.configure_pickle_data_node(
    "Zcomblo",
)
Zcombhi_cfg = Config.configure_pickle_data_node(
    "Zcombhi",
)

ZJpsicomb_cfg = Config.configure_pickle_data_node(
    "ZJpsicomb",
)

JpsiSFstatsyst_cfg = Config.configure_pickle_data_node(
    "Jpsi",
)
Jpsix_cfg = Config.configure_pickle_data_node(
    "Jpsi_x",
)
Jpsiy_cfg = Config.configure_pickle_data_node(
    "Jpsi_y",
)

JpsixMeV_cfg = Config.configure_pickle_data_node(
    "Jpsi_xMeV",
)

ChiSquaredScore_cfg = Config.configure_data_node("ChiSquaredScore")
ChiSquaredScoreEta_cfg = Config.configure_data_node("ChiSquaredScoreEta")

# Configuration of tasks
##########################
pathFinder_cfg = Config.configure_task(
    "SetPaths",
    pathFinder,
    year_cfg,
    [ZisoPath_cfg, ZmassPath_cfg, JpsiPath_cfg],
)

readZiso_cfg = Config.configure_task(
    "readZiso",
    readZiso,
    [ZisoPath_cfg, workingPointID_cfg, year_cfg],
    [ZisoSFstatsyst_cfg, Zisox_cfg, Zisoy_cfg],
    skippable=True,
)

convertGeVtoMeVZiso_cfg = Config.configure_task(
    "convertGeVtoMeVZiso", convertGeVtoMeV, Zisox_cfg, ZisoxMeV_cfg, skippable=True
)

readZisoStat_cfg = Config.configure_task(
    "readZisoStat",
    readZisoStat,
    [ZisoPath_cfg, workingPointID_cfg],
    ZisoStatUncorrCorr_cfg,
    skippable=True,
)

decorrelateStatZiso_cfg = Config.configure_task(
    "decorrelateStatZiso",
    decorrelateStat,
    [ZisoSFstatsyst_cfg, ZisoStatUncorrCorr_cfg, ZisoxMeV_cfg, Zisoy_cfg],
    ZisoDecorr_cfg,
    skippable=True,
)

readZmass_cfg = Config.configure_task(
    "readZmass",
    readZmass,
    [ZmassPath_cfg, workingPointID_cfg, year_cfg],
    [ZmassSFstatsyst_cfg, Zmassx_cfg, Zmassy_cfg],
    skippable=True,
)

convertGeVtoMeVZmass_cfg = Config.configure_task(
    "convertGeVtoMeVZmass", convertGeVtoMeV, Zmassx_cfg, ZmassxMeV_cfg, skippable=True
)

readZmassStat_cfg = Config.configure_task(
    "readZmassStat",
    readZmassStat,
    [ZmassPath_cfg, workingPointID_cfg],
    ZmassStatUncorrCorr_cfg,
    skippable=True,
)

decorrelateStatZmass_cfg = Config.configure_task(
    "decorrelateStatZmass",
    decorrelateStat,
    [ZmassSFstatsyst_cfg, ZmassStatUncorrCorr_cfg, ZmassxMeV_cfg, Zmassy_cfg],
    ZmassDecorr_cfg,
    skippable=True,
)
consistentBinning_cfg = Config.configure_task(
    "consistentBinning",
    consistentBinning,
    [ZmassxMeV_cfg, Zmassy_cfg, ZisoxMeV_cfg, Zisoy_cfg],
    [Zcombx_cfg, Zcomby_cfg],
    skippable=True,
)
decorrelateSyst_cfg = Config.configure_task(
    "decorrelateSyst",
    decorrelateSyst,
    [ZmassDecorr_cfg, ZisoDecorr_cfg, Zcombx_cfg, Zcomby_cfg],
    [ZmassSystDecorr_cfg, ZisoSystDecorr_cfg],
    skippable=True,
)
rebinLowPtZiso_cfg = Config.configure_task(
    "rebinLowPtZiso",
    rebinlowpt,
    [
        ZisoSystDecorr_cfg,
        Zcombx_cfg,
        Zcomby_cfg,
        ZisoMethodName_cfg,
        RebinMethod_cfg,
    ],
    [
        ZisoRebin_cfg,
        ZisoRebinx_cfg,
        ZisoRebiny_cfg,
        Zisolo_cfg,
        Zisolox_cfg,
        Zisoloy_cfg,
        Zisohi_cfg,
        Zisohix_cfg,
        Zisohiy_cfg,
    ],
    skippable=True,
)
rebinLowPtZmass_cfg = Config.configure_task(
    "rebinLowPtZmass",
    rebinlowpt,
    [
        ZmassSystDecorr_cfg,
        Zcombx_cfg,
        Zcomby_cfg,
        ZmassMethodName_cfg,
        RebinMethod_cfg,
    ],
    [
        ZmassRebin_cfg,
        ZmassRebinx_cfg,
        ZmassRebiny_cfg,
        Zmasslo_cfg,
        Zmasslox_cfg,
        Zmassloy_cfg,
        Zmasshi_cfg,
        Zmasshix_cfg,
        Zmasshiy_cfg,
    ],
    skippable=True,
)
combineZmethodsRebin_cfg = Config.configure_task(
    "combineZmethodsRebin",
    combineZmethods,
    [ZmassRebin_cfg, ZisoRebin_cfg, ZisoRebinx_cfg, ZisoRebiny_cfg],
    [ZcombRebin_cfg],
    skippable=True,
)
combineZmethodslo_cfg = Config.configure_task(
    "combineZmethodslo",
    combineZmethods,
    [Zmasslo_cfg, Zisolo_cfg, Zisolox_cfg, Zisoloy_cfg],
    [Zcomblo_cfg],
    skippable=True,
)
combineZmethodshi_cfg = Config.configure_task(
    "combineZmethodshi",
    combineZmethods,
    [Zmasshi_cfg, Zisohi_cfg, Zisohix_cfg, Zisohiy_cfg],
    [Zcombhi_cfg],
    skippable=True,
)
comparisonPlots_corruncorrRebin_cfg = Config.configure_task(
    "comparisonPlots_corruncorrRebin",
    comparisonPlots_corruncorr,
    [
        RebinName_cfg,
        ZcombRebin_cfg,
        ZmassRebin_cfg,
        ZisoRebin_cfg,
        ZisoRebinx_cfg,
        ZisoRebiny_cfg,
        ZcombPlotLabels_cfg,
        PlotSaving_cfg,
    ],
    [RebinPlotLoc_cfg],
    skippable=True,
)
comparisonPlots_corruncorrlo_cfg = Config.configure_task(
    "comparisonPlots_corruncorrlo",
    comparisonPlots_corruncorr,
    [
        LoName_cfg,
        Zcomblo_cfg,
        Zmasslo_cfg,
        Zisolo_cfg,
        Zisolox_cfg,
        Zisoloy_cfg,
        ZcombPlotLabels_cfg,
        PlotSaving_cfg,
    ],
    [LoPlotLoc_cfg],
    skippable=True,
)
comparisonPlots_corruncorrhi_cfg = Config.configure_task(
    "comparisonPlots_corruncorrhi",
    comparisonPlots_corruncorr,
    [
        HiName_cfg,
        Zcombhi_cfg,
        Zmasshi_cfg,
        Zisohi_cfg,
        Zisohix_cfg,
        Zisohiy_cfg,
        ZcombPlotLabels_cfg,
        PlotSaving_cfg,
    ],
    [HiPlotLoc_cfg],
    skippable=True,
)
"""
undecorrelateStatRebin_cfg = Config.configure_task(
    "undecorrelateStatRebin",
    undecorrelateStat,
    [ZcombRebin_cfg, ZisoRebinx_cfg, ZisoRebiny_cfg],
    [ZcombUnDecorrRebin_cfg],
    skippable=True
)
undecorrelateStatlo_cfg = Config.configure_task(
    "undecorrelateStatlo",
    undecorrelateStat,
    [Zcomblo_cfg, Zisolox_cfg, Zisoloy_cfg],
    [ZcombUnDecorrlo_cfg],
    skippable=True
)
undecorrelateStathi_cfg = Config.configure_task(
    "undecorrelateStathi",
    undecorrelateStat,
    [Zcombhi_cfg, Zisohix_cfg, Zisohiy_cfg],
    [ZcombUnDecorrhi_cfg],
    skippable=True
)
"""
readJpsi_cfg = Config.configure_task(
    "readJpsi",
    readJpsi,
    [JpsiPath_cfg, workingPointID_cfg, year_cfg],
    [JpsiSFstatsyst_cfg, Jpsix_cfg, Jpsiy_cfg],
    skippable=True,
)

convertGeVtoMeVJpsi_cfg = Config.configure_task(
    "convertGeVtoMeVJpsi", convertGeVtoMeV, Jpsix_cfg, JpsixMeV_cfg, skippable=True
)

decorrelateSystJpsi_cfg = Config.configure_task(
    "decorrelateSystJpsi",
    decorrelateSyst,
    [ZcombRebin_cfg, JpsiSFstatsyst_cfg, JpsixMeV_cfg, Jpsiy_cfg],
    [ZcombSystDecorr_cfg, JpsiSystDecorr_cfg],
    skippable=True,
)
combineZmethodsHerahi_cfg = Config.configure_task(
    "combineHera",
    combineHera,
    [ZcombSystDecorr_cfg, JpsiSystDecorr_cfg, JpsixMeV_cfg, Jpsiy_cfg],
    [ZJpsicomb_cfg],
    skippable=True,
)
chiSquared_cfg = Config.configure_task(
    "chiSquared",
    chi2,
    [ZcombSystDecorr_cfg, JpsiSystDecorr_cfg, ZJpsicomb_cfg, JpsixMeV_cfg, Jpsiy_cfg],
    [ChiSquaredScore_cfg, ChiSquaredScoreEta_cfg],
    skippable=True,
)
comparisonPlots_cfg = Config.configure_task(
    "comparisonPlots",
    comparisonPlots,
    [
        HiName_cfg,
        Zcombhi_cfg,
        Zmasshi_cfg,
        Zisohi_cfg,
        Zisohix_cfg,
        Zisohiy_cfg,
        ZcombPlotLabels_cfg,
        PlotSaving_cfg,
    ],
    [HiPlotLoc_cfg],
    skippable=True,
)

"""
undecorrelateStatAll_cfg = Config.configure_task(
    "undecorrelateStatRebin",
    undecorrelateStat,
    [ZcombRebin_cfg, ZisoRebinx_cfg, ZisoRebiny_cfg],
    [ZcombUnDecorrRebin_cfg],
    skippable=True
)"""
comparisonPlots_corruncorrAll_cfg = Config.configure_task(
    "comparisonPlots_corruncorrAll",
    comparisonPlots_corruncorr,
    [
        AllName_cfg,
        ZJpsicomb_cfg,
        ZcombSystDecorr_cfg,
        JpsiSystDecorr_cfg,
        JpsixMeV_cfg,
        Jpsiy_cfg,
        AllcombPlotLabels_cfg,
        PlotSaving_cfg,
    ],
    [HiPlotLoc_cfg],
    skippable=True,
)
# writeRootFilesDecompZmass_cfg
# writeRootFilesDecompZiso_cfg
# writeRootFilesDecompZJpsi_cfg

# Configuration of scenario
scenario_cfg = Config.configure_scenario(
    id="my_scenario",
    task_configs=[
        pathFinder_cfg,
        readZiso_cfg,
        convertGeVtoMeVZiso_cfg,
        readZisoStat_cfg,
        decorrelateStatZiso_cfg,
        readZmass_cfg,
        convertGeVtoMeVZmass_cfg,
        readZmassStat_cfg,
        decorrelateStatZmass_cfg,
        consistentBinning_cfg,
        decorrelateSyst_cfg,
        rebinLowPtZiso_cfg,
        rebinLowPtZmass_cfg,
        combineZmethodsRebin_cfg,
        combineZmethodslo_cfg,
        combineZmethodshi_cfg,
        comparisonPlots_corruncorrRebin_cfg,
        readJpsi_cfg,
        convertGeVtoMeVJpsi_cfg,
        decorrelateSystJpsi_cfg,
        combineZmethodsHerahi_cfg,
        chiSquared_cfg,
        comparisonPlots_corruncorrAll_cfg,
    ],
)
# Prebinned data
####################################################
RemoveNeg_cfg = Config.configure_data_node("RemoveNeg", default_data="yes")
ZisoPathPrebin_cfg = Config.configure_data_node("ZisoPathPrebin")
ZmassPathPrebin_cfg = Config.configure_data_node(
    "ZmassPathPrebin",
)
JpsiPathPrebin_cfg = Config.configure_data_node(
    "JpsiPathPrebin",
)
ZisoSFstatsystPrebin_cfg = Config.configure_pickle_data_node(
    "ZisoPrebin",
)
ZisoxPrebin_cfg = Config.configure_pickle_data_node(
    "Ziso_xPrebin",
)
ZisoyPrebin_cfg = Config.configure_pickle_data_node(
    "Ziso_yPrebin",
)

ZisoxMeVPrebin_cfg = Config.configure_pickle_data_node(
    "Ziso_xMeVPrebin",
)
ZisoStatUncorrCorrPrebin_cfg = Config.configure_pickle_data_node(
    "ZisoStatUncorrCorrPrebin",
)

ZisoDecorrPrebin_cfg = Config.configure_pickle_data_node(
    "ZisoDecorrPrebin",
)

ZmassSFstatsystPrebin_cfg = Config.configure_pickle_data_node(
    "ZmassPrebin",
)
ZmassxPrebin_cfg = Config.configure_pickle_data_node(
    "Zmass_xPrebin",
)
ZmassyPrebin_cfg = Config.configure_pickle_data_node(
    "Zmass_yPrebin",
)

ZmassxMeVPrebin_cfg = Config.configure_pickle_data_node(
    "Zmass_xMeVPrebin",
)

ZmassStatUncorrCorrPrebin_cfg = Config.configure_pickle_data_node(
    "ZmassStatUncorrCorrPrebin",
)

ZmassDecorrPrebin_cfg = Config.configure_pickle_data_node(
    "ZmassDecorrPrebin",
)
ZisoSystDecorrPrebin_cfg = Config.configure_pickle_data_node(
    "ZisoSystDecorrPrebin",
)
ZmassSystDecorrPrebin_cfg = Config.configure_pickle_data_node(
    "ZmassSystDecorrPrebin",
)

ZcombxPrebin_cfg = Config.configure_pickle_data_node(
    "ZcombxPrebin",
)

ZcombyPrebin_cfg = Config.configure_pickle_data_node(
    "ZcombyPrebin",
)
# Prebinned tasks
####################################################

pathFinderPrebin_cfg = Config.configure_task(
    "SetPathsforPrebin",
    pathFinderPrebin,
    [year_cfg, ExtraBin_cfg],
    [ZisoPathPrebin_cfg, ZmassPathPrebin_cfg, JpsiPathPrebin_cfg],
)
readJpsiPrebin_cfg = Config.configure_task(
    "readJpsiPrebin",
    readJpsi,
    [JpsiPathPrebin_cfg, workingPointID_cfg, year_cfg],
    [JpsiSFstatsyst_cfg, Jpsix_cfg, Jpsiy_cfg],
    skippable=True,
)
readZisoPrebin_cfg = Config.configure_task(
    "readZisoPrebin",
    readZiso,
    [ZisoPathPrebin_cfg, workingPointID_cfg, year_cfg, RemoveNeg_cfg],
    [ZisoSFstatsystPrebin_cfg, ZisoxPrebin_cfg, ZisoyPrebin_cfg],
    skippable=True,
)

convertGeVtoMeVZisoPrebin_cfg = Config.configure_task(
    "convertGeVtoMeVZisoPrebin",
    convertGeVtoMeV,
    ZisoxPrebin_cfg,
    ZisoxMeVPrebin_cfg,
    skippable=True,
)

readZisoStatPrebin_cfg = Config.configure_task(
    "readZisoStatPrebin",
    readZisoStat,
    [ZisoPathPrebin_cfg, workingPointID_cfg, RemoveNeg_cfg],
    ZisoStatUncorrCorrPrebin_cfg,
    skippable=True,
)

decorrelateStatZisoPrebin_cfg = Config.configure_task(
    "decorrelateStatZisoPrebin",
    decorrelateStat,
    [
        ZisoSFstatsystPrebin_cfg,
        ZisoStatUncorrCorrPrebin_cfg,
        ZisoxMeVPrebin_cfg,
        ZisoyPrebin_cfg,
    ],
    ZisoDecorrPrebin_cfg,
    skippable=True,
)

readZmassPrebin_cfg = Config.configure_task(
    "readZmassPrebin",
    readZmass,
    [ZmassPathPrebin_cfg, workingPointID_cfg, year_cfg, RemoveNeg_cfg],
    [ZmassSFstatsystPrebin_cfg, ZmassxPrebin_cfg, ZmassyPrebin_cfg],
    skippable=True,
)

convertGeVtoMeVZmassPrebin_cfg = Config.configure_task(
    "convertGeVtoMeVZmassPrebin",
    convertGeVtoMeV,
    ZmassxPrebin_cfg,
    ZmassxMeVPrebin_cfg,
    skippable=True,
)

readZmassStatPrebin_cfg = Config.configure_task(
    "readZmassStatPrebin",
    readZmassStat,
    [ZmassPathPrebin_cfg, workingPointID_cfg, RemoveNeg_cfg],
    ZmassStatUncorrCorrPrebin_cfg,
    skippable=True,
)

decorrelateStatZmassPrebin_cfg = Config.configure_task(
    "decorrelateStatZmassPrebin",
    decorrelateStat,
    [
        ZmassSFstatsystPrebin_cfg,
        ZmassStatUncorrCorrPrebin_cfg,
        ZmassxMeVPrebin_cfg,
        ZmassyPrebin_cfg,
    ],
    ZmassDecorrPrebin_cfg,
    skippable=True,
)

consistentBinningPrebin_cfg = Config.configure_task(
    "consistentBinningPrebin",
    consistentBinning,
    [ZmassxMeVPrebin_cfg, ZmassyPrebin_cfg, ZisoxMeVPrebin_cfg, ZisoyPrebin_cfg],
    [ZcombxPrebin_cfg, ZcombyPrebin_cfg],
    skippable=True,
)


decorrelateSystPrebin_cfg = Config.configure_task(
    "decorrelateSystPrebin",
    decorrelateSyst,
    [ZmassDecorrPrebin_cfg, ZisoDecorrPrebin_cfg, ZcombxPrebin_cfg, ZcombyPrebin_cfg],
    [ZmassSystDecorrPrebin_cfg, ZisoSystDecorrPrebin_cfg],
    skippable=True,
)

combineZmethodsRebinPrebin_cfg = Config.configure_task(
    "combineZmethodsRebinPrebin",
    combineZmethods,
    [
        ZmassSystDecorrPrebin_cfg,
        ZisoSystDecorrPrebin_cfg,
        ZcombxPrebin_cfg,
        ZcombyPrebin_cfg,
    ],
    [ZcombRebin_cfg],
    skippable=True,
)

comparisonPlots_corruncorrRebinPrebin_cfg = Config.configure_task(
    "comparisonPlots_corruncorrRebinPrebin",
    comparisonPlots_corruncorr,
    [
        RebinName_cfg,
        ZcombRebin_cfg,
        ZmassSystDecorrPrebin_cfg,
        ZisoSystDecorrPrebin_cfg,
        ZcombxPrebin_cfg,
        ZcombyPrebin_cfg,
        ZcombPlotLabels_cfg,
        PlotSaving_cfg,
    ],
    [RebinPlotLoc_cfg],
    skippable=True,
)
prebinScenario_cfg = Config.configure_scenario(
    id="prebinnedScenario",
    task_configs=[
        pathFinder_cfg,
        pathFinderPrebin_cfg,
        readZiso_cfg,
        readZisoPrebin_cfg,
        convertGeVtoMeVZiso_cfg,
        convertGeVtoMeVZisoPrebin_cfg,
        readZisoStat_cfg,
        readZisoStatPrebin_cfg,
        decorrelateStatZiso_cfg,
        decorrelateStatZisoPrebin_cfg,
        readZmass_cfg,
        readZmassPrebin_cfg,
        convertGeVtoMeVZmass_cfg,
        convertGeVtoMeVZmassPrebin_cfg,
        readZmassStat_cfg,
        readZmassStatPrebin_cfg,
        decorrelateStatZmass_cfg,
        decorrelateStatZmassPrebin_cfg,
        consistentBinning_cfg,
        consistentBinningPrebin_cfg,
        decorrelateSyst_cfg,
        decorrelateSystPrebin_cfg,
        rebinLowPtZiso_cfg,
        rebinLowPtZmass_cfg,
        combineZmethodsRebinPrebin_cfg,
        combineZmethodslo_cfg,
        combineZmethodshi_cfg,
        # comparisonPlots_corruncorrRebinPrebin_cfg,
        readJpsiPrebin_cfg,
        convertGeVtoMeVJpsi_cfg,
        decorrelateSystJpsi_cfg,
        combineZmethodsHerahi_cfg,
        chiSquared_cfg,
        # comparisonPlots_corruncorrAll_cfg,
    ],
)
Config.export("config.toml")
