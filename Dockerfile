
FROM rootproject/root:6.30.02-alma9

RUN yum update -y 
# Create a non-root user with a home directory, here named 'appuser'
RUN adduser -m -g root node 

EXPOSE 5000

WORKDIR /app

COPY . /app
# Change the ownership of the /app directory to 'appuser'
RUN chmod -R 777 *
RUN chmod -R 777 /app/.data/*
RUN chown -R node:root /app

RUN pip install -r requirements.txt

ENV WORKING_DIR=/app
# Switch to the non-root user for subsequent commands and container runtime
USER 1000
RUN ls -la

CMD ["python3", "main.py", "--production"]