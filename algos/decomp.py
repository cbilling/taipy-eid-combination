#!/usr/bin/env python

import sys, operator
from tools.arrayToEgamma import *
from tools.statTools import *
import numpy as np
import ROOT

# the old doFifteen variable makes sure we always fill 15 corr systs, regardless of the size of the eigenvalues
doFifteen = 15
sflabel = "FullSim"
print(sys.argv)
# now the input
oufname = sys.argv[1]
if ',' not in sys.argv[-1] and '/' not in sys.argv[-1] :
    # then it should be the label for AF2 and FullSim
    sflabel = sys.argv[-1]
    del sys.argv[-1] # delete it right away so I don't have to rewrite the code below

indirs = sys.argv[2:-1]
years  = sys.argv[-1].split(',')
print(years)
if len(years)==1 and '/' in years[0] : # then it's probably another dir and we didn't provide a label
    print("in stupid logic")
    indirs.append(years[0])
    years = 0
print("SFLABEL ", sflabel)
print("YEARS ", years)

# read the values from the previous steps 
SF   = []
stat = []
syst = []
xbin = []
ybin = []
for indir in indirs :

    SF_meth, stat_meth, syst_meth, x_meth, y_meth = readArray_SF_stat_syst( indir )
    SF.append(SF_meth)
    stat.append(stat_meth)
    syst.append(syst_meth)
    xbin.append(x_meth)
    ybin.append(y_meth)

if years and len(years) != len(SF) :
    print(' in writeRootFiles.py: you didn\'t provide enough years ')
    exit()

nSys = syst[0].shape[1]
consistent = True
for i in range(1,len(SF)) :
#    consistent = consistent and consistency(xbin[i],xbin[0]) # I don't think we need them to be consistent
#    consistent = consistent and consistency(ybin[i],ybin[0])
    # for Jpsi we also need to pad the syst, so let's store nSys (for the length we don't care)
    nSys_i = syst[i].shape[1]
    nSys = max( nSys, nSys_i )

if not consistent :
    print(' trying to combine two methods with inconsistent binning ')
    exit()

# stack the systs together so we can decompose
syst_all = np.zeros( (syst[0].shape[0],nSys) )
syst_all[:syst[0].shape[0],:syst[0].shape[1]] = syst[0]

for i in range(1,len(SF)) :

    syst_i = np.zeros( (syst[i].shape[0],nSys) )
    syst_i[:syst[i].shape[0],:syst[i].shape[1]] = syst[i]
    syst_all = np.concatenate( (syst_all,syst_i), axis=0 )

# get the covariance matrix
cov = get_covariance( syst_all )
val_eig,vec_eig = np.linalg.eigh(cov)

# !!! the stupid thing is upside down compared to linalg.eig !!!
val_eig = np.flipud(val_eig)
vec_eig = np.fliplr(vec_eig)

val_eig = np.abs(val_eig)

# eigenvalues are already sorted by size, let's find the largest 95%
counteig = 0
currentsum = 0
for eig in val_eig :

    currentsum += eig
    counteig += 1
    if currentsum/np.sum(val_eig) > 0.99 :
        break
    if counteig>doFifteen :
        print(' we have more than ', doFifteen, ' eig values ')
        break
        
#print(' checking vec eig ', vec_eig.shape)
#for i in range(0,vec_eig.shape[0]) :
#    print(' checking vec eig ', np.sum(vec_eig[:,i]**2))
gamma = vec_eig[:,:counteig].dot( np.diag( np.sqrt( val_eig[:counteig] ) ) )
rest  = vec_eig[:,counteig:].dot( np.diag( np.sqrt( val_eig[counteig:] ) ) )
rest  = np.sqrt( np.sum( rest**2, axis=1 ) )
if counteig<doFifteen :
    gamma = np.column_stack( (gamma,np.zeros( (gamma.shape[0],doFifteen-counteig) )) )

uncorr = []
for i in range(0,len(syst)) :
    currentsyst   = gamma[:syst[i].shape[0],:]
    remainingsyst = gamma[syst[i].shape[0]:,:]

    syst[i] = currentsyst
    gamma = remainingsyst

    currentrest   = rest[:syst[i].shape[0]]
    remainingrest = rest[syst[i].shape[0]:]
    #    print(' current rest  ', currentrest.shape, ' remaining ', remainingrest.shape)
    uncorr.append( currentrest )
    rest = remainingrest

outfile = ROOT.TFile(oufname,'RECREATE')

for i in range(0,len(SF)) :

    outfile.cd()
    runrange = lookupRunRange( years if years==0 else int(years[i]) )

    prefix = sflabel+'_'
    if not outfile.GetDirectory( runrange ) :
        outfile.mkdir( runrange )
    else : # the second get's the LowPt prefix, didn't have a better idea how to handle this
        prefix = prefix+'LowPt_'

    outfile.cd( runrange )
    writeToEgammaFile( SF[i], stat[i], syst[i], uncorr[i], xbin[i], ybin[i], prefix, False )
    
    h_eig = ROOT.TH1D(prefix+'eig', prefix+'eig', doFifteen+1, 0, doFifteen+1)
    h_eig.SetDirectory(0)
#    print(' PHILIP eig ', val_eig[:counteig], ' sum ', np.sum(val_eig), ' fraction ', np.sum(val_eig[:counteig]))
    for eig in range(0,counteig-1) :
        h_eig.SetBinContent(doFifteen-eig+1,val_eig[eig])
    h_eig.SetBinContent(1, np.sum(val_eig[counteig-1:]) )
#    print(' PHILIP integral ', h_eig.Integral())
    h_eig.Scale(1./h_eig.Integral())
    h_eig.Write()

outfile.Write()
outfile.Close()
