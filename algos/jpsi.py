import sys, operator
from .tools.rootToArray import *
from .tools.arrayToText import *


def readJpsi(infile, ID, year, removeNeg="no"):
    # define the histograms holding the central value, and the systematic variations
    print("infile: ", infile)
    list = [
        "sf_CentralValues_" + ID,  # this is the chosen nominal variation
        "sf_StatError_" + ID,
        "sf_CentralValues_prompt0p2_tag_baseline_den_baseline_Cheb_2_mll_2.7_3.4_" + ID,
        "sf_CentralValues_prompt0p2_tag_baseline_den_baseline_Cheb_3_mll_2.8_3.3_" + ID,
        "sf_CentralValues_prompt0p2_tag_baseline_den_baseline_Cheb_3_mll_2.7_3.4_" + ID,
        "sf_CentralValues_prompt0p4_tag_baseline_den_baseline_Cheb_3_mll_2.7_3.4_" + ID,
        "sf_CentralValues_prompt0p2_tag_iso0p15_den_baseline_Cheb_3_mll_2.7_3.4_" + ID,
        "sf_CentralValues_prompt0p2_tag_baseline_den_ptcone0p15_Cheb_3_mll_2.7_3.4_"
        + ID,
        "sf_CentralValues_prompt0p2_tag_baseline_den_ptcone0p02_Cheb_3_mll_2.7_3.4_"
        + ID,
    ]
    # sf_SystUp_" + ID,
    # "sf_CentralValues__tauprompt20_iso002_Cheb_2_mll_2.8_3.3_"+ID,
    # "StatError__tauprompt20_iso002_Cheb_2_mll_2.8_3.3_"+ID,
    # "sf_CentralValues__tauprompt20_iso002_Cheb_2_mll_2.7_3.4_"+ID,

    # "sf_CentralValues__tauprompt20_iso015_Cheb_2_mll_2.8_3.3_"+ID,
    # "sf_CentralValues__tauprompt20_iso015_Cheb_3_mll_2.8_3.3_"+ID,
    # "sf_CentralValues__tauprompt20_iso015_Cheb_2_mll_2.7_3.4_"+ID,

    # "sf_CentralValues__tauprompt20_iso50_Cheb_2_mll_2.8_3.3_"+ID,

    # "sf_CentralValues__tauprompt20_iso50_Cheb_3_mll_2.7_3.4_"+ID,
    # "sf_CentralValues__tauprompt40_iso002_Cheb_2_mll_2.8_3.3_"+ID,
    # "sf_CentralValues__tauprompt40_iso002_Cheb_3_mll_2.8_3.3_"+ID,
    # "sf_CentralValues__tauprompt40_iso002_Cheb_2_mll_2.7_3.4_"+ID,
    # "sf_CentralValues__tauprompt40_iso002_Cheb_3_mll_2.7_3.4_"+ID,
    # "sf_CentralValues__tauprompt40_iso015_Cheb_2_mll_2.8_3.3_"+ID,
    # "sf_CentralValues__tauprompt40_iso015_Cheb_3_mll_2.8_3.3_"+ID,
    # "sf_CentralValues__tauprompt40_iso015_Cheb_2_mll_2.7_3.4_"+ID,
    # "sf_CentralValues__tauprompt40_iso015_Cheb_3_mll_2.7_3.4_"+ID,
    # "sf_CentralValues__tauprompt40_iso50_Cheb_2_mll_2.8_3.3_"+ID,
    # "sf_CentralValues__tauprompt40_iso50_Cheb_3_mll_2.8_3.3_"+ID,
    # "sf_CentralValues__tauprompt40_iso50_Cheb_2_mll_2.7_3.4_"+ID,

    # now we fill the histograms into arrays
    SF, stat, syst, x, y = getArray_SF_stat_syst(infile, list)

    # in Elias file, the last bin seems problematic and a measurement doesn't always exist
    # as a temporary fix, I'm just using the measurement from bin in opposite eta
    def check_SF_stat_syst(SF, stat, syst):
        for i in range(0, len(SF)):
            if SF[i] != 0 and SF[i] == SF[i]:
                continue

            ybin = i % len(y)
            ybin_new = len(y) - ybin - 1
            i_new = i - ybin + ybin_new
            SF[i] = SF[i_new]
            stat[i] = stat[i_new]
            syst[i] = syst[i_new]

            if SF[i] != 0 and SF[i] == SF[i]:
                continue

            i_new = i - len(y)
            SF[i] = SF[i_new]
            stat[i] = stat[i_new]
            syst[i] = syst[i_new]

        return SF, stat, syst

    SF, stat, syst = check_SF_stat_syst(SF, stat, syst)

    arr_out = np.column_stack((SF, stat, syst))
    print()
    print("removeNeg: ", removeNeg)
    pos_arr = []
    if removeNeg != "no":
        posy = []
        for b in y:
            if b >= 0:
                posy.append(b)
        print(y, posy)
        y = posy
        print(arr_out, arr_out.shape)
        for row_i in range(len(arr_out)):
            if row_i % (len(posy) * 2) >= len(posy):
                pos_arr.append(arr_out[row_i, :])
        arr_out = np.array(pos_arr)
        print(arr_out, arr_out.shape)

    return arr_out, x, y
