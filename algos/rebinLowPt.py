import os
from .tools.arrayToText import writeArray, readArray, readArray_SF_stat_syst
import sys, operator
from .tools.statTools import *

working_dir = os.environ["WORKING_DIR"]

def rebinlowpt(indir_meth1, x, y, method, rebinMethod):
    if rebinMethod.lower() == "hera":
        path = working_dir+"/.data/" + method
        try:
            os.mkdir(path)
        except:
            pass
        writeArray(path, indir_meth1, x, y)
        
        os.system(
            f"{working_dir}/algos/averagerpy27/rebincentos7.sh {path} {path}"
        )

        Ziso_lo_rebin, x_lo_rebin, y_lo_rebin = readArray(path + "_lo_rebin")
        Ziso_lo, x_lo, y_lo = readArray(path + "_lo")
        Ziso_hi, x_hi, y_hi = readArray(path + "_hi")

    else:
        pT_max = 15000.0

        # the input arrays
        SF, stat, syst, x, y = readArray_SF_stat_syst(indir_meth1, x, y)

        bin = 0
        for xbin in x:
            if xbin <= pT_max:
                bin = bin + 1

        SF_lo, stat_lo, syst_lo, x_lo, y_lo = (
            SF[: (bin * y.size)],
            stat[: (bin * y.size)],
            syst[: (bin * y.size)],
            x[:bin],
            y,
        )
        SF_hi, stat_hi, syst_hi, x_hi, y_hi = (
            SF[(bin * y.size) :],
            stat[(bin * y.size) :],
            syst[(bin * y.size) :],
            x[bin:],
            y,
        )
        Ziso_lo_rebin = []
        x_lo_rebin = []
        y_lo_rebin = []

        Ziso_lo = np.column_stack((SF_lo, stat_lo, syst_lo))

        Ziso_hi = np.column_stack((SF_hi, stat_hi, syst_hi))

        print("\n\n\n\nREBIN Method: ", rebinMethod, "\n\n\n")
        if rebinMethod == "by_hand":
            # hardcoded binning at low pT
            pTbins = np.array([4500.00, 7000.00, 10000.00, 15000.00])
            etabins = np.array([0.00, 0.10, 0.80, 1.37, 1.52, 2.01])

            SF_new = np.zeros(len(pTbins) * len(etabins))
            stat_new = np.zeros(len(pTbins) * len(etabins))
            syst_new = np.zeros([len(pTbins) * len(etabins), syst_lo.shape[1]])

            xbin_new = 0
            ybin_new = len(etabins) - 1

            print(x_lo)
            print(x_hi)
            # determine the starting bin
            while pTbins[xbin_new] < x_lo[0]:
                xbin_new = xbin_new + 1

            SF_tmp = []
            stat_tmp = []
            syst_tmp = []

            def normalized_weighted_sum(sf, stat, syst=np.zeros([len(stat), 1])):
                # sig will a 1d array with the length equal to the bins being combined
                sig = [
                    np.sqrt(stat[i] ** 2 + np.sum(np.square(syst[i])))
                    for i in range(len(stat))
                ]

                # weights are the reciprocal of the sigma^2
                weights = np.reciprocal(np.square(sig))

                # Normalize the weights by dividing by the sum of the weights
                weights_norm = weights / np.sum(weights)

                avg = np.sum(sf * weights_norm)

                total_error = np.sqrt(np.sum(np.square(sig)))
                return avg, total_error

            print("etabins", y_lo)
            # pT has to go first because we're averaging over several eta bins
            for xbin in range(len(x_lo)):
                for ybin in range(len(y_lo) - 1, -1, -1):
                    if (
                        y_lo[ybin] < etabins[0]
                    ):  # skip these cases because we're going to do an abs
                        print(y_lo[ybin], etabins[0])
                        continue

                    # positive eta
                    SF_tmp.append(SF_lo[xbin * len(y_lo) + ybin])
                    stat_tmp.append(
                        stat_lo[xbin * len(y_lo) + ybin]
                        * stat_lo[xbin * len(y_lo) + ybin]
                    )
                    syst_tmp.append(syst_lo[xbin * len(y_lo) + ybin, :])
                    # stat_tmp.append(1)
                    # negative eta
                    # print "Positive eta", y_lo[ybin], "Negative eta", y_lo[len(y_lo)-ybin-1]
                    if y_lo[len(y_lo) - ybin - 1] != -2.48:
                        # stat_tmp.append(1)

                        SF_tmp.append(SF_lo[xbin * len(y_lo) + len(y_lo) - ybin - 1])
                        stat_tmp.append(
                            stat_lo[xbin * len(y_lo) + len(y_lo) - ybin - 1]
                            * stat_lo[xbin * len(y_lo) + len(y_lo) - ybin - 1]
                        )
                        syst_tmp.append(
                            syst_lo[xbin * len(y_lo) + len(y_lo) - ybin - 1, :]
                        )
                    # else:

                    #    print "Skipped last"
                    # if the bin boundaries match, we'll start a new calculation
                    if (
                        pTbins[xbin_new] == x_lo[xbin]
                        and etabins[ybin_new] == y_lo[ybin]
                    ):
                        SF_tmp = np.array(SF_tmp)
                        stat_tmp = np.array(stat_tmp)
                        syst_tmp = np.array(syst_tmp)

                        #            # chi2 combination from https://cds.cern.ch/record/273386
                        # I GAVE UP ON THIS EVENTUALLY, SO DOESN'T WORK
                        #            cov = get_covariance( np.column_stack( (np.diag(stat_tmp),syst_tmp) ) )
                        #            from scipy.linalg import pinvh
                        #            invcov = pinvh( cov )
                        #
                        #            av = np.sum( invcov.dot(SF_tmp) ) / np.sum( invcov )
                        #            av_err = np.sqrt( 1./ np.sum( invcov ) )
                        #
                        #            av_stat = np.sum( invcov.dot(np.square(stat_tmp)) ) / np.sum( invcov ) / len(SF_tmp)
                        #            #            print ' shape ', np.square(syst_tmp[:,-1])
                        #            av_syst = np.sum( invcov.dot(np.square(syst_tmp)), axis=0 ) / np.sum( invcov ) / len(SF_tmp)
                        #            print 'stat ',av_stat
                        ##            print 'syst ',av_syst
                        #            #c_mat1 = np.tensordot(Q, a1, axes=([-1],[0]))
                        #            exit()
                        SF_tmp = np.array(SF_tmp, ndmin=2).T
                        stat_tmp = np.array(stat_tmp, ndmin=2).T
                        syst_tmp = np.array(syst_tmp, ndmin=3).T
                        SF_tmp = np.swapaxes(SF_tmp, 0, 1)
                        stat_tmp = np.swapaxes(stat_tmp, 0, 1)
                        syst_tmp = np.swapaxes(syst_tmp, 1, 2)

                        #            print syst_tmp
                        #            print SF_tmp.shape
                        #            print stat_tmp.shape
                        #            print syst_tmp.shape
                        average, total_error = normalized_weighted_sum(
                            SF_tmp, stat_tmp, syst_tmp
                        )
                        #            print average.shape
                        #            print averstat.shape
                        #            print aversyst.shape

                        # average = expected_SF_stat_only
                        # averstat = expected_error_stat_only
                        # print aversyst.T
                        SF_new[xbin_new * len(etabins) + ybin_new] = average
                        stat_new[xbin_new * len(etabins) + ybin_new] = total_error
                        syst_new[xbin_new * len(etabins) + ybin_new] = syst_tmp[
                            :, :, xbin
                        ].ravel()

                        SF_tmp = []
                        stat_tmp = []
                        syst_tmp = []

                        ybin_new = ybin_new - 1

            Ziso_lo = np.column_stack((SF_lo, stat_lo, syst_lo))

            Ziso_lo_rebin = np.column_stack((SF_new, stat_new, syst_new))
            x_lo_rebin = pTbins
            y_lo_rebin = etabins

            Ziso_hi = np.column_stack((SF_hi, stat_hi, syst_hi))

    return (
        Ziso_lo_rebin,
        x_lo_rebin,
        y_lo_rebin,
        Ziso_lo,
        x_lo,
        y_lo,
        Ziso_hi,
        x_hi,
        y_hi,
    )
