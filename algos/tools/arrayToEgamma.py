import array
import numpy as np
from ROOT import gDirectory, TFile, gROOT, TH1D, TH2D
from .plotting import *


def writeToEgammaFile(
    SF, stat, syst, uncorr, xbins, ybins, prefix="FullSim_", doEig=True
):
    tot = 0
    if uncorr.size:
        tot = np.sqrt(np.sum(np.column_stack((stat, syst, uncorr)) ** 2, axis=1))
        makeTH2D(
            uncorr, np.zeros(uncorr.shape), xbins, ybins, prefix + "uncorr"
        ).Write()
    else:
        tot = np.sqrt(np.sum(np.column_stack((stat, syst)) ** 2, axis=1))

    makeTH2D(SF, tot, xbins, ybins, prefix + "sf").Write()
    makeTH2D(stat, np.zeros(stat.shape), xbins, ybins, prefix + "stat").Write()
    if syst.shape[1] == 1:
        makeTH2D(syst, np.zeros(syst.shape), xbins, ybins, prefix + "syst").Write()
    #    makeTH2D( tot , np.zeros(stat.shape), xbins, ybins, prefix+'tot' ).Write()

    if syst.shape[1] != 1:
        nsys = syst.shape[1]
        for isys in range(0, nsys):
            makeTH2D(
                syst[:, nsys - isys - 1],
                np.zeros(syst[:, nsys - isys - 1].shape),
                xbins,
                ybins,
                prefix + "corr%d" % isys,
            ).Write()

        if doEig:
            h_eig = TH1D(
                prefix + "eig", prefix + "eig", syst.shape[1] + 1, 0, syst.shape[1] + 1
            )
            for isys in range(0, nsys):
                val_eig = np.sum(syst[:, nsys - isys - 1] ** 2)
                h_eig.SetBinContent(isys + 2, val_eig)
            h_eig.Scale(h_eig.Integral())
            h_eig.Write()


def lookupRunRange(year):
    print(year)
    if year == 2018:
        runrange = "348197_364485"
    elif year == 2017:
        runrange = "324320_340453"
    elif year == 2016:
        runrange = "296939_311481"
    elif year == 2015:
        runrange = "266904_284484"
    elif year == 20152016:
        runrange = "266904_311481"
    elif year == 20171:  # 2e17 was accidentally prescaled in 326834-328393
        runrange = "324320_326695"
    elif year == 20172:
        runrange = "329385_340453"
    elif year == 20172018:
        runrange = "324320_9999999"
    elif year == 8882016888:  # that's p-Pb data at 8 TeV in 2016
        runrange = "313063_314170"
    elif year == 2022:  # that's for 2022 prerecommendations
        runrange = "422632_9999999"
    else:
        print("in arrayToEgamma.py: unknown year, can't look up run range", year)
        runrange = "0_9999999"
    return runrange


# for asymmetric uncertainties use the photon format with an additional asymm map
def writeToEgammaFile_asymm(SF, stat, syst, asym, xbins, ybins, prefix="FullSim_"):
    #    tot = np.sqrt( np.sum( np.column_stack( (stat,syst) )**2, axis=1 ) ) # don't include syst in tot
    tot = np.array(SF)  # for debugging store the val of the SF as error

    makeTH2F(SF, tot, xbins, ybins, prefix + "sf").Write()
    makeTH2F(stat, np.zeros(stat.shape), xbins, ybins, prefix + "stat").Write()
    makeTH2F(
        syst, np.zeros(syst.shape), xbins, ybins, prefix + "sys"
    ).Write()  # has a different name in photon format!!!
    makeTH2F(asym, np.zeros(asym.shape), xbins, ybins, prefix + "asymm").Write()
