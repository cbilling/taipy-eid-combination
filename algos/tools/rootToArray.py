#!/usr/bin/env python

import array
import numpy as np
from ROOT import gDirectory, TFile, gROOT, TH2F

def getArray_SF_stat_syst( filename, paths ) :
    # ------------------------------------------------------------------------------
    # this is the most important function for us
    #
    # it will return 5 np.arrays:
    # - the central values
    # - the stat uncertainty
    # - an array of systematic uncertainties, with columns of systematic sources
    # - the x-bins
    # - the y-bins
    #
    # it takes as input: 
    # - a root file name
    # - one of the following:
    #   - a path to a histogram as string --> will return bin content and error of the histogram
    #   - a list of paths --> (0) is nominal, (1) is stat, (2) is syst source 1, (3) is syst source 2, ...
    #                         if a list is in the list of paths, this will be considered as up and down variations
    # ------------------------------------------------------------------------------
    # here is what we're returning
    SF   = np.array([])
    stat = np.array([])
    syst = np.array([])
    xbins= np.array([])
    ybins= np.array([])

    infile = TFile.Open( filename )
    if not infile.IsOpen() :
        print( ' in rootToArray: problem opening file ',filename)
        return SF,stat,syst,xbins,ybins

    if isinstance( paths, str ) : # in case we pass a string we just return value and uncertainty of the histogram
        hist = infile.Get(paths)
        # return whatever getArray_SF_err does, and the binning
        SF   = getArray_SF ( hist )
        stat = getArray_err( hist )
        syst = np.zeros( SF.shape )
        syst = syst[:,np.newaxis]
        xbins,ybins = getArray_binning( hist )

    if isinstance( paths, list ) : # if we pass a list of strings, we're doing systematics
        
        # first the central value
        if len(paths)>0 and isinstance( paths[0], str ) :
            path = paths[0]
            hist = infile.Get(path)
            #            print( ' PHILIP getting ', path,  ' ',hist)
            SF = getArray_SF( hist ) # only the first array returned matters
            xbins,ybins = getArray_binning( hist )
        else :
            print( ' in rootToArray: I don\'t know what to do with your paths ',paths)
            exit()

        # then the stat uncertainty
        if len(paths)>1 and isinstance( paths[1], str ) :
            path = paths[1]
            hist = infile.Get(path)
            stat = getArray_SF( hist ) # only the first array returned matters
        else :
            print( ' in rootToArray: I don\'t know what to do with your paths ',paths)
            exit()


        # now let's do some systematics
        if len(paths)==3 : # this means we passed a hist containg the syst, rather than syst variations
            if isinstance( paths[2], str ) :
                path = paths[2]
                hist = infile.Get(path)
                syst = getArray_SF( hist ) # only the first array returned matters
                syst = syst[:,np.newaxis]
            else :
                print( ' in rootToArray: I don\'t know what to do with your paths ',paths)
                exit()

        elif len(paths)>2 : # we passed syst variations
            for entry in range(2,len(paths)) :

                values = np.array([])
                if isinstance( paths[entry], str ) : # if we just passed a string, it's a one sided systematic variation (which gets symmetrised)
                    path = paths[entry]
                    hist = infile.Get(path)
                    values = getArray_SF( hist ) # only the first array returned matters
                    values = systematic( SF, values )
                    
                elif isinstance( paths[entry], list ) and len(paths[entry])==2 : # we can also pass a list with two strings, corresponding to up and dn variation (where we take the envelope and sign)
                    
                    path1 = paths[entry][0]
                    path2 = paths[entry][1]
                    
                    hist1 = infile.Get(path1)
                    value1 = getArray_SF( hist1 ) # only the first array returned matters
                    value1 = systematic( SF, value1 )
                    
                    hist2 = infile.Get(path2)
                    value2 = getArray_SF( hist2 ) # only the first array returned matters
                    value2 = systematic( SF, value2 )
                    
                    values = envelope( value1, value2 )
                    
                else :
                    print( ' in rootToArray: I don\'t know what to do with your paths ',paths[entry])
                    exit()
                
                # add to the exisiting array (if there is one already)
                if syst.shape[0]>0 :
                    syst = np.column_stack((syst,values))
                else :
                    syst = values

        else : # this means we don't do systematics, so let's pass zeros
            syst = np.zeros( stat.shape )
            syst = syst[:,np.newaxis]
    infile.Close()
    return SF,stat,syst,xbins,ybins

    

def getArray_binning( histogram ) : 

    # returns arrays with bin boundaries in x and y, excluding the upper edge of the last bin
    if not histogram :
        print( ' in rootToArray:  no histogram ',histogram)
        return 0
    
    nxbins = histogram.GetXaxis().GetNbins()
    nybins = histogram.GetYaxis().GetNbins()

    xbins = np.array( [histogram.GetXaxis().GetBinLowEdge(x+1) for x in range(0,nxbins)] ) # I don't want the upper edge for now
    ybins = np.array( [histogram.GetYaxis().GetBinLowEdge(y+1) for y in range(0,nybins)] )

    return xbins,ybins


def getArray_SF( histogram ) : 

    # we pass a single histogram, then read off the content and error
    if not histogram :
        print( ' in rootToArray:  no histogram ',histogram)
        return 0
    
    xbins,ybins = getArray_binning( histogram )

    array_SF  = []

    # transform the 2d histogram into a 1d array
    # we fill all y-bins for the first x-bin, then all y-bins for the second x-bin, ...
    for x in range(0,xbins.size) :
        for y in range(0,ybins.size) :
            
            array_SF .append( histogram.GetBinContent(x+1,y+1) )

    return np.array(array_SF)

def getArray_err( histogram ) : 

    # we pass a single histogram, then read off the content and error
    if not histogram :
        print( ' in rootToArray:  no histogram ',histogram)
        return 0
    
    xbins,ybins = getArray_binning( histogram )

    array_err = []

    # transform the 2d histogram into a 1d array
    # we fill all y-bins for the first x-bin, then all y-bins for the second x-bin, ...
    for x in range(0,len(xbins)) :
        for y in range(0,len(ybins)) :
            
            array_err.append( histogram.GetBinError  (x+1,y+1) )

    # we always return three arrays: SF, uncertainty, and blanks
    return np.array(array_err)


def systematic( val1, val2 ) :
    
    # here we just calculate the systematic as the difference of two arrays
    # 
    if len(val1) != len(val2) :
        print( ' in rootToArray:  inconsistent arrays passed with lengths of ', len(val1), ' and ', len(val2))
    
    syst = np.copy(val1)
    for i in range( 0,len(syst) ) :
        syst[i] = val2[i]-val1[i]

    return syst


def envelope( val1, val2 ) :
    
    # here we just calculate the systematic as the difference of two arrays
    # 
    if len(val1) != len(val2) :
        print( ' in rootToArray:  inconsistent arrays passed with lengths of ', len(val1), ' and ', len(val2))
    
    syst = np.copy(val1)
    for i in range( 0,len(syst) ) :

#        maxdev = max( abs(val1[i]), abs(val2[i]) )
#        sigdev = np.sign( val1[i] )
#
#        syst[i] = sigdev*maxdev

        # find the max element
        updn = np.array( [abs(val1[i]),abs(val2[i])] )
        maxelt = np.argmax(updn)

        if maxelt > 0 :
            syst[i] = -1.*val2[i]
        else :
            syst[i] = val1[i]

#        print( ' debug syst ', val1[i], ' ', val2[i], ' syst ', syst[i])

    return syst

def stripFromArrayX( threshold, SF, stat, syst, xbins, ybins ) :
    
    # not all our methods cover the full pT range, so let's strip some of the zeros
    bin = 0
    for x in xbins :
        if x<threshold :
            bin = bin+1

    if len(syst.shape) < 2 :
        return SF[(bin*ybins.size):],stat[(bin*ybins.size):],syst[(bin*ybins.size):],xbins[bin:],ybins
    else :
        return SF[(bin*ybins.size):],stat[(bin*ybins.size):],syst[(bin*ybins.size):,:],xbins[bin:],ybins

# analysers put NaN or 0 at high Et, where we measure in abs(eta)
# NO AVERAGING IS DONE HERE, THE NEGATIVE ETA BINS ARE THROWN
def makeEtaAbs( arr_in, xbins, ybins, cond_pT=0. ) :
    
    arr_out = np.copy( arr_in )
    for ientry in range(0,arr_in.shape[0]) :
        
        xbin  = int(ientry/len(ybins))
        ybin  = ientry % len(ybins)
        
        if xbins[xbin]>=cond_pT and ybins[ybin]<0. :
            
            ybin_new = len(ybins)-ybin-1
            ientry_new = ientry - ybin + ybin_new

            arr_out[ientry] = arr_in[ientry_new]
                    
    return arr_out


