#!/usr/bin/env python

import os,array
from ROOT import *
import numpy as np
from .arrayToText import *
from ctypes import *

val_xmin = +999.
val_xmax = -999.

val_ymin = +999.
val_ymax = -999.

def resetRange() :

    global val_xmin
    global val_xmax
    global val_ymin
    global val_ymax

    val_xmin = +999.
    val_xmax = -999.
    
    val_ymin = +999.
    val_ymax = -999.

def writePlots( listOfGraphs, outdir='.', outname='', doRatio=True, labels=[], otherobjects = [] ) :

    # ------------------------------------------------------------------------------
    # let's make some pretty plots
    # the function takes the following as arguments:
    # - a list of lists of, or a list of TGraphErrors (which you can produce with the getProjection functions below)
    #   -> in a list of lists, each list needs the same length!
    #   -> the nth entries of all lists will be plotted together
    # - out directory name and out filename
    # - whether or not to plot a ratio
    # - the labels to appear in the legend, in the same order and same length as the graphs
    # ------------------------------------------------------------------------------
    if not len(listOfGraphs) :
        print( ' in plotting: passed empmty list ')
        return

    ensure_directory( outdir )

    global val_xmin
    global val_xmax
    global val_ymin
    global val_ymax

    if isinstance( listOfGraphs[0], list ) :

        # we're dealing with a list of lists, so we'll have to slice it
        # all this assumes that each list in the list of lists has the same length
        # i.e. if we pass [ [1,2,3] [4,5,6] ] we'll want to put pairs 1,4, 2,5 and 3,6 on a plot together
        resetRange()

        # check if all have the same length
        len_orig = len(listOfGraphs[0])
        samelength = True
        for l in listOfGraphs :
            samelength = (samelength and len_orig == len(l))
        if not samelength :
            print( ' in plotting: you passed a list of lists, but not all of them have length ', len_orig)
            exit()
        flatlist = sum(listOfGraphs, [])     # Flattens the 2D list

        # compute axis ranges from min/max values of the entire list
        for g in flatlist :
            for j in range(0,g.GetN()) :

                x = c_double()
                y = c_double()
                g.GetPoint(j,x,y)
                val_xmin = min(val_xmin,x.value-g.GetErrorX(j))
                val_xmax = max(val_xmax,x.value+g.GetErrorX(j))
                val_xmin = min(val_xmin,x.value)
                val_xmax = max(val_xmax,x.value)
                if y!=0 :
                    val_ymin = min(val_ymin,y.value-g.GetErrorY(j))
                val_ymax = max(val_ymax,y.value+g.GetErrorY(j))
    #                print( ' x y ', x, y, ' err ', g.GetErrorX(j), g.GetErrorY(j))
        lenall = len(flatlist)
        newlist = [flatlist[i:lenall:len_orig] for i in range(len_orig)] 

        counter = 0
        for i in newlist :
            writePlots(i,outdir,outname+str(counter),doRatio,labels,otherobjects)
            counter = counter+1

        return


    # things for the geometry
    # scaling of labels and axis ranges
    yscaling = (1. / (1. - 0.37) / 8. * 6.) if doRatio else 1.
    ycorrection = (1.- (0.16 if doRatio else 0.))
    for g in listOfGraphs :
        for j in range(0,g.GetN()) :
            
            x = c_double()
            y = c_double()
            g.GetPoint(j,x,y)
            val_xmin = min(val_xmin,x.value)
            val_xmax = max(val_xmax,x.value)
            if y!=0 :
                val_ymin = min(val_ymin,y.value)
            val_ymax = max(val_ymax,y.value)

            val_xmin = min(val_xmin,x.value-g.GetErrorX(j))
            val_xmax = max(val_xmax,x.value+g.GetErrorX(j))
            if y!=0 :
                val_ymin = min(val_ymin,y.value-g.GetErrorY(j))
            val_ymax = max(val_ymax,y.value+g.GetErrorY(j))
            
    firstGraph = 0
    if len(listOfGraphs)>0 and isinstance( listOfGraphs[0], TGraphErrors ) :
        firstGraph = listOfGraphs[0]
#        firstGraph.Print(("all"))
    else :
        print( ' in plotting: no histogram given ', listOfGraphs)
        return 0
    

    currentbin = firstGraph.GetTitle()
    name = currentbin.replace(' ','').replace('>','').replace('<','').replace('}','').replace('{','').replace('#','').replace('.','').replace('-','')    

    # now we start with the plotting

    canvas = TCanvas('can_'+name,'can_'+name,0,0,800,800 if doRatio else 600)
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)
    canvas.Range(0,0,1,1)
    canvas.SetFillColor(0)
    canvas.SetBorderMode(0)
    canvas.SetBorderSize(2)
    canvas.SetLeftMargin(0)
    canvas.SetRightMargin(0)
    canvas.SetTopMargin(0)
    canvas.SetBottomMargin(0)
    canvas.SetFrameBorderMode(0)
    gStyle.SetEndErrorSize(4.)
    
    main = TPad('main_'+name, 'main_'+name,0,0.37 if doRatio else 0,1,1)
    main.Draw()
    main.cd()
    main.SetLeftMargin(0.16)
    main.SetRightMargin(0.05)
    main.SetTopMargin(0.05*yscaling)
    main.SetBottomMargin(0. if doRatio else 0.16)
    main.SetFrameBorderMode(0)
    main.SetFrameBorderMode(0)

    canvas.cd()
    main.cd()

    master = TH1F('master_'+name,'Main Coordinate System',100,val_xmin,val_xmax)
    master.GetXaxis().SetTitle(firstGraph.GetXaxis().GetTitle())
    master.GetYaxis().SetTitle(firstGraph.GetYaxis().GetTitle())
    master.GetXaxis().SetLabelFont(42)
    master.GetXaxis().SetLabelSize(0 if doRatio else 0.05)
    master.GetXaxis().SetTitleSize(0 if doRatio else 0.05)
    master.GetXaxis().SetTitleOffset(1.4)
    master.GetXaxis().SetTitleFont(42)
    master.GetYaxis().SetLabelFont(42)
    master.GetYaxis().SetLabelSize(0.05*yscaling)
    master.GetYaxis().SetTitleSize(0.05*yscaling)
    master.GetYaxis().SetTitleOffset(1.4/yscaling)
    master.GetYaxis().SetTitleFont(42)
    master.GetYaxis().SetRangeUser(0,val_ymax+val_ymax*0.05)
#    master.GetYaxis().SetRangeUser(0.,val_ymax+val_ymax*0.05)
#    if val_ymin<0.1 :
#        print( ' setting y axis range to lower ')
#        master.GetYaxis().SetRangeUser(0.,0.05)
#    else :
#        master.GetYaxis().SetRangeUser(0.79, 1.12)
#    master.GetYaxis().SetRangeUser(0.55, 1.45)
    master.Draw('hist')

    tex = TLatex()
    tex.SetNDC(1)
    tex.SetTextFont(72)
    tex.SetLineWidth(2)
    tex.SetTextSize(0.05*yscaling)
    tex.DrawLatex(0.2,1.-(1.-0.88)/ycorrection,'ATLAS')
    tex.SetTextFont(42)
    tex.SetTextSize(0.05*yscaling)
    tex.DrawLatex(0.328,1.-(1.-0.88)/ycorrection,'Internal')
    tex.SetTextSize(0.0425*yscaling)
    tex.DrawLatex(0.2,1.-(1-0.82)/ycorrection,currentbin)

    nLegs = 0
    for ig in range(0,len(listOfGraphs)) :
        if len(labels)>ig :
            label = ' '+labels[ig]
            if not label.isspace() :
                nLegs = nLegs+1

    leg = TLegend(0.52,1.-(1.-0.92+0.04*yscaling*nLegs)/ycorrection,0.9,1.-(1.-0.92)/ycorrection)
    leg.SetNColumns(1)
    leg.SetBorderSize(0)
    leg.SetTextSize(0.04*yscaling)
    leg.SetLineColor(1)
    leg.SetLineStyle(1)
    leg.SetLineWidth(1)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    SetOwnership(leg, False)

    # for each graph we're going to store the ratio
    ratios  = []
    ngraphs = 0
    doRelUnc = False
    # looping all graphs
    for graph in listOfGraphs :
        
        canvas.cd()
        main.cd()
        if not graph :
            print( ' in plotting: couldn\'t find graph ', graph)
            continue

        # and deal with legend and ratio
        label = graph.GetTitle()
        if len(labels)>ngraphs : # len ratios can be used as a counter here
            label = ' '+labels[ngraphs]
            if not label.isspace() :
                entry = leg.AddEntry(graph,label,'lep')

        # draw the graph
        graph.SetTitle('graph')
        graph.Draw('P same')

        ngraphs = ngraphs+1

        # the ratio. We handle the first graph further below (it'll be displayed as a filled area)
        if doRatio and graph != firstGraph :
            
            graph_ratio = graph.Clone()
            if graph.GetN() != firstGraph.GetN() :
                print( ' in plotting: inconsistent bins (firstGraph ', firstGraph.GetN(), ' vs current ', graph.GetN(), ')')
                doRatio = False
                doRelUnc = True
                continue
                
            for x in range(0,graph.GetN()) :
                val_x_num = c_double()
                val_y_num = c_double()
                graph_ratio.GetPoint(x,val_x_num,val_y_num)
                val_x_denom = c_double()
                val_y_denom = c_double()
                firstGraph.GetPoint(x,val_x_denom,val_y_denom)
                
                if val_y_denom.value != 0 :
                    graph_ratio.SetPoint(x,val_x_num.value,val_y_num.value/val_y_denom.value)
                else :
                    graph_ratio.SetPoint(x,val_x_num.value, 999.)

                err_x_num = graph_ratio.GetErrorX(x)
                err_y_num = graph_ratio.GetErrorY(x)
                err_x_denom = firstGraph.GetErrorX(x)
                err_y_denom = firstGraph.GetErrorY(x)
                
                # including the denominator uncertainty
                #                graph_ratio.SetPointError(x,err_x_num, \ 
                #                                          val_y_num/val_y_denom * np.sqrt( (err_y_num/val_y_num)**2 + (err_y_denom/val_y_denom)**2 ) )

                # excluding the denominator uncertaity
                if val_y_denom.value != 0 :
                    graph_ratio.SetPointError(x,err_x_num, err_y_num/val_y_denom.value )
                else :
                    graph_ratio.SetPointError(x,err_x_num, 0.)
                
            ratios.append(graph_ratio)


    ngraphs = 0
    # do relative unc if ratios are not possible
    for graph in listOfGraphs :
        
        # the ratio. We handle the first graph further below (it'll be displayed as a filled area)
        if doRelUnc :

            graph_ratio = graph.Clone()
            for x in range(0,graph.GetN()) :

                val_x_num = graph_ratio.GetErrorX(x)
                val_y_num = graph_ratio.GetErrorY(x)
                val_x_denom = c_double()
                val_y_denom = c_double()
                graph_ratio.GetPoint(x,val_x_denom,val_y_denom)

                if val_y_denom != 0 :
                    graph_ratio.SetPoint(x,val_x_denom.value,val_y_num/val_y_denom.value)
                else :
                    graph_ratio.SetPoint(x,val_x_denom, 999.)

                err_x_num = graph_ratio.GetErrorX(x)
                # excluding the denominator uncertaity
                if val_y_denom!=0 :
                    graph_ratio.SetPointError(x,err_x_num, 0. )
                else :
                    graph_ratio.SetPointError(x,err_x_num, 0.)

            ratios.append(graph_ratio)
    
    # draw the first graph again so it's on top
    firstGraph.SetTitle('graph')
    firstGraph.Draw('P same')
    leg.Draw('same')

    # if we don't want to do a ratio plot we're done
    if ( doRatio or doRelUnc ) and len(ratios)>0 :

        canvas.cd()
        sub = TPad('sub_'+name, 'sub_'+name,0,0,1,0.37)
        sub.Draw()
        sub.Range(0,0,1,1)
        sub.SetFillColor(0)
        sub.SetFillStyle(4000)
        sub.SetBorderMode(0)
        sub.SetBorderSize(0)
        sub.SetGridy()
        sub.SetTickx(1)
        sub.SetTicky(1)
        sub.SetLeftMargin(0.16)
        sub.SetRightMargin(0.05)
        sub.SetTopMargin(0.03214286)
        sub.SetBottomMargin(0.3428572)
        sub.SetFrameBorderMode(0)
        sub.cd()        
        
        master_sub = master.Clone('master_sub')
        master_sub.GetYaxis().SetRangeUser(0.95,1.05)
        if abs(val_ymin - val_ymax)>1. :
#            print( ' ymin ymax ', val_ymin, val_ymax)
            master_sub.GetYaxis().SetRangeUser(0.85,1.15)
        if doRelUnc :
            master_sub.GetYaxis().SetRangeUser(0.,.025)
            
        master_sub.SetLineColor(0)
        master_sub.GetXaxis().SetLabelSize(0.1071429)
        master_sub.GetXaxis().SetTitleSize(0.1071429)
        master_sub.GetXaxis().SetTickLength(0.06428572)
        master_sub.GetXaxis().SetTitleOffset(1.4)
        master_sub.GetXaxis().SetTitleFont(42)
        master_sub.GetYaxis().SetTitle('Ratio ')
        master_sub.GetYaxis().SetLabelSize(0.1071429)
        master_sub.GetYaxis().SetTitleSize(0.1071429)
        master_sub.GetYaxis().SetTickLength(0.05142857)
        master_sub.GetYaxis().SetTitleOffset(0.6533333)
        master_sub.GetYaxis().SetTitleFont(42)
        master_sub.Draw()
        
        
        # draw the uncertainty of the first graph as band
        first_ratio = firstGraph.Clone()
#        first_ratio.Print(("all"))
#        print( ' PHILIP comparing the ns ', first_ratio.GetN(), ' and ', firstGraph.GetN())
        for x in range(0,first_ratio.GetN()) :
            val_x_SF = c_double()
            val_y_SF = c_double()
            firstGraph.GetPoint(x,val_x_SF,val_y_SF)

            val_x_num = c_double()
            val_y_num = c_double()
            first_ratio.GetPoint(x,val_x_num,val_y_num)
            err_x_num = first_ratio.GetErrorX(x)
            err_y_num = first_ratio.GetErrorY(x)
            first_ratio.SetPoint(x, val_x_num, 1.)
            if val_y_num.value != 0 :
                first_ratio.SetPointError(x,err_x_num, err_y_num/val_y_num.value )
            else :
                first_ratio.SetPointError(x,err_x_num, 0.)
#            first_ratio.SetPointError(x,err_x_num, 0.04)

        first_ratio.SetFillColor(kOrange)
        first_ratio.SetLineColor(kOrange)
        first_ratio.SetTitle('graph')
        first_ratio.Draw('2 same')

        for iratio in ratios :
            iratio.SetTitle('graph')
            iratio.Draw('P same')

    gPad.RedrawAxis()

    main.cd()
    for o in otherobjects :
        o.Draw()
            
    canvas.Print((outdir+'/'+outname+'.png'))
    #canvas.Print((outdir+'/'+outname+'.C'))

    canvas.Destructor()
    return 

def makeTH2F( val, err, x, y, name = 'hist' ) :
    xbins = array.array('d',x)
    ybins = array.array('d',y)
    if xbins[-1]<30000. :
        xbins.append(xbins[-1]+5000.)
    else :
        xbins.append(2.*xbins[-1])
    ybins.append(2.47)
    hist = TH2F(name, name, len(xbins)-1,xbins,len(ybins)-1,ybins)
    return makeTH2( val, err, x, y, hist ) 

def makeTH2D( val, err, x, y, name = 'hist' ) :
    xbins = array.array('d',x)
    ybins = array.array('d',y)
    if xbins[-1]<30000. :
        xbins.append(xbins[-1]+5000.)
    else :
        xbins.append(2.*xbins[-1])
    ybins.append(2.47)
    hist = TH2D(name, name, len(xbins)-1,xbins,len(ybins)-1,ybins)
    return makeTH2( val, err, x, y, hist ) 

def makeTH2( val, err, x, y ) :
    return makeTH2D( val, err, x, y, 'hist' )
    
def makeTH2( val, err, x, y, hist ) :

    # function to convert the 1d arrays back to a TH2
    xbins = array.array('d',x)
    ybins = array.array('d',y)

    # usually our array holding the bins only contain lower edges
    #    print( ' before val ', val.shape, ' x ', x.shape, ' y ', y.shape)
    #    if val.ndim = 1 :
    #    if (len(x),len(y)) != val.shape :
    #        x = x[:-1].copy()
    #        y = y[:-1].copy()
    #    print( ' after val ', val.shape, ' x ', x.shape, ' y ', y.shape)
    
    val2d = val.reshape(len(x),len(y))
    err2d = err.reshape(len(x),len(y))

    if val2d.shape != err2d.shape :
        print( ' in plotting: you passed inconsistent value and error arrays ')
        return
        
    if val2d.shape[0] != x.shape[0] or val2d.shape[1] != y.shape[0] :
        print( ' in plotting: you passed arrays inconsistent with your bins ')
        return

    for i in range(0,x.shape[0]) :
        for j in range(0,y.shape[0]) :

            hist.SetBinContent( i+1, j+1, val2d[i,j] )
            hist.SetBinError  ( i+1, j+1, err2d[i,j] )


    hist.GetXaxis().SetTitle('E_{T} [MeV]')
    hist.GetYaxis().SetTitle('#eta')

    hist.SetDirectory(0)
    return hist


def getProjectionX( histo, color=1, shift=0. ) :
    return getProjection( histo, True, color, shift )
def getProjectionY( histo, color=1, shift=0. ) :
    return getProjection( histo, False, color, shift )

def getProjection( histo, projX=False, color=1, shift=0. ) :
    # takes a TH2 and slices it in bins of X or Y (where projX=True means we're slicing in Y)
    # the TH2 is converted into a list of TGraphErrors
    # optionally the marker/line color is set, and it's possible to shift all values a bit for better display
    # the bin boundaries of the variable we sliced in are stored as title

    outlist = []
    xAxis = histo.GetYaxis() if projX else histo.GetXaxis()
    
    val_xmin = +999.
    val_xmax = -999.
    
    val_ymin = +999.
    val_ymax = -999.
    
    for x in range(0,xAxis.GetNbins()) :
        
        yAxis = histo.GetXaxis() if projX else histo.GetYaxis()
        nbins = yAxis.GetNbins()
        
        val_x    = array.array('d',[0 for i in range(0,100)])
        val_y    = array.array('d',[0 for i in range(0,100)])
        val_ex   = array.array('d',[0 for i in range(0,100)])
        val_ey   = array.array('d',[0 for i in range(0,100)])
        val_zero = array.array('d',[0 for i in range(0,100)])

        for y in range(0,nbins) :
            
            bincenter = yAxis.GetBinCenter(y+1)
            binwidth  = yAxis.GetBinWidth (y+1)
            
            val_x   [y] = (bincenter+shift)/(1000. if projX else 1.)
            val_ex  [y] = binwidth/2.      /(1000. if projX else 1.)
            val_y   [y] = histo.GetBinContent(y+1,x+1) if projX else histo.GetBinContent(x+1,y+1)
            val_zero[y] = 0
            
            sqerr = ( histo.GetBinError(y+1,x+1) if projX else histo.GetBinError(x+1,y+1) )**2
            val_ey  [y] = np.sqrt(sqerr)
        
        currentbin = ''
        if not projX :
            currentbin = '%1.0f < E_{T} < %1.0f GeV' % (xAxis.GetBinLowEdge(x+1)/1000.,xAxis.GetBinUpEdge(x+1)/1000.)
        else :
            currentbin = '%1.2f < #eta < %1.2f'      % (xAxis.GetBinLowEdge(x+1),xAxis.GetBinUpEdge(x+1))
                
        gr = TGraphErrors(nbins,val_x,val_y,val_ex,val_ey)
        gr.SetMarkerSize(1.2)
        gr.SetMarkerStyle(20)
        gr.SetLineWidth(2)
        gr.SetMarkerColor(color)
        gr.SetLineColor(color)
        gr.SetFillColor(color)
        gr.SetTitle(currentbin)

        gr.SetTitle(currentbin)
        gr.GetXaxis().SetTitle(yAxis.GetTitle())
        gr.GetYaxis().SetTitle('Scale factor')
#        gr.GetYaxis().SetTitle('Data Efficiency')

        outlist.append(gr)

    return outlist

# how I wanted to set it up but abandoned 
#def getItem( hist, hunc=0, name='', color=1, shift=0.0 ) :
#    
#    item = plotItem()
#    item.name = name
#    item.hist = hist
#    #    item.hunc = hunc
#    item.color = color
#    item.shift = shift
#    return item
#
#class plotItem() :
#
#    def __init__(self) :
#        self.name = ''
#        self.title = ''
#        self.hist = 0
#        #        self.hunc = 0 # separate histo for errors if needed
#        self.color       = 1
#        self.shift       = 0.
#
#        self.linewidth   = 1
#        self.markersize  = 1.2
#        self.markerstyle = 20
#        
#    def __str__(self) :
#        return str(self.__class__) + ': ' + str(self.__dict__)
#
#    pass

def canvasAndPads( name, doRatio ) :
    # getting the canvas and pads
    yscaling = (1. / (1. - 0.37) / 8. * 6.) if doRatio else 1.
    ycorrection = (1.- (0.16 if doRatio else 0.))
    
    return canvas, main


def plotCovariance( outfilename, arr_in, x, y ) :

    # check if arr_in is square
    if arr_in.shape[0] != arr_in.shape[1] :
        # for sure it's not a covariance matrix 
        print( ' in plotting: you didn\'t pass a square matrix ')
        return

    # check if length is correct
    if arr_in.shape[0] != len(x)*len(y) :
        print( ' in plotting: your bins don\'t match the matrix ')
        return

    #SetAtlasStyle()
#    TGaxis.SetMaxDigits(3)
#    gStyle.SetPalette(1)
#    stops = array.array('d', [ 0.00, 0.34, 0.61, 0.84, 1.00 ])
#    red   = array.array('d', [ 0.00, 0.00, 0.87, 1.00, 0.51 ])
#    green = array.array('d', [ 0.00, 0.81, 1.00, 0.20, 0.00 ])
#    blue  = array.array('d', [ 0.51, 1.00, 0.12, 0.00, 0.00 ])
#    TColor.CreateGradientColorTable(5, stops, red, green, blue, 255)
    gStyle.SetNumberContours(255)
    gStyle.SetPaintTextFormat('1.3f')

    # setting palette to kBird
    gStyle.SetPalette(54)


    # now we make several TH2 from the big array
    firstMatrix = 0
    splitMatrix = int(arr_in.shape[0]/len(y))

    extraCan = 4./3.
    can = TCanvas('correlations','correlations', int(extraCan*600), 600 )
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)
    can.Range(0,0,1,1)
    can.SetFillColor(0)
    can.SetBorderMode(0)
    can.SetBorderSize(2)
    can.SetFrameBorderMode(0)
    can.SetLeftMargin(0.16)
    can.SetRightMargin(0.05)
    can.SetTopMargin(0.05)
    can.SetBottomMargin(0.16)
    can.SetFrameBorderMode(0)
    can.SetFrameBorderMode(0)
    can.cd()

    zmin = -1. #np.amin(arr_in)
    zmax = +1. #np.amax(arr_in)

    listOfRootStuff = [] # so PyRoot doesn't delete it after the loop
    for x1 in range(0,splitMatrix-1) : # since the array doesn't contain the upper edge of the last bin, this means we don't plot it (but I anyways don't want to)
        for x2 in range(0,splitMatrix-1) :

            nbins_eta = len(y)
            submatrix = arr_in[x1*nbins_eta:(x1+1)*nbins_eta,x2*nbins_eta:(x2+1)*nbins_eta]

            ybins = array.array('d',y)
            ybins.append(2.47)

            # create a TH2 with appropriate bin boundaries and fill in the submatrix
            hist = TH2F('hist_%d_%d' % (x1,x2),'hist_%d_%d' % (x1,x2),len(ybins)-1,ybins,len(ybins)-1,ybins)
            for i in range(0,y.shape[0]) :
                for j in range(0,y.shape[0]) :
                    hist.SetBinContent( i+1, j+1, submatrix[i,j] )
                    hist.SetBinError  ( i+1, j+1, submatrix[i,j] )
            listOfRootStuff.append(hist)

            # keep the first matrix for later
            if x1==0 and x1==x2 :
                firstMatrix = hist

            # create one pad to hold one correlation matrix TH2
            can.cd()
            val_x1 = ( x[x1]   - x[0] ) / ( x[-1] - x[0] ) / extraCan*( 1 - can.GetLeftMargin() - can.GetRightMargin() ) 
            val_x2 = ( x[x1+1] - x[0] ) / ( x[-1] - x[0] ) / extraCan*( 1 - can.GetLeftMargin() - can.GetRightMargin() ) + can.GetLeftMargin()
            val_y1 = ( x[x2]   - x[0] ) / ( x[-1] - x[0] )           *( 1 - can.GetLeftMargin() - can.GetRightMargin() ) 
            val_y2 = ( x[x2+1] - x[0] ) / ( x[-1] - x[0] )           *( 1 - can.GetLeftMargin() - can.GetRightMargin() ) + can.GetBottomMargin()

            if x1 != 0 :
                val_x1 = val_x1 + can.GetLeftMargin()
            if x1+1 == splitMatrix :
                val_x2 = val_x2 + can.GetRightMargin()
            if x2 != 0 :
                val_y1 = val_y1 + can.GetBottomMargin()
            if x2+1 == splitMatrix :
                val_y2 = val_y2 + can.GetTopMargin()

            pad = TPad( 'pad_%d_%d' % (x1, x2), 'pad_%d_%d' % (x1, x2), val_x1, val_y1, val_x2, val_y2 )
            pad.SetLeftMargin(0.16)
            pad.SetRightMargin(0.05)
            pad.SetTopMargin(0.05)
            pad.SetBottomMargin(0.16)
            pad.SetFrameBorderMode(0)
            pad.SetFrameBorderMode(0)
            pad.SetFillColor(0)
            if x1 == 0 :
                pad.SetLeftMargin( pad.GetLeftMargin()/(val_x2-val_x1) )
            else :
                pad.SetLeftMargin(0.)
            if x1+1 == splitMatrix :
                pad.SetRightMargin( pad.GetRightMargin()/(val_x2-val_x1) )
            else :
                pad.SetRightMargin(0.)
            if x2 == 0 :
                pad.SetBottomMargin( pad.GetBottomMargin()/(val_y2-val_y1) )
            else :
                pad.SetBottomMargin(0.)
            if x2+1 == splitMatrix :
                pad.SetTopMargin( pad.GetTopMargin()/(val_y2-val_y1) )
            else :
                pad.SetTopMargin(0.)

            pad.Draw()
            pad.cd()
            listOfRootStuff.append(pad)

            hist.GetZaxis().SetRangeUser(zmin,zmax)
            hist.GetXaxis().SetNdivisions(000)
            hist.GetYaxis().SetNdivisions(000)
            hist.Draw('col')

    fractionX = ( 1 - can.GetLeftMargin()- can.GetRightMargin() ) / (x[0]-x[-1]) / extraCan * (x[0]-x[1])
    fractionY = ( 1 - can.GetBottomMargin()- can.GetTopMargin() ) / (x[0]-x[-1])            * (x[0]-x[1])
    padForPalette = TPad( 'padForPalette', 'padForPalette', 1./extraCan+.005, 0., 1., 1. )
    padForPalette.SetLeftMargin(0.16)
    padForPalette.SetRightMargin(0.05)
    padForPalette.SetTopMargin(0.05)
    padForPalette.SetBottomMargin(0.16)
    padForPalette.SetFrameBorderMode(0)
    padForPalette.SetFrameBorderMode(0)
    padForPalette.SetFillColor(0)
    palette = TPaletteAxis( 0.1, padForPalette.GetBottomMargin(), (1-padForPalette.GetLeftMargin())/3., (1-padForPalette.GetTopMargin()), firstMatrix )
    can.cd()
    padForPalette.Draw()
    padForPalette.cd()
    
    ratioPadScaling = 1. / (1. - 1./extraCan) / extraCan
#    palette.GetAxis().SetLabelSize  ( palette.GetAxis().GetLabelSize()*ratioPadScaling*3./4. )
#    palette.GetAxis().SetLabelOffset( palette.GetAxis().GetLabelOffset()*ratioPadScaling )
    palette.Draw()
    palette.GetAxis().SetLabelSize  ( 0.1125 )
    palette.GetAxis().SetLabelOffset( 0.015  )
    palette.Draw()
    
    padOnTop = TPad( 'padOnTop', 'padOnTop', 0., 0., 1., 1. )
    padOnTop.SetFillColor(0)
    padOnTop.SetFillStyle(0)
    padOnTop.SetLeftMargin(0.16)
    padOnTop.SetRightMargin(0.05)
    padOnTop.SetTopMargin(0.05)
    padOnTop.SetBottomMargin(0.16)
    padOnTop.SetFrameBorderMode(0)
    padOnTop.SetFrameBorderMode(0)
    can.cd()
    padOnTop.Draw()
    padOnTop.cd()

    x = np.true_divide(x, 1000.)
    xAxis = TGaxis(can.GetLeftMargin(),can.GetBottomMargin(),can.GetLeftMargin()+( 1 - can.GetLeftMargin()- can.GetRightMargin() ) / extraCan,can.GetBottomMargin(),x[0],x[-1],305,'')
    xAxis.SetTitle('E_{T} [GeV]')
    xAxis.Draw('same')

    yAxis = TGaxis(can.GetLeftMargin(),can.GetBottomMargin(),can.GetLeftMargin(),can.GetBottomMargin()+( 1 - can.GetBottomMargin()- can.GetTopMargin() ),x[0],x[-1],305,'')
    yAxis.SetTitle('E_{T} [GeV]')
    yAxis.Draw('same')
    
    etaXAxis = TGaxis(can.GetLeftMargin(),.1,can.GetLeftMargin()+fractionX,.1,-2.47,2.47,305,'')
    etaXAxis.SetLabelSize(etaXAxis.GetLabelSize()/2.)
    etaXAxis.SetTitle('#eta^{cl}')
    etaXAxis.Draw('same')
    
    etaYAxis = TGaxis(.1,can.GetBottomMargin(),.1,can.GetBottomMargin()+fractionY,-2.47,2.47,305,'')
    etaYAxis.SetLabelSize(etaYAxis.GetLabelSize()/2.)
    etaYAxis.SetTitle('#eta^{cl}')
    etaYAxis.Draw('same')
    
    can.Print((outfilename))
    #can.Print((outfilename.replace('.png','.C')))
    return

