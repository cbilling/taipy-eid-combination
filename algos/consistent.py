import numpy as np


def consistentBinning(x1, y1, x2, y2):
    x_consistent = np.array_equal(x1, x2)
    y_consistent = np.array_equal(y1, y2)
    x = y = 0
    if x_consistent and y_consistent:
        x = x1
        y = y1

    return x, y
