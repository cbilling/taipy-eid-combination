import sys, operator
from .tools.arrayToText import *
from .tools.statTools import *
from .tools.rootToArray import *
from .tools.plotting import *
import numpy as np

def chi2(indir_meth1,indir_meth2,indir_avg,x,y):

    # read the values from the previous steps for two methods
    SF   = []
    stat = []
    syst = []
    xbin = []
    ybin = []
    for indir in [indir_meth1, indir_meth2] :
        SF_meth, stat_meth, syst_meth, x_meth, y_meth = readArray_SF_stat_syst( indir, x, y )
        SF.append(SF_meth)
        stat.append(stat_meth)
        syst.append(syst_meth)
        xbin.append(x_meth)
        ybin.append(y_meth)

    SF_avg, stat_avg, syst_avg, x_avg, y_avg = readArray_SF_stat_syst( indir_avg,x,y )

   
    listofchi2 = []
    listofchi2_eta = []

    thischi2 = [get_chi2( SF_avg, 0, SF, stat, syst ),SF_avg.size*(len(SF)-1),-999]
    print('    chi2 (all bins): ', thischi2[0],'/',thischi2[1])
    listofchi2.append(thischi2)
    listofchi2_eta.append(thischi2)

    for pT in range(len(x_avg)) :
        #only valid bin for combination to check chi2
        tmpSF   = []
        tmpstat = []
        tmpsyst = []
        tmpx    = []
        tmpy    = []
        for imeth in range(len(SF)) :
            SF_meth, stat_meth, syst_meth, x_meth, y_meth = stripBins( SF[imeth], stat[imeth], syst[imeth], xbin[imeth], ybin[imeth], pT, pT+1)
            tmpSF  .append(SF_meth)
            tmpstat.append(stat_meth)
            tmpsyst.append(syst_meth)
            tmpx   .append(x_meth)
            tmpy   .append(y_meth)
        tmpSF_avg, tmpstat_avg, tmpsyst_avg, tmpx_avg, tmpy_avg = stripBins( SF_avg, stat_avg, syst_avg, x_avg, y_avg, pT, pT+1 )

        thischi2 = [get_chi2( tmpSF_avg, 0, tmpSF, tmpstat, tmpsyst),tmpSF_avg.size*(len(tmpSF)-1),x_avg[pT]]
        print('    chi2 (pT=', x_avg[pT],'): ', thischi2[0],'/',thischi2[1])
        listofchi2.append(thischi2)
    
    for eta in range(len(y_avg)) :
        
        tmpSF   = []
        tmpstat = []
        tmpsyst = []
        tmpx    = []
        tmpy    = []
        for imeth in range(len(SF)) :
            SF_meth, stat_meth, syst_meth, x_meth, y_meth = stripBins( SF[imeth], stat[imeth], syst[imeth], xbin[imeth], ybin[imeth], 0, -1, eta, eta+1)
            tmpSF  .append(SF_meth)
            tmpstat.append(stat_meth)
            tmpsyst.append(syst_meth)
            tmpx   .append(x_meth)
            tmpy   .append(y_meth)
        tmpSF_avg, tmpstat_avg, tmpsyst_avg, tmpx_avg, tmpy_avg = stripBins( SF_avg, stat_avg, syst_avg, x_avg, y_avg, 0, -1, eta, eta+1)

        thischi2 = [get_chi2( tmpSF_avg, 0, tmpSF, tmpstat, tmpsyst),tmpSF_avg.size*(len(tmpSF)-1),y_avg[eta]]
        print( '    chi2 (eta=', y_avg[eta],'): ', thischi2[0],'/',thischi2[1])
        listofchi2_eta.append(thischi2)
    
    return np.array(listofchi2), np.array(listofchi2_eta)