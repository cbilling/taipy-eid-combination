def pathFinder(year):
    base_path = "combination_inputs"

    # ZISO
    zIsoPath = base_path + f"/ziso/inclusive/scalefactors_ziso_example_stat{year}.root"

    # ZMASS
    zMassPath = base_path + f"/zmass/inclusive/scalefactors_zmass_for_comb_{year}.root"

    # JPSI
    jPsiPath = base_path + f"/jpsi/lowPt/scalefactors_jpsi_for_comb_{year}.root"

    return zIsoPath, zMassPath, jPsiPath


def pathFinderPrebin(year, extraBin):
    print(extraBin)
    base_path = "combination_inputs"
    if extraBin.split("_")[0] == "no":
        prebinDir = "lowPt"
    else:
        prebinDir = "lowPtExtraBin"

    print("preBinDir: ", prebinDir)
    # ZISO
    zIsoPath = base_path + f"/ziso/{prebinDir}/scalefactors_ziso_for_comb_{year}.root"

    # ZMASS
    zMassPath = (
        base_path + f"/zmass/{prebinDir}/scalefactors_zmass_for_comb_{year}.root"
    )

    # JPSI
    jPsiPath = base_path + f"/jpsi/{prebinDir}/scalefactors_jpsi_for_comb_{year}.root"

    return zIsoPath, zMassPath, jPsiPath
