import sys, operator
from .tools.rootToArray import *
from .tools.arrayToText import *
import numpy as np


def readZiso(infile, ID, year, removeNeg="no"):
    print("infile: ", infile)
    print("ID: ", ID)
    # define the histograms holding the central value, and the systematic variations
    basename = "SF/SF_%s_%s_%s_%s_%s_%s_%s_%s_%s_DataDriven_Rel21_Smooth_vTest"

    # for the Ziso the templates and their syst variations is a complicated if-else thing
    # I'll loop a couple of templates and later stitch them together
    templates = [
        "TemplateVar1",  # pT<20 GeV
        "Template_ge1_sel_noloose",  # 20<pT<25 GeV, endcap
        "Template_ge1_sel_noloose",  # 20<pT<25 GeV, barrel
        "Template_ge1_sel_noloose",  # pT>25 GeV
    ]
    templates_syst = [
        "Template_ge1_sel_noloose",  # pT<20 GeV
        "Template6",  # 20<pT<25 GeV, endcap
        "TemplateVar1",  # 20<pT<25 GeV, barrel
        "Template6",  # pT>25 GeV
    ]
    templates_pTeta = [  # later we'll loop and fill values if pT>=1st entry and eta<=2nd entry. The order of these is therefore important
        [0.0, 99.0],  # pT<20 GeV
        [20.0, 99.0],  # 20<pT<25 GeV, endcap
        [20.0, 1.52],  # 20<pT<25 GeV, barrel
        [25.0, 99.0],  # pT>25 GeV
    ]

    # np arrays to hold the combined values
    SF = np.array([])
    stat = np.array([])
    syst = np.array([])
    x = np.array([])
    y = np.array([])

    for var in range(0, len(templates)):
        list = []
        list.append(
            basename
            % (
                "CentralValue",
                templates[var],
                "Tag1",
                "mll75105",
                "ProbeIsoCut0p5",
                "topoetcone30",
                "doSubractMCeventsInTailScaleFactor1.0",
                "RealElectronContaminationScalingFactor1p0",
                ID,
            )
        )  # SF
        list.append(
            basename
            % (
                "StatError",
                templates[var],
                "Tag1",
                "mll75105",
                "ProbeIsoCut0p5",
                "topoetcone30",
                "doSubractMCeventsInTailScaleFactor1.0",
                "RealElectronContaminationScalingFactor1p0",
                ID,
            )
        )  # stat
        list.append(
            basename
            % (
                "CentralValue",
                templates[var],
                "Tag1AndTagIso",
                "mll75105",
                "ProbeIsoCut0p5",
                "topoetcone30",
                "doSubractMCeventsInTailScaleFactor1.0",
                "RealElectronContaminationScalingFactor1p0",
                ID,
            )
        )  # tag variation
        list.append(
            [
                basename
                % (
                    "CentralValue",
                    templates[var],
                    "Tag1",
                    "mll70110",
                    "ProbeIsoCut0p5",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor1.0",
                    "RealElectronContaminationScalingFactor1p0",
                    ID,
                ),  # mll variation
                basename
                % (
                    "CentralValue",
                    templates[var],
                    "Tag1",
                    "mll80100",
                    "ProbeIsoCut0p5",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor1.0",
                    "RealElectronContaminationScalingFactor1p0",
                    ID,
                ),
            ]
        )
        list.append(
            [
                basename
                % (
                    "CentralValue",
                    templates[var],
                    "Tag1",
                    "mll75105",
                    "ProbeIsoCut0p4",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor1.0",
                    "RealElectronContaminationScalingFactor1p0",
                    ID,
                ),  # iso threshold
                basename
                % (
                    "CentralValue",
                    templates[var],
                    "Tag1",
                    "mll75105",
                    "ProbeIsoCut0p6",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor1.0",
                    "RealElectronContaminationScalingFactor1p0",
                    ID,
                ),
            ]
        )
        list.append(
            basename
            % (
                "CentralValue",
                templates[var],
                "Tag1",
                "mll75105",
                "ProbeIsoCut0p5",
                "topoetcone40",
                "doSubractMCeventsInTailScaleFactor1.0",
                "RealElectronContaminationScalingFactor1p0",
                ID,
            )
        )  # discrim variable
        list.append(
            [
                basename
                % (
                    "CentralValue",
                    templates[var],
                    "Tag1",
                    "mll75105",
                    "ProbeIsoCut0p5",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor0.7",
                    "RealElectronContaminationScalingFactor1p0",
                    ID,
                ),  # signal contamination
                basename
                % (
                    "CentralValue",
                    templates[var],
                    "Tag1",
                    "mll75105",
                    "ProbeIsoCut0p5",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor1.3",
                    "RealElectronContaminationScalingFactor1p0",
                    ID,
                ),
            ]
        )
        list.append(
            [
                basename
                % (
                    "CentralValue",
                    templates[var],
                    "Tag1",
                    "mll75105",
                    "ProbeIsoCut0p5",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor1.0",
                    "RealElectronContaminationScalingFactor0p7",
                    ID,
                ),  # 2nd signal contamination
                basename
                % (
                    "CentralValue",
                    templates[var],
                    "Tag1",
                    "mll75105",
                    "ProbeIsoCut0p5",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor1.0",
                    "RealElectronContaminationScalingFactor1p3",
                    ID,
                ),
            ]
        )
        for t in range(0, var):
            list.append(
                basename
                % (
                    "CentralValue",
                    templates[var],
                    "Tag1",
                    "mll75105",
                    "ProbeIsoCut0p5",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor1.0",
                    "RealElectronContaminationScalingFactor1p0",
                    ID,
                )
            )  # SF
        list.append(
            basename
            % (
                "CentralValue",
                templates_syst[var],
                "Tag1",
                "mll75105",
                "ProbeIsoCut0p5",
                "topoetcone30",
                "doSubractMCeventsInTailScaleFactor1.0",
                "RealElectronContaminationScalingFactor1p0",
                ID,
            )
        )  # template variation
        for t in range(var + 1, len(templates)):
            list.append(
                basename
                % (
                    "CentralValue",
                    templates[var],
                    "Tag1",
                    "mll75105",
                    "ProbeIsoCut0p5",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor1.0",
                    "RealElectronContaminationScalingFactor1p0",
                    ID,
                )
            )  # SF

        # now we fill the histograms into arrays
        SF_var, stat_var, syst_var, x_var, y_var = getArray_SF_stat_syst(infile, list)

        # stitching together the various template choices
        if var == 0:
            x = x_var
            y = y_var
            SF = np.zeros(SF_var.shape)
            stat = np.zeros(stat_var.shape)
            syst = np.zeros(syst_var.shape)

        def replaceValues(arr_orig, arr_new, xbins, ybins, cond_pT, cond_eta):
            arr_out = np.copy(arr_orig)
            if arr_orig.shape != arr_new.shape:
                print(" in replaceValues: passing incompatible arrays")
                return arr_out

            for ientry in range(0, arr_orig.shape[0]):
                xbin = int(ientry / len(ybins))
                ybin = ientry % len(ybins)

                if (
                    xbins[xbin] >= cond_pT
                    and abs(ybins[ybin]) + np.sign(ybins[ybin]) * sys.float_info.epsilon
                    <= cond_eta
                ):  # +sign*epsilon is there since the array only contains the lower bin boundary
                    #                if var ==2 :
                    #                    print( ' in var ', var, ' replacing ', xbins[xbin], ' ', ybins[ybin])
                    arr_out[ientry] = arr_new[ientry]

            return arr_out

        SF = replaceValues(
            SF, SF_var, x, y, templates_pTeta[var][0], templates_pTeta[var][1]
        )
        stat = replaceValues(
            stat, stat_var, x, y, templates_pTeta[var][0], templates_pTeta[var][1]
        )
        syst = replaceValues(
            syst, syst_var, x, y, templates_pTeta[var][0], templates_pTeta[var][1]
        )

    # now the SF, stat, syst, x, y arrays should correspond to what we want for the measurement

    # let's strip off the bins below 15 GeV
    SF, stat, syst, x, y = stripFromArrayX(15.0, SF, stat, syst, x, y)

    # Hanlin has NaNs at high Et where we do the measurement in abs(eta)
    SF = makeEtaAbs(SF, x, y, 150.0)
    stat = makeEtaAbs(stat, x, y, 150.0)
    syst = makeEtaAbs(syst, x, y, 150.0)

    # and we're done! This stuff we can now store
    # writeArray_SF_stat_syst( outdir, SF, stat, syst, x, y )

    arr_out = np.column_stack((SF, stat, syst))
    print("removeNeg: ", removeNeg)
    if removeNeg != "no":
        posy = []
        for b in y:
            if b >= 0:
                posy.append(b)
        y = posy
        print(arr_out)
        arr_out = arr_out[len(posy) :, :]
        print(arr_out)
        print(y, posy)
    return arr_out, x, y


def convertGeVtoMeV(x):
    # print("before ", x)
    x = np.ravel(x)
    scalefactor = 1000
    x = x * scalefactor
    # print("after ", x)

    return x


def removeNeg(sf, x):
    posx = []
    for b in x:
        if b >= 0:
            posx.append(b)

    sf = sf[len(posx) :, :]
    return sf, posx


def readZisoStat(infile, ID, removeNeg="no"):
    print("infile: ", infile)
    print("ID: ", ID)
    # define the histograms holding the central value, and the systematic variations
    basename = "%s_%s_%s_%s_%s_%s_%s_%s_DataDriven_Rel21_Smooth_vTest"

    # for the Ziso the templates and their syst variations is a complicated if-else thing
    # I'll loop a couple of templates and later stitch them together
    templates = [
        "TemplateVar1",  # pT<20 GeV
        "Template_ge1_sel_noloose",  # 20<pT<25 GeV, endcap
        "Template_ge1_sel_noloose",  # 20<pT<25 GeV, barrel
        "Template_ge1_sel_noloose",  # pT>25 GeV
    ]
    templates_pTeta = [  # later we'll loop and fill values if pT>=1st entry and eta<=2nd entry. The order of these is therefore important
        [0.0, 99.0],  # pT<20 GeV
        [20.0, 99.0],  # 20<pT<25 GeV, endcap
        [20.0, 1.52],  # 20<pT<25 GeV, barrel
        [25.0, 99.0],  # pT>25 GeV
    ]

    # np arrays to hold the combined values
    MC_ref = np.array([])
    Da_ref = np.array([])
    SF_ref = np.array([])
    stat_mc = np.array([])
    stat_da = np.array([])
    stat_sf = np.array([])
    bla_mc = np.array([])
    bla_da = np.array([])
    bla_sf = np.array([])
    x_mc = np.array([])
    x_da = np.array([])
    x = np.array([])
    y_mc = np.array([])
    y_da = np.array([])
    y = np.array([])

    def replaceValues(arr_orig, arr_new, xbins, ybins, cond_pT, cond_eta):
        arr_out = np.copy(arr_orig)
        if arr_orig.shape != arr_new.shape:
            print(" in replaceValues: passing incompatible arrays")
            return arr_out

        for ientry in range(0, arr_orig.shape[0]):
            xbin = int(ientry / len(ybins))
            ybin = ientry % len(ybins)

            if (
                xbins[xbin] >= cond_pT
                and abs(ybins[ybin]) + np.sign(ybins[ybin]) * sys.float_info.epsilon
                <= cond_eta
            ):  # +sign*epsilon is there since the array only contains the lower bin boundary
                arr_out[ientry] = arr_new[ientry]

        return arr_out

    for var in range(0, len(templates)):
        reference = [
            templates[var],
            "Tag1",
            "mll75105",
            "ProbeIsoCut0p5",
            "topoetcone30",
            "doSubractMCeventsInTailScaleFactor1.0",
            "RealElectronContaminationScalingFactor1p0",
            ID,
        ]

        # now we fill the histograms into arrays
        MC_var, stat_mc_var, syst_mc_var, x_mc, y_mc = getArray_SF_stat_syst(
            infile,
            [
                "EffMC/EffMC_" + basename % tuple(reference),
                "EffMC/EffMC_StatError_" + basename % tuple(reference),
            ],
        )
        Da_var, stat_da_var, syst_da_var, x_da, y_da = getArray_SF_stat_syst(
            infile, "EffData/EffData_" + basename % tuple(reference)
        )
        SF_var, stat_var, syst_var, x, y = getArray_SF_stat_syst(
            infile, "SF/SF_CentralValue_" + basename % tuple(reference)
        )

        # stitching together the various template choices
        if var == 0:
            MC_ref = Da_ref = SF_ref = np.zeros(SF_var.shape)
            stat_mc = stat_da = stat_sf = np.zeros(stat_var.shape)
            bla_mc = bla_da = bla_sf = np.zeros(syst_var.shape)

        MC_ref = replaceValues(
            MC_ref, MC_var, x_mc, y_mc, templates_pTeta[var][0], templates_pTeta[var][1]
        )
        stat_mc = replaceValues(
            stat_mc,
            stat_mc_var,
            x_mc,
            y_mc,
            templates_pTeta[var][0],
            templates_pTeta[var][1],
        )

        Da_ref = replaceValues(
            Da_ref, Da_var, x_da, y_da, templates_pTeta[var][0], templates_pTeta[var][1]
        )
        stat_da = replaceValues(
            stat_da,
            stat_da_var,
            x_da,
            y_da,
            templates_pTeta[var][0],
            templates_pTeta[var][1],
        )

        SF_ref = replaceValues(
            SF_ref, SF_var, x, y, templates_pTeta[var][0], templates_pTeta[var][1]
        )
        stat_sf = replaceValues(
            stat_sf, stat_var, x, y, templates_pTeta[var][0], templates_pTeta[var][1]
        )

    ###################################################
    # now the new stuff
    SF_new = np.array([])
    stat_new_corr = np.array([])
    stat_new_uncorr = np.array([])

    for var in range(0, len(templates)):
        reference = [
            templates[var],
            "Tag1",
            "mll75105",
            "ProbeIsoCut0p5",
            "topoetcone30",
            "doSubractMCeventsInTailScaleFactor1.0",
            "RealElectronContaminationScalingFactor1p0",
            ID,
        ]
        reflist = []
        reflist.append("EffData/EffData_" + basename % tuple(reference))
        reflist.append("EffData/EffData_StatErrorCorr_" + basename % tuple(reference))
        Da_corr, stat_corr, syst_corr, x_corr, y_corr = getArray_SF_stat_syst(
            infile, reflist
        )
        reflist = []
        reflist.append("EffData/EffData_" + basename % tuple(reference))
        reflist.append("EffData/EffData_StatErrorUncorr_" + basename % tuple(reference))
        Da_uncorr, stat_uncorr, syst_uncorr, x_uncorr, y_uncorr = getArray_SF_stat_syst(
            infile, reflist
        )

        SF_corr = np.zeros(Da_corr.shape)
        stat_sf_corr = np.zeros(Da_corr.shape)
        SF_uncorr = np.zeros(Da_uncorr.shape)
        stat_sf_uncorr = np.zeros(Da_uncorr.shape)
        for i in range(0, SF_corr.shape[0]):
            if MC_ref[i] != 0:
                SF_corr[i] = Da_corr[i] / MC_ref[i]
                SF_uncorr[i] = Da_uncorr[i] / MC_ref[i]
                stat_sf_corr[i] = SF_corr[i] * np.sqrt(
                    (stat_corr[i] / Da_corr[i]) ** 2 + (stat_mc[i] / MC_ref[i]) ** 2
                )
                # print(np.sqrt( (stat_corr  [i]/Da_corr  [i])**2 + (stat_mc[i]/MC_ref  [i])**2 ),(stat_corr  [i]/Da_corr  [i]),(stat_mc[i]/MC_ref  [i]))
                # print("\t",stat_corr  [i],Da_corr[i],stat_mc[i],MC_ref  [i])
                stat_sf_uncorr[i] = SF_uncorr[i] * np.sqrt(
                    (stat_uncorr[i] / Da_uncorr[i]) ** 2
                )
                if stat_sf_corr[i] == 0 or stat_sf_uncorr[i] == 0:
                    # print(
                    #    " PHILIP found 0 uncertainties ",
                    #    stat_sf_corr[i],
                    #    " ",
                    #    stat_sf_uncorr[i],
                    # )
                    # print(
                    #    "  --> input is ",
                    #    SF_uncorr[i],
                    #    " ",
                    #    stat_uncorr[i],
                    #    " ",
                    #    Da_uncorr[i],
                    # )
                    pass
        # stitching together the various template choices
        if var == 0:
            SF_new = np.zeros(SF_corr.shape)
            stat_new_corr = np.zeros(stat_sf_corr.shape)
            stat_new_uncorr = np.zeros(stat_sf_uncorr.shape)

        SF_new = replaceValues(
            SF_new, SF_corr, x, y, templates_pTeta[var][0], templates_pTeta[var][1]
        )
        stat_new_corr = replaceValues(
            stat_new_corr,
            stat_sf_corr,
            x,
            y,
            templates_pTeta[var][0],
            templates_pTeta[var][1],
        )
        stat_new_uncorr = replaceValues(
            stat_new_uncorr,
            stat_sf_uncorr,
            x,
            y,
            templates_pTeta[var][0],
            templates_pTeta[var][1],
        )

    x_tmp = np.array(x)
    y_tmp = np.array(y)

    # let's strip off the bins below 15 GeV
    MC_ref, stat_mc, bla_mc, x_mc, y_mc = stripFromArrayX(
        15.0, MC_ref, stat_mc, bla_mc, x_mc, y_mc
    )
    Da_ref, stat_da, bla_da, x_da, y_da = stripFromArrayX(
        15.0, Da_ref, stat_da, bla_da, x_da, y_da
    )
    SF_ref, stat_sf, bla_sf, x, y = stripFromArrayX(15.0, SF_ref, stat_sf, bla_sf, x, y)

    # eta abs
    MC_ref = makeEtaAbs(MC_ref, x, y, 150.0)
    stat_mc = makeEtaAbs(stat_mc, x, y, 150.0)

    Da_ref = makeEtaAbs(Da_ref, x, y, 150.0)
    stat_da = makeEtaAbs(stat_da, x, y, 150.0)

    SF_ref = makeEtaAbs(SF_ref, x, y, 150.0)
    stat_sf = makeEtaAbs(stat_sf, x, y, 150.0)

    # let's strip off the bins below 15 GeV
    SF_new, stat_new_uncorr, stat_new_corr, x_tmp, y_tmp = stripFromArrayX(
        15.0, SF_new, stat_new_uncorr, stat_new_corr, x_tmp, y_tmp
    )

    # eta abs
    SF_new = makeEtaAbs(SF_new, x, y, 150.0)
    stat_new_corr = makeEtaAbs(stat_new_corr, x, y, 150.0)
    stat_new_uncorr = makeEtaAbs(stat_new_uncorr, x, y, 150.0)

    # and we're done! This stuff we can now store
    # writeArray_SF_stat_syst( outdir, SF_new, stat_new_uncorr, stat_new_corr, x, y )
    # writeArray_SF_stat_syst( outdir+'_ref', SF_ref, stat_sf, np.zeros(stat_sf.shape), x, y )

    arr_out = np.column_stack((SF_new, stat_new_uncorr, stat_new_corr))
    # print(arr_out)
    print("removeNeg: ", removeNeg)
    if removeNeg != "no":
        posy = []
        for b in y:
            if b >= 0:
                posy.append(b)
        y = posy
        print(arr_out)
        arr_out = arr_out[len(posy) :, :]
        print(arr_out)
        print(y, posy)
    return arr_out
