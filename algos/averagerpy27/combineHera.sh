#! /bin/bash -l
export ALRB_testPath=",,,,,,,,"
export ALRB_CONT_SWTYPE="apptainer"
#export ALRB_CONT_RUNPAYLOAD="source /afs/cern.ch/user/c/cbilling/tNp/tnp_taipy/algos/rebin.sh"
#payload="lsetup 'root 6.14.08-x86_64-centos7-gcc8-opt';cd /afs/cern.ch/user/c/cbilling/tNp/tnp_taipy/algos/;export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_94a/ROOT/6.14.08/x86_64-centos7-gcc8-opt/lib:/usr/lib64:/usr/lib:/afs/cern.ch/user/c/cbilling/.local/lib/python2.7;export PYTHONPATH=/usr/lib64:/cvmfs/sft.cern.ch/lcg/releases/LCG_94a/ROOT/6.14.08/x86_64-centos7-gcc8-opt/lib:/afs/cern.ch/user/c/cbilling/.local/lib/python2.7:/usr/lib64/python2.7/site-packages;python averagerpy27/combineZMethods_heraverager.py "
payload="lsetup 'root 6.14.08-x86_64-centos7-gcc8-opt';cd "$WORKING_DIR"/algos/;python averagerpy27/combineZMethods_heraverager.py "
payload+=$1
payload+=" "
payload+=$2
payload+=" "
payload+=$3
export ALRB_CONT_RUNPAYLOAD=$payload
export ALRB_CONT_CMDOPTS=" -B "$HOME":"$HOME
export ALRB_CONT_PRESETUP="hostname -f; date; id -a"

alias | \grep -e "setupATLAS" > /dev/null 2>&1
if [ $? -ne 0 ]; then
	echo trying to set setupATLAS
    typeset  -f setupATLAS > /dev/null 2>&1
    if [ $? -ne 0 ]; then
	function setupATLAS
	{
            if [ -d  /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase ]; then
		export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
		source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh $@
		return $?
            else
		\echo "Error: cvmfs atlas repo is unavailable"
		return 64
            fi
	}
    fi
fi

# setupATLAS -c <container> which will run and also return the exit code
#  (setupATLAS is source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh)
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh -c centos7
exit $?