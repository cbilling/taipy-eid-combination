
import sys, operator, os
sys.path.append('/root/taipy-eid-combination/python2.7')
from tools.arrayToText import *
from tools.statTools import *
from tools.rootToArray import *
from tools.plotting import *
import averager

print ' ----------------------------------------------- '
print ' in combineZMethods_heraverager.py'
print ' ----------------------------------------------- '

indir_meth1 = sys.argv[1] # SF, stat, syst from one method
indir_meth2 = sys.argv[2] # SF, stat, syst from another method

outdir = sys.argv[3]

# read the values from the previous steps for two methods
SF_meth1, stat_meth1, syst_meth1, x_meth1, y_meth1 = readArray_SF_stat_syst_leg( indir_meth1 )
SF_meth2, stat_meth2, syst_meth2, x_meth2, y_meth2 = readArray_SF_stat_syst_leg( indir_meth2 )

consistent = True
consistent = consistent and consistency(x_meth1,x_meth2)
consistent = consistent and consistency(y_meth1,y_meth2)

if not consistent :
    print ' trying to combine two methods with inconsistent binning '
    exit()

# the full blown chi2
SF_all = np.array( (SF_meth1,SF_meth2) ) # it's a 2-d array
# FROM NOW ON WE DECORRELATE THE INPUT WITH A DEDICATED SCRIPT
#syst_meth1, syst_meth2 = decorrelate( syst_meth1, syst_meth2, list(range(220+2)) ) # in practise we want to keep columns 0 and 1 correlated, so there's a exception for columns [0,1]

#make some names that also steer the behaviour in the fit!
snames = []
for i in range(syst_meth1.shape[1]) :
    snames.append( 'e%03d:M' % i ) 

stat_all = np.array( (stat_meth1,stat_meth2) )
syst_all = np.array( (syst_meth1[:,:],syst_meth2[:,:]) )


# configure and run the heraverager and save the result
averager.avin.initvariables()
averager.avin.indebug = 0
averager.avin.setsnames(snames)

averager.avin.initeration = 10
averager.avin.inrescalestatsep = False
averager.avin.incorrectstatbias = True
averager.avin.infixstat = True
averager.avin.inpostrotatesyst = True

average, averstat, aversyst = averager.average(SF_all.T, stat_all.T, syst_all.T)
aversyst = aversyst.T # heraverager has a different format than I do

ensure_directory( outdir )
np.savetxt( outdir+'/pulldata.txt'   ,  averager.avout.pulldata   , fmt='%1.6f' )
np.savetxt( outdir+'/pullsyst.txt'   ,  averager.avout.pullsyst   , fmt='%1.6f' )
np.savetxt( outdir+'/shiftsyst.txt'  ,  averager.avout.shiftsyst  , fmt='%1.6f' )
np.savetxt( outdir+'/squeezesyst.txt',  averager.avout.squeezesyst, fmt="%1.6f" )
np.savetxt( outdir+'/chi2.txt'       ,  np.array([averager.avout.chi2,averager.avout.ndof]), fmt="%1.6f" )

averstat = np.sqrt( averstat**2-np.sum( aversyst**2, axis=1 ) )
writeArray_SF_stat_syst( outdir, average, averstat, aversyst, x_meth1, y_meth1 )

#chi2 = get_chi2( average, 0, SF_all, stat_all, syst_all )
#ndof = len(SF_meth1)
#np.savetxt( outdir+'/chi2_philip.txt'       ,  np.array([chi2,ndof]), fmt="%1.6f" )
#print '    chi2 (chi2 incl all correlations): ', chi2/ndof

print 'av ',average
print 'stat',averstat
print 'syst',np.sqrt( np.sum( aversyst**2, axis=1 ) )
