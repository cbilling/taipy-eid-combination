import sys, operator
from .tools.arrayToText import *
from .tools.statTools import *
import ROOT
from .tools.plotting import *

# python2 comparisonPlots_corruncorr.py 'plotting_jbin/Zcomb_loPt_rebin' $outfile'/Zcomb_loPt_rebin' $outfile'/Ziso_decorr_sys_jbin' $outfile'/Zmass_decorr_sys_jbin' 'Z combined','Z iso','Z mass'


def comparisonPlots_corruncorr(
    outmethod,
    indir_meth1,
    indir_meth2,
    indir_meth3,
    x,
    y,
    labels,
    plotlocation,
):
    print(outmethod)

    outdir = f"/afs/cern.ch/user/c/cbilling/tNp/tnp_taipy/.data/plotting/{plotlocation}/{outmethod}"
    labels = labels.split(",")
    # read the values from the previous steps for two methods
    SF = []
    stat = []
    syst = []
    xbin = []
    ybin = []
    print(indir_meth1.shape)
    print(indir_meth2.shape)
    print(indir_meth3.shape)
    print(x)
    print(y)
    print(labels)
    print(plotlocation)
    for indir in [indir_meth1, indir_meth2, indir_meth3]:
        SF_meth, stat_meth, syst_meth, x_meth, y_meth = readArray_SF_stat_syst(
            indir, x, y
        )
        SF.append(SF_meth)
        stat.append(stat_meth)
        syst.append(syst_meth)
        xbin.append(x_meth)
        ybin.append(y_meth)

    shapeSF = SF[0].shape  # we already know all are consistent
    corr = [np.zeros(shapeSF)] * len(SF)
    uncorr = [np.zeros(shapeSF)] * len(SF)

    for meth in range(len(SF)):
        corr[meth] = np.sqrt(np.sum(syst[meth][:, :222] ** 2, axis=1))
        uncorr[meth] = np.sqrt(
            stat[meth] ** 2 + np.sum(syst[meth][:, 222:] ** 2, axis=1)
        )

    # --------------------------------------------------------------
    # we have everything together to convert this back to a TH2
    # --------------------------------------------------------------
    graphs = []
    shift = [-0.04, 0.0, 0.04, 0.04]
    colour = [1, ROOT.kRed + 1, ROOT.kBlue + 1, ROOT.kGreen + 1]
    counter = 0

    cleanLabels = []
    for SF_meth, stat_meth, syst_meth, x, y in zip(SF, uncorr, corr, xbin, ybin):
        hist = makeTH2D(
            SF_meth, get_standarddev(np.column_stack((stat_meth, syst_meth))), x, y
        )
        graphs.append(
            getProjectionY(
                hist,
                colour[min(counter, len(colour))],
                shift[min(counter, len(colour))],
            )
        )
        hist = makeTH2D(
            SF_meth,
            get_standarddev(np.column_stack((stat_meth, np.zeros(syst_meth.shape)))),
            x,
            y,
        )
        graphs.append(
            getProjectionY(
                hist,
                colour[min(counter, len(colour))],
                shift[min(counter, len(colour))],
            )
        )
        if counter < len(labels):
            cleanLabels.append(labels[counter])
            cleanLabels.append("")  # we don't plot entries without label
        counter = counter + 1
    print(f"using these labels: {cleanLabels}")
    writePlots(graphs, outdir, "comparison_", True, cleanLabels)
    return outdir


def comparisonPlots(outmethod, indir_meth1, indir_meth2, indir_meth3, x, y, labels):
    outdir = sys.argv[1]

    indirs = sys.argv[2:-1]
    labels = sys.argv[-1].split(",")
    if len(labels) == 1:  # then it's probably another dir and we didn't provide a label
        indirs.append(labels)
        labels = 0

    # read the values from the previous steps for two methods
    SF = []
    stat = []
    syst = []
    xbin = []
    ybin = []
    counter = 0
    for indir in indirs:
        SF_meth, stat_meth, syst_meth, x_meth, y_meth = readArray_SF_stat_syst(indir)
        #    if counter==1 :
        #        SF_meth=SF_meth[:-1*len(y_meth)]
        #        stat_meth=stat_meth[:-1*len(y_meth)]
        #        syst_meth=syst_meth[:-1*len(y_meth),:]
        #        x_meth=x_meth[:-1]
        SF.append(SF_meth)
        stat.append(stat_meth)
        syst.append(syst_meth)
        xbin.append(x_meth)
        ybin.append(y_meth)
        counter = counter + 1

    # array holding both the stat and syst
    statsyst = []
    for sta, sys in zip(stat, syst):
        statsyst.append(np.column_stack((sta, sys)))

    # std dev
    stddev = []
    for stasys in statsyst:
        stddev.append(get_standarddev(stasys))

    # --------------------------------------------------------------
    # we have everything together to convert this back to a TH2
    # --------------------------------------------------------------
    graphs = []
    shift = [0.0]  # -0.02,0.,0.02,0.04]
    colour = [1, ROOT.kRed + 1, ROOT.kBlue + 1, ROOT.kGreen + 1]
    counter = 0
    for SF_meth, stddev_meth, x, y in zip(SF, stddev, xbin, ybin):
        hist = makeTH2D(SF_meth, stddev_meth, x, y)
        # and here we do the actual plotting
        graphs.append(
            getProjectionY(
                hist,
                colour[min(counter, len(colour) - 1)],
                shift[min(counter, len(shift) - 1)],
            )
        )

        counter = counter + 1

    ensure_directory(outdir)
    writePlots(graphs, outdir, "comparison_", False, labels)
