import sys, operator
from .tools.arrayToText import *
from .tools.statTools import *
from .tools.rootToArray import *
from .tools.plotting import *


def decorrelateStat(indir_meth1, indir_meth2, x, y):
    SF_meth1, stat_meth1, syst_meth1, x_meth1, y_meth1 = readArray_SF_stat_syst(
        indir_meth1, x, y
    )
    SF_meth2, stat_meth2, syst_meth2, x_meth2, y_meth2 = readArray_SF_stat_syst(
        indir_meth2, x, y
    )
    print(SF_meth1.shape, SF_meth2.shape)
    # consistent = True
    # consistent = consistent and consistency(x_meth1,x_meth2)
    # consistent = consistent and consistency(y_meth1,y_meth2)

    SF_out = np.copy(SF_meth1)
    uncorr_out = np.zeros(SF_meth1.shape)
    corr_out = np.zeros(SF_meth1.shape)  # the syst here is the correlated stat

    for bin in range(0, len(SF_out)):
        stat_ref_rel = stat_meth1[bin] / SF_meth1[bin]
        uncorr_new_rel = stat_meth2[bin] / SF_meth2[bin]
        corr_new_rel = syst_meth2[bin] / SF_meth2[bin]

        rescale = stat_ref_rel / np.sqrt(uncorr_new_rel**2 + corr_new_rel**2)
        if uncorr_new_rel == 0.0 or np.isnan(uncorr_new_rel):
            rescale = 1.0
            stat_ref_rel = 0.0
            uncorr_new_rel = 0.0
            corr_new_rel = 0.0
            SF_out[bin] = 0.0
            syst_meth1[bin, :] = np.zeros(syst_meth1.shape[1])

        uncorr_out[bin] = uncorr_new_rel * rescale * SF_meth1[bin]
        corr_out[bin] = corr_new_rel * rescale * SF_meth1[bin]

    corr_out = np.diag(corr_out)
    # corr_out = np.zeros(SF_out.shape)

    # writing where the correlated stats go at the beginning of systs
    # writeArray( outdir, np.column_stack( (SF_out, uncorr_out, corr_out, syst_meth1) ), x_meth1, y_meth2 )

    arr_out = np.column_stack((SF_out, uncorr_out, corr_out, syst_meth1))
    return arr_out


def decorrelateSyst(indir_meth1, indir_meth2, x, y):
    nCorr = 222  # How many lines are in the Ziso file + 2
    print(" correlating the first ", nCorr, " sources of syst uncertainty ")
    print(indir_meth1.shape, indir_meth2.shape)
    # To make shape match jpsi to be able to plot
    zero_row = np.zeros(
        (abs(indir_meth1.shape[0] - indir_meth2.shape[0]), indir_meth1.shape[1])
    )

    # Prepending the row of zeros to the existing array
    indir_meth1 = np.vstack((zero_row, indir_meth1))

    SF_meth1, stat_meth1, syst_meth1, x_meth1, y_meth1 = readArray_SF_stat_syst(
        indir_meth1, x, y
    )
    SF_meth2, stat_meth2, syst_meth2, x_meth2, y_meth2 = readArray_SF_stat_syst(
        indir_meth2, x, y
    )

    consistent = True
    consistent = consistent and consistency(x_meth1, x_meth2)
    consistent = consistent and consistency(y_meth1, y_meth2)

    if not consistent:
        print(" trying to combine two methods with inconsistent binning ")
        exit()

    # print("Correlated",nCorr)

    print("Before:", syst_meth1.shape, syst_meth2.shape)
    syst_meth1, syst_meth2 = decorrelate(syst_meth1, syst_meth2, list(range(nCorr)))
    print("After:", syst_meth1.shape, syst_meth2.shape)
    # writing where the correlated stats go at the beginning of systs
    # writeArray_SF_stat_syst( outdir_meth1, SF_meth1, stat_meth1, syst_meth1, x_meth1, y_meth1 )
    # writeArray_SF_stat_syst( outdir_meth2, SF_meth2, stat_meth2, syst_meth2, x_meth2, y_meth2 )

    arr_out1 = np.column_stack((SF_meth1, stat_meth1, syst_meth1))
    arr_out2 = np.column_stack((SF_meth2, stat_meth2, syst_meth2))
    return arr_out1, arr_out2


def undecorrelateStat(indir_meth1, x, y):
    SF_meth1, stat_meth1, syst_meth1, x_meth1, y_meth1 = readArray_SF_stat_syst(
        indir_meth1, x, y
    )

    nStat = SF_meth1.shape[0]
    if len(sys.argv) > 3:
        nStat = int(sys.argv[3])

    SF_out = np.copy(SF_meth1)
    stat_out = np.sqrt(
        np.copy(stat_meth1) ** 2 + np.sum(syst_meth1[:, :nStat] ** 2, axis=1)
    )
    syst_out = np.copy(syst_meth1[:, nStat:])

    arr_out1 = np.column_stack((SF_out, stat_out, syst_out))
    return arr_out1
