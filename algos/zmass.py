import sys, operator
from .tools.rootToArray import *
from .tools.arrayToText import *


def readZmass(infile, ID, year, removeNeg="no"):
    # define the histograms holding the central value, and the systematic variations
    # basename = 'SF_%s_%s_%s_%s_%s_fit_%s_MC_%s_cont_%s'

    list = [
        "sf_CentralValues_nominal_" + ID,
        "StatError_nominal_" + ID,
        "sf_CentralValues_tag_iso_up_" + ID,
        "sf_CentralValues_tag_iso_do_" + ID,
        "sf_CentralValues_int_70_110_" + ID,
        "sf_CentralValues_int_80_100_" + ID,
        "sf_CentralValues_bkg_fit_range_hi_" + ID,
        "sf_CentralValues_bkg_fit_range_lo_" + ID,
        "sf_CentralValues_bkg_fit_up_" + ID,
        "sf_CentralValues_bkg_fit_do_" + ID,
        "sf_CentralValues_bkg_model_alt25_" + ID,
        "sf_CentralValues_mc_weight_off_" + ID,
        "sf_CentralValues_sig_cont_up_" + ID,
        "sf_CentralValues_sig_cont_do_" + ID,
        "sf_CentralValues_var1_" + ID,
        "sf_CentralValues_var3_" + ID,
    ]
    # now we fill the histograms into arrays
    SF, stat, syst, x, y = getArray_SF_stat_syst(infile, list)
    # let's strip off the bins below 15 GeV
    SF, stat, syst, x, y = stripFromArrayX(15.0, SF, stat, syst, x, y)

    # Elias has zeros at high Et where we do the measurement in abs(eta)
    SF = makeEtaAbs(SF, x, y, 150.0)
    stat = makeEtaAbs(stat, x, y, 150.0)
    syst = makeEtaAbs(syst, x, y, 150.0)

    # in Elias file, the last bin seems problematic and a measurement doesn't always exist
    # as a temporary fix, I'm just using the measurement from bin in opposite eta
    def check_SF_stat_syst(SF, stat, syst):
        for i in range(0, len(SF)):
            if SF[i] != 0 and SF[i] == SF[i]:
                continue

            print(
                "    found problematic bin for Zmass in pT ",
                x[int(i / len(y))],
                " eta ",
                y[i % len(y)],
                " val ",
                SF[i],
                " ",
                i,
            )
            ybin = i % len(y)
            ybin_new = len(y) - ybin - 1
            i_new = i - ybin + ybin_new
            print(
                "    instead using bin pT ",
                x[int(i_new / len(y))],
                " eta ",
                y[i_new % len(y)],
                " val ",
                SF[i_new],
                " ",
                i_new,
            )
            SF[i] = SF[i_new]
            stat[i] = stat[i_new]
            syst[i] = syst[i_new]

            if SF[i] != 0 and SF[i] == SF[i]:
                continue

            print(
                "    still problematic bin for Zmass in pT ",
                x[int(i / len(y))],
                " eta ",
                y[i % len(y)],
                " val ",
                SF[i],
                " ",
                i,
            )
            i_new = i - len(y)
            print(
                "    instead using bin pT ",
                x[int(i_new / len(y))],
                " eta ",
                y[i_new % len(y)],
                " val ",
                SF[i_new],
                " ",
                i_new,
            )
            SF[i] = SF[i_new]
            stat[i] = stat[i_new]
            syst[i] = syst[i_new]

        return SF, stat, syst

    SF, stat, syst = check_SF_stat_syst(SF, stat, syst)

    # and we're done! This stuff we can now store
    # writeArray_SF_stat_syst( outdir, SF, stat, syst, x, y )
    arr_out = np.column_stack((SF, stat, syst))
    print("removeNeg: ", removeNeg)
    if removeNeg != "no":
        posy = []
        for b in y:
            if b >= 0:
                posy.append(b)
        y = posy
        print(arr_out)
        arr_out = arr_out[len(posy) :, :]
        print(arr_out)
        print(y, posy)
    return arr_out, x, y


def readZmassStat(filename, ID, removeNeg="no"):
    infile = TFile.Open(filename)
    # define the histograms holding the central value, and the systematic variations
    # basename = '%s_%s_%s_%s_%s_%s_%s_%s'

    list_var = [
        "nominal_" + ID,  #'StatError_nominal', # nominal and stat uncertainty
        # ['tag_iso_up', 'tag_iso_do'],  # iso variations
        #'eff_CentralValues_var1', # bkg variation 1
        #'eff_CentralValues_var3', # bkg variation 3
        # ['sf_CentralValues_bkg_fit_range_lo', 'sf_CentralValues_bkg_fit_range_hi'], # fit range vairations
        #'eff_CentralValues_mc_weight_off', # turning MC weight off
        # ['sf_CentralValues_sig_cont_up', 'sf_CentralValues_sig_cont_do'], # fit ucertainty of signa contamination
        #'f_CentralValues_bkg_model_alt25',  # alternative fit model for 20-25 GeV PT bin
        # ['sf_CentralValues_bkg_fit_do', 'sf_CentralValues_bkg_fit_up'], # fit ucertainty on the bkg normalization
        # ['sf_CentralValues_int_80_100', 'sf_CentralValues_int_70_110'] # mll window variation
    ]

    # now we fill the histograms into arrays
    MC_ref, stat_mc, bla_mc, x_mc, y_mc = getArray_SF_stat_syst(
        filename,
        ["EffMC_eff_CentralValues_nominal_" + ID, "EffMC_StatError_nominal_" + ID],
    )
    Da_ref, stat_da, bla_da, x_da, y_da = getArray_SF_stat_syst(
        filename,
        ["EffData_eff_CentralValues_nominal_" + ID, "EffData_StatError_nominal_" + ID],
    )
    SF_ref, stat_sf, bla_sf, x, y = getArray_SF_stat_syst(
        filename, ["sf_CentralValues_nominal_" + ID, "StatError_nominal_" + ID]
    )

    def replaceValues(arr_orig, arr_new, xbins, ybins, cond_pT, cond_eta):
        arr_out = np.copy(arr_orig)
        if arr_orig.shape != arr_new.shape:
            print(" in replaceValues: passing incompatible arrays")
            return arr_out

        for ientry in range(0, arr_orig.shape[0]):
            xbin = int(ientry / len(ybins))
            ybin = ientry % len(ybins)

            # if xbins[xbin]>=cond_pT and abs(ybins[ybin])+np.sign(ybins[ybin])*sys.float_info.epsilon<=cond_eta : # +sign*epsilon is there since the array only contains the lower bin boundary
            #    arr_out[ientry] = arr_new[ientry]

        return arr_out

    """
    for var in range(0,len(templates)) :
        reference = [ templates[var],'Tag1','mll75105','ProbeIsoCut0p5','topoetcone30','doSubractMCeventsInTailScaleFactor1.0','RealElectronContaminationScalingFactor1p0',ID ]

        # now we fill the histograms into arrays
        MC_var, stat_mc_var, syst_mc_var, x_mc, y_mc = getArray_SF_stat_syst(filename_orig,'EffMC/EffMC_'       +basename % tuple(reference))
        Da_var, stat_da_var, syst_da_var, x_da, y_da = getArray_SF_stat_syst(filename_orig,'EffData/EffData_'   +basename % tuple(reference))
        SF_var, stat_var   , syst_var   , x   , y    = getArray_SF_stat_syst(filename_orig,'SF/SF_CentralValue_'+basename % tuple(reference))

        # stitching together the various template choices 
        if var == 0 :
            MC_ref  = Da_ref  = SF_ref  = np.zeros(SF_var  .shape)
            stat_mc = stat_da = stat_sf = np.zeros(stat_var.shape)
            bla_mc  = bla_da  = bla_sf  = np.zeros(syst_var.shape)

        MC_ref   = replaceValues(MC_ref,MC_var,x_mc,y_mc,templates_pTeta[var][0],templates_pTeta[var][1])
        stat_mc  = replaceValues(stat_mc,stat_mc_var,x_mc,y_mc,templates_pTeta[var][0],templates_pTeta[var][1])

        Da_ref   = replaceValues(Da_ref,Da_var,x_da,y_da,templates_pTeta[var][0],templates_pTeta[var][1])
        stat_da  = replaceValues(stat_da,stat_da_var,x_da,y_da,templates_pTeta[var][0],templates_pTeta[var][1])

        SF_ref   = replaceValues(SF_ref,SF_var,x,y,templates_pTeta[var][0],templates_pTeta[var][1])
        stat_sf  = replaceValues(stat_sf,stat_var,x,y,templates_pTeta[var][0],templates_pTeta[var][1])
    """

    ###################################################
    # now the new stuff
    SF_new = np.array([])
    stat_new_corr = np.array([])
    stat_new_uncorr = np.array([])

    for var in range(0, len(list_var)):
        reference = list_var[var]
        reflist = []
        reflist.append("EffData_eff_CentralValues_" + reference)
        reflist.append("EffData_StatErrorCorr_" + reference)
        Da_corr, stat_corr, syst_corr, x_corr, y_corr = getArray_SF_stat_syst(
            filename, reflist
        )
        reflist = []
        reflist.append("EffData_eff_CentralValues_" + reference)
        reflist.append("EffData_StatErrorUncorr_" + reference)
        Da_uncorr, stat_uncorr, syst_uncorr, x_uncorr, y_uncorr = getArray_SF_stat_syst(
            filename, reflist
        )

        SF_corr = np.zeros(Da_corr.shape)
        stat_sf_corr = np.zeros(Da_corr.shape)
        SF_uncorr = np.zeros(Da_uncorr.shape)
        stat_sf_uncorr = np.zeros(Da_uncorr.shape)
        for i in range(0, SF_corr.shape[0]):
            if MC_ref[i] != 0:
                SF_corr[i] = Da_corr[i] / MC_ref[i]
                SF_uncorr[i] = Da_uncorr[i] / MC_ref[i]
                stat_sf_corr[i] = SF_corr[i] * np.sqrt(
                    (stat_corr[i] / Da_corr[i]) ** 2 + (stat_mc[i] / MC_ref[i]) ** 2
                )
                stat_sf_uncorr[i] = SF_uncorr[i] * np.sqrt(
                    (stat_uncorr[i] / Da_uncorr[i]) ** 2
                )
                if stat_sf_corr[i] == 0 or stat_sf_uncorr[i] == 0:
                    print(
                        " PHILIP found 0 uncertainties ",
                        stat_sf_corr[i],
                        " ",
                        stat_sf_uncorr[i],
                    )
                    print(
                        "  --> input is ",
                        SF_uncorr[i],
                        " ",
                        stat_uncorr[i],
                        " ",
                        Da_uncorr[i],
                    )
        # stitching together the various template choices
        if var == 0:
            SF_new = np.zeros(SF_corr.shape)
            stat_new_corr = np.zeros(stat_sf_corr.shape)
            stat_new_uncorr = np.zeros(stat_sf_uncorr.shape)

        SF_new = SF_corr
        stat_new_corr = stat_sf_corr
        stat_new_uncorr = stat_sf_uncorr

    x_tmp = np.array(x)
    y_tmp = np.array(y)

    # let's strip off the bins below 15 GeV
    MC_ref, stat_mc, bla_mc, x_mc, y_mc = stripFromArrayX(
        15.0, MC_ref, stat_mc, bla_mc, x_mc, y_mc
    )
    Da_ref, stat_da, bla_da, x_da, y_da = stripFromArrayX(
        15.0, Da_ref, stat_da, bla_da, x_da, y_da
    )
    SF_ref, stat_sf, bla_sf, x, y = stripFromArrayX(15.0, SF_ref, stat_sf, bla_sf, x, y)

    # eta abs
    MC_ref = makeEtaAbs(MC_ref, x, y, 150.0)
    stat_mc = makeEtaAbs(stat_mc, x, y, 150.0)

    Da_ref = makeEtaAbs(Da_ref, x, y, 150.0)
    stat_da = makeEtaAbs(stat_da, x, y, 150.0)

    SF_ref = makeEtaAbs(SF_ref, x, y, 150.0)
    stat_sf = makeEtaAbs(stat_sf, x, y, 150.0)

    # let's strip off the bins below 15 GeV
    SF_new, stat_new_uncorr, stat_new_corr, x_tmp, y_tmp = stripFromArrayX(
        15.0, SF_new, stat_new_uncorr, stat_new_corr, x_tmp, y_tmp
    )

    # eta abs
    SF_new = makeEtaAbs(SF_new, x, y, 150.0)
    stat_new_corr = makeEtaAbs(stat_new_corr, x, y, 150.0)
    stat_new_uncorr = makeEtaAbs(stat_new_uncorr, x, y, 150.0)

    # and we're done! This stuff we can now store
    # writeArray_SF_stat_syst( outdir, SF_new, stat_new_uncorr, stat_new_corr, x, y )
    # writeArray_SF_stat_syst( outdir+'_ref', SF_ref, stat_sf, np.zeros(stat_sf.shape), x, y )

    arr_out = np.column_stack((SF_new, stat_new_uncorr, stat_new_corr))
    if removeNeg != "no":
        posy = []
        for b in y:
            if b >= 0:
                posy.append(b)
        y = posy
        print(arr_out)
        arr_out = arr_out[len(posy) :, :]
        print(arr_out)
        print(y, posy)
    return arr_out
