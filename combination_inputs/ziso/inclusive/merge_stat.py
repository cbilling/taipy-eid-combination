import ROOT
import os,glob


ProbeIDList= {
    'TightLLH_d0z0':'TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest',
    'MediumLLH_d0z0':'MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest',
    'LooseLLH_d0z0':'LooseLLH_d0z0_v14',
    'LooseAndBLayerLLH_d0z0':'LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest',
    'VeryLooseLLH_d0z0':'VeryLooseLLH_d0z0_v14',
}
TagIDList={
    'TightLLH_d0z0':'TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest',
}
TagIsoList={
    'tag': 'Tag1',
    'tag_iso': 'Tag1AndTagIso',
}
BkgTemplateList ={
    'template_ge1_sel_noloose':'Template_ge1_sel_noloose',
    'template1':'TemplateVar1',
    'template6':'Template6'
}

##   Control working point 
wps = ['TightLLH_d0z0','MediumLLH_d0z0','LooseLLH_d0z0','LooseAndBLayerLLH_d0z0','VeryLooseLLH_d0z0']
# wps = ['TightLLH_d0z0']
bkg_temp = ['template_ge1_sel_noloose','template1','template6']
# bkg_temp = ['template_ge1_sel_noloose']
# bkg_temp = ['template1']

# piso_peak = [(-0.25*25000,0.5*25000),(-0.25*25000,0.4*25000),(-0.25*25000,0.6*25000)]#4375
# piso_tail = [(0.5*25000,5*25000),(0.4*25000,5*25000),(0.6*25000,5*25000) ]
# piso_peak = [(-0.25*25000,0.5*25000)]#4375
# piso_tail = [(0.5*25000,5*25000) ]

# piso_peak = [(-0.25,0.5)]#4375
# piso_tail = [(0.5,5) ]

piso_peak = [(-0.25,0.5),(-0.25,0.4),(-0.25,0.6)]#4375
piso_tail = [(0.5,5),(0.4,5),(0.6,5) ]

# mass_variations = [(80,100),(75,105),(70,110)]
mass_variations = [(75,105)]
# tag_variations = ['tag']
tag_variations = ['tag','tag_iso']

RealElectronContaminationScalingFactor = {1.0:'1p0',0.7:'0p7',1.3:'1p3'}
# RealElectronContaminationScalingFactor = {1.0:'1p0'}
doSubractMCeventsInTailScaleFactor = [1.0,0.7,1.3]
# doSubractMCeventsInTailScaleFactor = [1.0]
isolation_variable = {30:'topoetcone30_over_25GeV',40:'topoetcone40_over_25GeV'}
# isolation_variable = {30:'topoetcone30_over_25GeV'}

pwd = os.getcwd()
outputfname='scalefactors_ziso_for_comb_18.root'
foutput = ROOT.TFile(outputfname, "recreate")
foutput.mkdir('SF')
foutput.mkdir('EffData')
foutput.mkdir('EffMC')

for wp in wps:
    # mc_model = eff.Model('MC_' + wp )
    # data_model = eff.Model('Data_' + wp )

    def add_variation(wp,bkg_template,iso_variable,mc_leakage_sf,mc_ss_sf,tag,piso_p,piso_t,mlo,mhi):
        variation_name = f"{BkgTemplateList[bkg_template]}_{TagIsoList[tag]}_mll{mlo}{mhi}_ProbeIsoCut0p{str(piso_p[1])[-1]}_topoetcone{iso_variable}_doSubractMCeventsInTailScaleFactor{str(mc_ss_sf)}_RealElectronContaminationScalingFactor{RealElectronContaminationScalingFactor[mc_leakage_sf]}_{ProbeIDList[wp]}"
        print(variation_name)
        efficiency_data_path = pwd+f'/tp-efficiency-ziso-example_stat18/efficiency_Data_'+wp+'_variations.root'
        efficiency_mc_path = pwd+f'/tp-efficiency-ziso-example_stat18/efficiency_MC_'+wp+'_variations.root'
        sf_path = pwd+f'/tp-scalefactor-ziso-example_stat18/scalefactor_'+wp+'_variations.root'
        effdata_root = ROOT.TFile(efficiency_data_path)
        effmc_root = ROOT.TFile(efficiency_mc_path)
        sf_root = ROOT.TFile(sf_path)
        # print(effdata_root.Get("eff_CentralValues_"+variation_name))
        # print(effdata_root.Get("StatError_"+variation_name))
        # print(sf_root.Get("sf_CentralValues_"+variation_name))
        # print(sf_root.Get("StatError_"+variation_name))
        effdata_cv = effdata_root.Get("eff_CentralValues_"+variation_name)
        effdata_err = effdata_root.Get("StatError_"+variation_name)
        effdata_errcorr =  effdata_root.Get("StatErrorCorr_"+variation_name)
        effdata_erruncorr =  effdata_root.Get("StatErrorUncorr_"+variation_name)
        effmc_cv = effmc_root.Get("eff_CentralValues_"+variation_name)
        effmc_err = effmc_root.Get("StatError_"+variation_name)
        # effmc_errcorr =  effmc_root.Get("StatErrorCorr_"+variation_name)
        # effmc_erruncorr =  effmc_root.Get("StatErrorUncorr_"+variation_name)
        sf_cv = sf_root.Get("sf_CentralValues_"+variation_name)
        sf_err = sf_root.Get("StatError_"+variation_name)
        # print(variation_name,sf_err)
        # if effdata_err is None:
        # print(variation_name)
        # assert effdata_err is not None
        # print(effdata_errcorr)

        foutput.cd('SF')
        sf_cv.Write('SF_CentralValue_'+variation_name)
        sf_err.Write('SF_StatError_'+variation_name)
        foutput.cd ('../')
        foutput.cd('EffMC')
        effmc_cv.Write('EffMC_'+variation_name)
        effmc_err.Write('EffMC_StatError_'+variation_name)
        # effmc_errcorr.Write("EffMC_StatErrorCorr_"+variation_name)
        # effmc_erruncorr.Write("EffMC_StatErrorUncorr_"+variation_name)
        foutput.cd ('../')
        foutput.cd('EffData')
        effdata_cv.Write('EffData_'+variation_name)
        effdata_err.Write('EffData_StatError_'+variation_name)
        effdata_errcorr.Write("EffData_StatErrorCorr_"+variation_name)
        effdata_erruncorr.Write("EffData_StatErrorUncorr_"+variation_name)
        
    for bkg_template in bkg_temp:

        # Nominal 
        iso_variable = 30
        mc_leakage_sf = 1.0
        mc_ss_sf = 1.0
        tag_variations = 'tag'
        piso_peak = [(-0.25,0.5)]
        piso_tail = [(0.5,5) ]
        mlo = 75
        mhi = 105
        
        # add_variation(wp,bkg_template,iso_variable,mc_leakage_sf,mc_ss_sf,tag,piso_p,piso_t,mlo,mhi)

        # Nominal 
        add_variation(wp,bkg_template,30,1.0,1.0,'tag',(-0.25,0.5),(0.5,5),75,105)

        # tag_iso
        add_variation(wp,bkg_template,30,1.0,1.0,'tag_iso',(-0.25,0.5),(0.5,5),75,105)

        # topocone 40
        add_variation(wp,bkg_template,40,1.0,1.0,'tag',(-0.25,0.5),(0.5,5),75,105)

        # Peak 0.4
        add_variation(wp,bkg_template,30,1.0,1.0,'tag',(-0.25,0.4),(0.4,5),75,105)

        # Peak 0.6
        add_variation(wp,bkg_template,30,1.0,1.0,'tag',(-0.25,0.6),(0.6,5),75,105)

        #  mc_leakage_sf down 0.7
        add_variation(wp,bkg_template,30,0.7,1.0,'tag',(-0.25,0.5),(0.5,5),75,105)

        #  mc_leakage_sf up 1.3
        add_variation(wp,bkg_template,30,1.3,1.0,'tag',(-0.25,0.5),(0.5,5),75,105)

        # mc_ss_sf down 0.7
        add_variation(wp,bkg_template,30,1.0,0.7,'tag',(-0.25,0.5),(0.5,5),75,105)

        # mc_ss_sf down 1.3
        add_variation(wp,bkg_template,30,1.0,1.3,'tag',(-0.25,0.5),(0.5,5),75,105)

        # mass 70<mee<110
        add_variation(wp,bkg_template,30,1.0,1.0,'tag',(-0.25,0.5),(0.5,5),70,110)

        # mass 80<mee<100
        add_variation(wp,bkg_template,30,1.0,1.0,'tag',(-0.25,0.5),(0.5,5),80,100)



foutput.Close()