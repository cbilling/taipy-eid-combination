import RunDirectory as rd
import EfficiencyCommon as eff
import ZIsoMethod as zm
import ROOT
from array import array
from itertools import product

'''
YOUR CONFIG FILE *MUST* CONTAIN THE FOLLOWING VARIABLES

- MODELS

These will be read by the TagAndProbe software.

You may define arbitrary other auxiliary variables and functions for use
inside this config file (from a technical perspective, your config gets
imported by TagAndProbe as a normal Python module).

For better readability and to avoid name clashes (in case we introduce further
reserved variable names), please do not use any all-uppercase variable names for
your own variables in this file.

NOTE: This example is written to run over the run directory generated when
running the selection using the example configuration named example_zmass.py.
'''

# Selections specification

# Specify the run directories produced by the event framework
# NOTE: update the name of the run directory to work on the output of
# tp analyse according to your needs




# 1. Selections specification
# Specify the run directories produced by the event framework
# NOTE: the rundir names here need to be updated!
rundir_data = rd.Rundir('tp-analyse-selectionrel22-data17_13TeV-EGAM1-grp17_v01_p5535')
rundir_mc = rd.Rundir('tp-analyse-selectionrel22-361106-EGAM1-e3601_s3681_r13144_p4940')
# Cross section (pb): mc sample at ami https://atlas-ami.cern.ch/
cross_section = 1901.1 #pb
# Luminosity (pb-1): https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists
sample_ratio_analysed = 1 #0.51 # 1.0 = 100%
luminosity = 44630.6  #pb-1
# Normalization
rundir_mc.set_normalization(cross_section, sample_ratio_analysed*luminosity)



# tight_mc = rundir_mc.get_histogram('os_base_{tag}_mll75_105/nominal/TightLLH_d0z0-TightLLH_d0z0-tag_TightLLH_d0z0/reco/pt-primary_cluster_be2_eta-m')
# tight_data = rundir_data.get_histogram('os_base_{tag}_mll75_105/nominal/TightLLH_d0z0-TightLLH_d0z0-tag_TightLLH_d0z0/reco/pt-primary_cluster_be2_eta-m')


# tight_mc_os = tight_mc.Integral(tight_mc.FindBin(80),tight_mc.FindBin(100))
# tight_data_os = tight_data.Integral(tight_data.FindBin(80),tight_data.FindBin(100))
# tight_scale_peak =  tight_data_os/tight_mc_os
# print(tight_scale_peak)
# assert False

# Models specification

# wps = {
#        'TightLLH_d0z0': 'TightLLH_d0z0',
#     #    'MediumLLH_d0z0': 'MediumLLH_d0z0',
#     #    'LooseLLH_d0z0': 'LooseLLH_d0z0',
#     #    'VeryLooseLLH_d0z0': 'VeryLooseLLH_d0z0',
#     #    'LooseAndBLayerLLH_d0z0': 'LooseAndBLayerLLH_d0z0'
# }


models = []

ProbeIDList= {
    'TightLLH_d0z0':'TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest',
    'MediumLLH_d0z0':'MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest',
    'LooseLLH_d0z0':'LooseLLH_d0z0_v14',
    'VeryLooseLLH_d0z0':'VeryLooseLLH_d0z0_v14',
    'LooseAndBLayerLLH_d0z0':'LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest',
}
TagIDList={
    'TightLLH_d0z0':'TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest',
}
TagIsoList={
    'tag': 'Tag1',
    'tag_iso': 'Tag1AndTagIso',
}
BkgTemplateList ={
    'template_ge1_sel_noloose':'Template_ge1_sel_noloose',
    'template1':'TemplateVar1',
    'template6':'Template6'
}

# variation_name = f'{piso_p[0]}'

##   Control working point 
wps = ['TightLLH_d0z0','MediumLLH_d0z0','LooseLLH_d0z0','LooseAndBLayerLLH_d0z0','VeryLooseLLH_d0z0']
# wps = ['TightLLH_d0z0']
bkg_temp = ['template_ge1_sel_noloose','template1','template6']
# bkg_temp = ['template_ge1_sel_noloose']
# bkg_temp = ['template1']

# piso_peak = [(-0.25*25000,0.5*25000),(-0.25*25000,0.4*25000),(-0.25*25000,0.6*25000)]#4375
# piso_tail = [(0.5*25000,5*25000),(0.4*25000,5*25000),(0.6*25000,5*25000) ]
# piso_peak = [(-0.25*25000,0.5*25000)]#4375
# piso_tail = [(0.5*25000,5*25000) ]

# piso_peak = [(-0.25,0.5)]#4375
# piso_tail = [(0.5,5) ]

piso_peak = [(-0.25,0.5),(-0.25,0.4),(-0.25,0.6)]
piso_tail = [(0.5,5),(0.4,5),(0.6,5) ]

# mass_variations = [(80,100),(75,105),(70,110)]
# mass_variations = [(75,105)]
tag_variations = ['tag','tag_iso']

RealElectronContaminationScalingFactor = {1.0:'1p0',0.7:'0p7',1.3:'1p3'}
doSubractMCeventsInTailScaleFactor = [1.0,0.7,1.3]
isolation_variable = {30:'topoetcone30_over_25GeV',40:'topoetcone40_over_25GeV'}
# isolation_variable = {30:'topoetcone30_over_25GeV'}
for wp in wps:
    mc_model = eff.Model('MC_' + wp )
    data_model = eff.Model('Data_' + wp )

    def add_variation(wp,bkg_template,iso_variable,mc_leakage_sf,mc_ss_sf,tag,piso_p,piso_t,mlo,mhi):
        variation_name = f"{BkgTemplateList[bkg_template]}_{TagIsoList[tag]}_mll{mlo}{mhi}_ProbeIsoCut0p{str(piso_p[1])[-1]}_topoetcone{iso_variable}_doSubractMCeventsInTailScaleFactor{str(mc_ss_sf)}_RealElectronContaminationScalingFactor{RealElectronContaminationScalingFactor[mc_leakage_sf]}_{ProbeIDList[wp]}"
        print(' variation name: ',variation_name)
            # Define a model for each efficiency measurement
        mc_os = zm.ZisoSelection(rundir_mc, f'os_base_{tag}_mll{mlo}_{mhi}',binning=[[15, 20], [-2.47, -2.01, -1.52, -1.37, -0.8, -0.1, 0.0, 0.1, 0.8, 1.37, 1.52, 2.01, 2.47], None],isolation_variable=isolation_variable[iso_variable])
        data_os = zm.ZisoSelection(rundir_data, f'os_base_{tag}_mll{mlo}_{mhi}',binning=[[15, 20], [-2.47, -2.01, -1.52, -1.37, -0.8, -0.1, 0.0, 0.1, 0.8, 1.37, 1.52, 2.01, 2.47], None],isolation_variable=isolation_variable[iso_variable])
        mc_ss_bkg = zm.ZisoSelection(rundir_mc, f'ss_background_{tag}_mll{mlo}_{mhi}',binning=[[15, 20], [-2.47, -2.01, -1.52, -1.37, -0.8, -0.1, 0.0, 0.1, 0.8, 1.37, 1.52, 2.01, 2.47], None],isolation_variable=isolation_variable[iso_variable])
        data_ss_bkg = zm.ZisoSelection(rundir_data, f'ss_background_{tag}_mll{mlo}_{mhi}',binning=[[15, 20], [-2.47, -2.01, -1.52, -1.37, -0.8, -0.1, 0.0, 0.1, 0.8, 1.37, 1.52, 2.01, 2.47], None],isolation_variable=isolation_variable[iso_variable])
        # print(dir(data_os))
        # print(data_os.preselected_probe)
        # Add variation to the model
        data_model.add_variation(
            label=variation_name,
            numerator=data_os.passing_probes[wp], # data used to extract final yield(s)
            # denominator=data_os.preselected_probe, # data used to extract final yield(s)
            denominator=data_os.preselected_probe, # data used to extract final yield(s)
            # denominator=data_os.passing_probes[uid], # data used to extract final yield(s)
            components={
                'background_subtraction_method' : 'normalisation_in_mass_tail',
                'background_template_denominator' : data_ss_bkg.passing_probes[f'ziso_{bkg_template}_den'], 
                'background_template_numerator' : data_ss_bkg.passing_probes[f'ziso_{bkg_template}_num'],
                'mc_ss_background_template_denominator' : mc_ss_bkg.passing_probes[f'ziso_{bkg_template}_den'],
                'mc_ss_background_template_numerator' : mc_ss_bkg.passing_probes[f'ziso_{bkg_template}_num'],
                'signal_mc_numerator' : mc_os.passing_probes[wp],
                'signal_mc_denominator' : mc_os.preselected_probe,
                'tight_data_80' :  rundir_data.get_histogram('os_base_tag_iso_mll80_100/nominal/tag_iso_TightLLH_d0z0/reco/m'),
                "tight_mc_80" : rundir_mc.get_histogram('os_base_tag_iso_mll80_100/nominal/tag_iso_TightLLH_d0z0/reco/m'),
                "do_abseta_in_last_pt_bin" : True,
                'mc_leakage_sf': mc_leakage_sf,
                'mc_ss_sf': mc_ss_sf,
                'isolation_variable':isolation_variable[iso_variable],
                # 'pt_bins_with_abseta':[1],
                },
            attributes={
                'integration_range' : piso_p, # where the yield is extracted
                'normalisation_region' : piso_t, # Control Region
                 'pt_bins_with_abseta' : [1],
                },
            )

        mc_model.add_variation(
            label=variation_name,
            numerator=mc_os.passing_probes[wp], # data used to extract final yield(s)
            denominator=mc_os.preselected_probe, # data used to extract final yield(s)
            # denominator=mc_os.preselected_probe, # data used to extract final yield(s)
            components={
                "do_abseta_in_last_pt_bin" : True,
                'isolation_variable':isolation_variable[iso_variable],
                #  'pt_bins_with_abseta' : [1],
                },
            attributes={
                'integration_range' : piso_p, # where the yield is extracted
                # 'normalisation_region' : piso_t, # Control Region
                 'pt_bins_with_abseta' : [1],
                }
            )
    
    for bkg_template in bkg_temp:

        # Nominal 
        iso_variable = 30
        mc_leakage_sf = 1.0
        mc_ss_sf = 1.0
        tag_variations = 'tag'
        piso_peak = [(-0.25,0.5)]
        piso_tail = [(0.5,5) ]
        mlo = 75
        mhi = 105
        
        # add_variation(wp,bkg_template,iso_variable,mc_leakage_sf,mc_ss_sf,tag,piso_p,piso_t,mlo,mhi)

        # Nominal 
        add_variation(wp,bkg_template,30,1.0,1.0,'tag',(-0.25,0.5),(0.5,5),75,105)

        # tag_iso
        add_variation(wp,bkg_template,30,1.0,1.0,'tag_iso',(-0.25,0.5),(0.5,5),75,105)

        # topocone 40
        add_variation(wp,bkg_template,40,1.0,1.0,'tag',(-0.25,0.5),(0.5,5),75,105)

        # Peak 0.4
        add_variation(wp,bkg_template,30,1.0,1.0,'tag',(-0.25,0.4),(0.4,5),75,105)

        # Peak 0.6
        add_variation(wp,bkg_template,30,1.0,1.0,'tag',(-0.25,0.6),(0.6,5),75,105)

        #  mc_leakage_sf down 0.7
        add_variation(wp,bkg_template,30,0.7,1.0,'tag',(-0.25,0.5),(0.5,5),75,105)

        #  mc_leakage_sf up 1.3
        add_variation(wp,bkg_template,30,1.3,1.0,'tag',(-0.25,0.5),(0.5,5),75,105)

        # mc_ss_sf down 0.7
        add_variation(wp,bkg_template,30,1.0,0.7,'tag',(-0.25,0.5),(0.5,5),75,105)

        # mc_ss_sf down 1.3
        add_variation(wp,bkg_template,30,1.0,1.3,'tag',(-0.25,0.5),(0.5,5),75,105)

        # mass 70<mee<110
        add_variation(wp,bkg_template,30,1.0,1.0,'tag',(-0.25,0.5),(0.5,5),70,110)

        # mass 80<mee<100
        add_variation(wp,bkg_template,30,1.0,1.0,'tag',(-0.25,0.5),(0.5,5),80,100)

    models.append(data_model)
    models.append(mc_model)

# Combine all the model definitions. This is what TagAndProbe will work with.
MODELS = eff.ModelCollection(*models)
