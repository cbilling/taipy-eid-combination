## tp-efficiency configuration file for electron ID efficiency measurement
## Zmass method with normalisation by fit

import RunDirectory as rd
import EfficiencyCommon as eff
import ZMassMethod as zm

from array import array
from itertools import product

# 1. Selections specification
# Specify the run directories produced by the event framework
# NOTE: the rundir names here need to be updated!
rundir_data = rd.Rundir('tp-analyse-selection_run2_r22_oldLH_final-data18_13TeV-EGAM1-grp18_v01_p5535')
rundir_mc = rd.Rundir('tp-analyse-selection_run2_r22_oldLH_final-361106-EGAM1-e3601_s3681_r13145_p4940')
# Cross section (pb): mc sample at ami https://atlas-ami.cern.ch/
cross_section = 1901.1 #pb
# Luminosity (pb-1): https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists
sample_ratio_analysed = 1 #0.51 # 1.0 = 100%
luminosity = 58791.6 #pb-1
# Normalization
rundir_mc.set_normalization(cross_section, sample_ratio_analysed*luminosity)

# 2. Models specification

# Create dictionary to organise the model name and corresponding electron ID,
# different only for the preliminary estimation of the TightLLH ID
wps = {'TightLLH_d0z0_est': 'TightLLH_d0z0',
       'TightLLH_d0z0': 'TightLLH_d0z0',
       # 'MediumLLH_d0z0': 'MediumLLH_d0z0',
       # 'LooseAndBLayerLLH_d0z0': 'LooseAndBLayerLLH_d0z0',
       # 'LooseLLH_d0z0': 'LooseLLH_d0z0',
       # 'VeryLooseLLH_d0z0': 'VeryLooseLLH_d0z0',
}
observables = ['pt', 'primary_cluster_be2_eta']
bin_var = 'coarse'

models = []
for (name, wp) in wps.items():

    # Define a model for each efficiency measurement

    mc_model = eff.Model(f'MC_{name}')
    data_model = eff.Model(f'Data_{name}',
                           priority=1 if name == 'TightLLH_d0z0_est' else None,
                           save_to_collection=True if name == 'TightLLH_d0z0_est' else False)

    def add_variation(variation_name, tag_var, bkg_var, mll_window, binning_num, binning_den, binning_sig_cont, variation_sig_cont, variation_bkg_fit, tight_ref, sig_cont_order_25=1):

        # Specify the tag-and-probe selection in which we are interested.
        data_os = zm.ZmassSelection(rundir_data, f'os_{tag_var}_{bkg_var}_{bin_var}', observables=observables,
            binning=[[15, 20], [-2.47, -2.37, -2.01, -1.52, -1.37, -0.8, -0.1, 0.0, 0.1, 0.8, 1.37, 1.52, 2.01, 2.37, 2.47], None])
        mc_os = zm.ZmassSelection(rundir_mc, f'os_{tag_var}_{bkg_var}_{bin_var}', observables=observables,
            binning=[[15, 20], [-2.47, -2.37, -2.01, -1.52, -1.37, -0.8, -0.1, 0.0, 0.1, 0.8, 1.37, 1.52, 2.01, 2.37, 2.47], None])

        # Add variation to the data model
        data_model.add_variation(
            label=variation_name,
            numerator=data_os.passing_probes[wp], # data used to extract final yield(s)
            denominator=data_os.preselected_probe, # data used to extract final yield(s)
            components={
                'background_subtraction_method' : 'normalisation_by_fit',
                'background_template_numerator' : data_os.passing_probes[f'bkg_{bkg_var}'],
                'background_template_denominator' : data_os.passing_probes[f'bkg_{bkg_var}'],
                'background_signal_contamination_template' : mc_os.passing_probes[f'bkg_{bkg_var}'],
                'background_template_yield_min' : 15,
                'data_yield_min' : None,
                'signal_mc_numerator' : mc_os.passing_probes[wp],
                'signal_mc_denominator' : mc_os.preselected_probe,
                'tight_ref' : tight_ref,
                },
            attributes={
                'integration_range' : mll_window, # where the yield is extracted
                'do_signal_contamination_fit' : True,
                'variation_bkg_fit' : variation_bkg_fit,
                'variation_sig_cont' : variation_sig_cont,
                'binning_sig_cont' : binning_sig_cont,
                'binning_den' : binning_den,
                'binning_num' : binning_num,
                'binning_weight' : [60, 65, 70, 75, 78, 80, 82, 84, 86, 88, 90,
                                    92, 94, 96, 98, 100, 102, 105, 130, 250 ],
                'pt_bins_with_abseta' : [1],
                'signal_contamination_order_25' : sig_cont_order_25,
                'save_all' : tight_ref is not None,
                },
            )

        # Add variation to the MC model
        mc_model.add_variation(
            label=variation_name,
            numerator=mc_os.passing_probes[wp], # data used to extract final yield(s)
            denominator=mc_os.preselected_probe, # data used to extract final yield(s)
            components={
                },
            attributes={
                'integration_range' : mll_window, # where the yield is extracted
                'pt_bins_with_abseta' : [1],
                }
            )

    # Nominal
    b_num_nom = [65, 80, 100, 250]
    b_den_nom = [65, 70, 75, 80, 100, 120, 150, 250]
    b_sig_nom = [70,73,76,79,82,85,88,91,94,97,100,103,
                                      106,109,112,115]
    tight_ref = f'Data_TightLLH_d0z0_est' if name != 'TightLLH_d0z0_est' else ''

    add_variation('nominal', 'tag', 'var4', (75, 105),
                  b_num_nom, b_den_nom, b_sig_nom, 'NOM', 'NOM', tight_ref)


    # Tag variation
    add_variation('tag_iso_up', 'tag_iso_up', 'var4', (75, 105),
                  b_num_nom, b_den_nom, b_sig_nom, 'NOM', 'NOM', tight_ref)
    add_variation('tag_iso_do', 'tag_iso_do', 'var4', (75, 105),
                  b_num_nom, b_den_nom, b_sig_nom, 'NOM', 'NOM', tight_ref)
    # mll window variation
    add_variation('int_80_100', 'tag', 'var4', (80, 100),
                  b_num_nom, b_den_nom, b_sig_nom, 'NOM', 'NOM', tight_ref)
    add_variation('int_70_110', 'tag', 'var4', (70, 110),
                  b_num_nom, b_den_nom, b_sig_nom, 'NOM', 'NOM', tight_ref)
    # Bkg variation
    add_variation('var1', 'tag', 'var1', (75, 105),
                  b_num_nom, b_den_nom, b_sig_nom, 'NOM', 'NOM', tight_ref)
    add_variation('var3', 'tag', 'var3', (75, 105),
                  b_num_nom, b_den_nom, b_sig_nom, 'NOM', 'NOM', tight_ref)
    # Bkg fit range
    add_variation('bkg_fit_range_lo', 'tag', 'var4', (75, 105),
                  [65, 80, 100, 120], [65, 70, 75, 80, 100],
                  b_sig_nom, 'NOM', 'NOM', tight_ref)
    add_variation('bkg_fit_range_hi', 'tag', 'var4', (75, 105),
                  [80, 100, 120, 250], [80, 100, 120, 150, 250], b_sig_nom, 'NOM', 'NOM', tight_ref)
    # Turn off mc reweighting
    add_variation('mc_weight_off', 'tag', 'var4', (75, 105),
                  b_num_nom, b_den_nom, b_sig_nom, 'NOM', 'NOM', '')
    # Signal cont shift
    # add_variation('sig_cont_range_lo', 'tag', 'var4', (75, 105),
    #               b_num_nom, b_den_nom,
    #               [65,68,71,74,77,80,83,86,89,92,95,98,101,104,107,110],
    #               'NOM', 'NOM', tight_ref)
    # Signal cont fit unc
    add_variation('sig_cont_up', 'tag', 'var4', (75, 105),
                  b_num_nom, b_den_nom, b_sig_nom, 'UP', 'NOM', tight_ref)
    add_variation('sig_cont_do', 'tag', 'var4', (75, 105),
                  b_num_nom, b_den_nom, b_sig_nom, 'DO', 'NOM', tight_ref)
    add_variation('bkg_model_alt25', 'tag', 'var4', (75, 105),
                  b_num_nom, b_den_nom, b_sig_nom,
                  'NOM', 'NOM', tight_ref, sig_cont_order_25=3)
    # Bkg fit unc
    add_variation('bkg_fit_up', 'tag', 'var4', (75, 105),
                  b_num_nom, b_den_nom, b_sig_nom, 'NOM', 'UP', tight_ref)
    add_variation('bkg_fit_do', 'tag', 'var4', (75, 105),
                  b_num_nom, b_den_nom, b_sig_nom, 'NOM', 'DO', tight_ref)

    models.append(data_model)
    models.append(mc_model)

# 3. Collection specification
# Combine all the model definitions. This is what TagAndProbe will work with.
MODELS = eff.ModelCollection(*models)
