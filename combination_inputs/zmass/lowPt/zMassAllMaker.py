#!/usr/bin/env python

import ROOT

year = 2015
working_points = [
    "LooseAndBLayerLLH_d0z0",
    "LooseLLH_d0z0",
    "MediumLLH_d0z0",
    "TightLLH_d0z0",
    "VeryLooseLLH_d0z0",
]
end = "_variations.root"
# List of the three input ROOT files
input_files = [
    f"tp-efficiency-zmass-efficiency_r22_oldLH_final_{year}s_lowpt_dec6/efficiency_Data_",
    f"tp-efficiency-zmass-efficiency_r22_oldLH_final_{year}s_lowpt_dec6/efficiency_MC_",
    f"tp-scalefactor-zmass-efficiency_r22_oldLH_final_{year}s_lowpt_dec6/scalefactor_",
]

# Output ROOT file
output_file = f"scalefactors_zmass_for_comb_{year}.root"

# Create a new output ROOT file
output_root = ROOT.TFile(output_file, "RECREATE")

for wp in working_points:
    # Loop through the input files
    i = 0
    for input_file in input_files:
        input_file = input_file + wp + end
        # Open the input ROOT file
        input_root = ROOT.TFile(input_file, "READ")

        # Get the list of keys (items) in the input file
        keys = input_root.GetListOfKeys()

        # Loop through the keys
        for key in keys:
            # Get the item from the input file
            item = input_root.Get(key.GetName())
            # print(item)
            # Clone the item to preserve the original
            # and write it to the output file
            if i == 0:
                extra = "EffData_"
            if i == 1:
                extra = "EffMC_"
            if i == 2:
                extra = ""
            output_root.cd()
            cloned_item = item.Clone()
            cloned_item.Write(extra + key.GetName() + "_" + wp)

        # Close the input ROOT file
        input_root.Close()
        i += 1
# Close the output ROOT file
output_root.Close()
