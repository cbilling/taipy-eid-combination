import RunDirectory as rd
import EfficiencyCommon as eff
import TauCutMethod as tc
import numpy as np

import itertools

from array import array

'''
YOUR CONFIG FILE *MUST* CONTAIN THE FOLLOWING VARIABLES

- MODELS

These will be read by the TagAndProbe software.

You may define arbitrary other auxiliary variables and functions for use
inside this config file (from a technical perspective, your config gets
imported by TagAndProbe as a normal Python module).

For better readability and to avoid name clashes (in case we introduce further
reserved variable names), please do not use any all-uppercase variable names for
your own variables in this file
'''

# Selections specification

# Specify the run directories produced by the event framework
# NOTE: update the name of the run directory to work on the output of
# tp analyse according to your needs

rundir_data = rd.Rundir('../Sully/tp-analyse-lifetime-data18_13TeV-EGAM2-grp18_v01_p5535/')
rundir_mc = rd.Rundir('../Sully/tp-analyse-pp/')
rundir_mc_np = rd.Rundir('../Sully/tp-analyse-bb/')

# Specify the tag-and-probe selection in which we are interested
models = []

# To check nb of variations for each model
list_var = []

# Models specification
for uid in ['LooseAndBLayerLLH', 'MediumLLH', 'TightLLH']: # 'VeryLooseLLH',  'LooseLLH',

    # Data efficiency
	
    # Define a model for each data efficiency measurement.
	data_model = eff.Model('Data_' + uid, int_lumi=6.0, priority = None)
							# save_to_collection=True if uid == 'TightLLH' else False
                            
    # Same for MC efficiency. MC models have priority over data models because we want to use MC final fit parameters as initial values for data fit parameters.
	mc_model = eff.Model('MC_' + uid, int_lumi=6.0, priority=1)
    
    # Once the models for MC and data are defined, then we can start adding variation
    
	# First variations on the event selections (different input histograms)  
	# for tau, iso in itertools.product(['_tauprompt20','_tauprompt40'], ['_iso002','_iso015','_iso50']) : #_iso02
	for tau, iso, mll_window, bkgOS_order in zip(['_tauprompt20', '_tauprompt40', '_tauprompt20', '_tauprompt20', '_tauprompt20', '_tauprompt20',], ['_iso50', '_iso50','_iso015', '_iso002', '_iso50', '_iso50'], [(2.7, 3.4), (2.7, 3.4), (2.7, 3.4), (2.7, 3.4), (2.8, 3.3), (2.7, 3.4)], [3, 3, 3, 3, 3, 2]) :

		# Data pT*eta*m histograms			         
	    data_base = tc.TauCutSelection(rundir_data, 'base_selection'+tau+iso, observables_probe = ['pt','primary_cluster_be2_abseta'], observables_tagprobe=['m']) #'primary_cluster_be2_abseta' previously
	    data_ss = tc.TauCutSelection(rundir_data, 'same-sign_selection'+tau.replace('tau','')+iso,observables_probe = ['pt','primary_cluster_be2_abseta'],observables_tagprobe=['m'])

		# Prompt MC pT*eta*m histograms
	    mc_base = tc.TauCutSelection(rundir_mc, 'base_selection'+tau+iso,observables_probe = ['pt','primary_cluster_be2_abseta'],observables_tagprobe=['m'])

		# Non-prompt MC pT*eta*m histograms
	    mc_np_base = tc.TauCutSelection(rundir_mc_np, 'base_selection'+tau+iso,observables_probe = ['pt','primary_cluster_be2_abseta'],observables_tagprobe=['m'])
	    mc_fraction = tc.TauCutSelection(rundir_mc, 'base_selection_taupromptall'+iso,observables_probe = ['pt','primary_cluster_be2_abseta'],observables_tagprobe=['pt','abs_y'])
	    mc_np_fraction = tc.TauCutSelection(rundir_mc_np, 'base_selection_taupromptall'+iso,observables_probe = ['pt','primary_cluster_be2_abseta'],observables_tagprobe=['pt','abs_y'])
	    mc_fraction_cut = tc.TauCutSelection(rundir_mc, 'base_selection_taupromptall'+iso,observables_probe = ['pt','primary_cluster_be2_abseta'],observables_tagprobe=['pseudoproper_lifetime'])
	    mc_np_fraction_cut = tc.TauCutSelection(rundir_mc_np, 'base_selection_taupromptall'+iso,observables_probe = ['pt','primary_cluster_be2_abseta'],observables_tagprobe=['pseudoproper_lifetime'])
	    mc_lifetime_cut = tc.TauCutSelection(rundir_mc_np, 'base_selection'+tau+iso,observables_probe = ['pt','primary_cluster_be2_abseta'],observables_tagprobe=['pseudoproper_lifetime'])
	                            
	    # Check the uid used in the tp analyse (LLH + iso) step and match it with the LLH only
	    selection_uid = []
	    for probe in data_base.passing_probes :
	    	selection_uid.append(probe)
	    for p in selection_uid :
	    	if uid in p :
	    		sel_uid = p
	    # print('LLH = ', uid, sel_uid)
	
	
	    # Then from each input file, specify variations on the fit
	    # for mll_window, bkgOS_order in itertools.product([(2.7, 3.4)], [3]):
	        
        # For MC model
	    mc_model.add_variation(
	        label=tau+iso+'_Cheb_'+str(bkgOS_order)+'_mll_%s_%s' % mll_window ,
	        numerator=mc_base.passing_probes[sel_uid], # data used to extract final yield(s)
	        denominator=mc_base.preselected_probe, # data used to extract final yield(s)
	        components={
	            'lifetime_cut' : mc_lifetime_cut.preselected_probe,
	            'prompt_4D' : mc_fraction.preselected_probe,
	            'np_4D' : mc_np_fraction.preselected_probe,
	            'prompt_3D' : mc_fraction_cut.preselected_probe,
	            'np_3D' : mc_np_fraction_cut.preselected_probe,
	            'np_numerator' : mc_np_base.passing_probes[sel_uid],
	            'np_denominator' : mc_np_base.preselected_probe
	            },
	        attributes={
	            'integration_range' : mll_window, # where the yield is extracted
	            'fit_range' : (1.8, 4.6),
	            'bkgOS_order' : bkgOS_order,
	            }
	        )

        # For data model
	    data_model.add_variation(
	        label=tau+iso+'_Cheb_'+str(bkgOS_order)+'_mll_%s_%s' % mll_window,
	        numerator=data_base.passing_probes[sel_uid], # data used to extract final yield(s)
	        denominator=data_base.preselected_probe, # data used to extract final yield(s)
	        components={
	            'numerator_SS' : data_ss.passing_probes[sel_uid],
	            'denominator_SS' : data_ss.preselected_probe
	            },
	        attributes={
	            'integration_range' : mll_window, # where the yield is extracted
	            'fit_range' : (1.8, 4.6),
	            'bkgOS_order' : bkgOS_order,
	            },
	        )
        
	        # if uid == 'MediumLLH' :
	        #     list_var.append(tau+iso+'_Cheb_'+str(bkgOS_order)+'_mll_%s_%s' % mll_window)
	models.append(mc_model)
	models.append(data_model)
	
    
# print('nb models = ', len(models)) 
# print('nb variations per model = ', len(list_var))
# print('list of variations = ', list_var)


# Combine all the model definitions. This is what TagAndProbe will work with.
MODELS = eff.ModelCollection(*models)
