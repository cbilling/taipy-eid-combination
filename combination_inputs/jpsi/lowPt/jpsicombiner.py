#!/usr/bin/env python

import ROOT

working_points = ["TightLLH", "MediumLLH", "LooseAndBLayerLLH"]
# List of the three input ROOT files
input_files = [
    "tp-scalefactor-taucut-eff_taucut_2015/scalefactor_{}_central_value.root",
    "tp-scalefactor-taucut-eff_taucut_2015/scalefactor_{}_variations.root",
]

# Output ROOT file
output_file = "scalefactors_jpsi_for_comb_2015.root"

# Create a new output ROOT file
output_root = ROOT.TFile(output_file, "RECREATE")

for wp in working_points:
    # Loop through the input files
    for input_file in input_files:
        input_file1 = input_file.format(wp)
        # Open the input ROOT file
        input_root = ROOT.TFile(input_file1, "READ")

        # Get the list of keys (items) in the input file
        keys = input_root.GetListOfKeys()

        # Loop through the keys
        for key in keys:
            # Get the item from the input file
            item = input_root.Get(key.GetName())
            # print(item)
            # Clone the item to preserve the original
            # and write it to the output file

            output_root.cd()
            cloned_item = item.Clone()
            cloned_item.Write(key.GetName() + "_" + wp + "_d0z0")

        # Close the input ROOT file
        input_root.Close()

# Close the output ROOT file
output_root.Close()
