import taipy as tp
from config.config import *
import numpy as np
from taipy import Config
from taipy.gui import Gui, Icon, navigate
from pages.compareScenarios import *
from pages.finalize import *
from pages.scenarioMaker import *
from pages.uncertainties import *

Config.configure_job_executions(mode="standalone", nb_of_workers=4)
# Run of the Core
tp.Core().run()
# Creation of the scenario and execution
# scenario = tp.create_scenario(scenario_cfg)
# prebinScenario = tp.create_scenario(prebinScenario_cfg, name="latest1")
# prebinScenario.WorkingPointID.write("TightLLH_d0z0")
# prebinScenario.Year.write("2018")
# prebinScenario.RebinMethod.write("pre_bin")
# prebinScenario.ExtraBin.write("no_extra")
# tp.submit(prebinScenario)
# prebinScenario1 = tp.create_scenario(prebinScenario_cfg, name="latest_extra")
# prebinScenario1.WorkingPointID.write("TightLLH_d0z0")
# prebinScenario1.Year.write("2018")
# prebinScenario1.RebinMethod.write("pre_bin")
# prebinScenario1.ExtraBin.write("extra")
# tp.submit(prebinScenario1)
# scenario = tp.get("4e2e1ee2-ce53-416d-9e4d-cfa3771b33ba")
# scenario.historical_temperature.write(data)
# scenario.date_to_forecast.write(dt.datetime.now())
# tp.submit(scenario)

# print("Value at the end of task", scenario.predictions.read())


year = None
workingPoint = None


def menu_fct(state, var_name, var_value):
    """Function that is called when there is a change in the menu control."""
    state.page = var_value["args"][0]
    navigate(state, state.page.replace(" ", "-"))


menu_lov = [
    ("Scenario Maker", Icon("images/histogram_menu.svg", "Scenario Maker")),
    ("Compare Scenarios", Icon("images/model.svg", "Compare Scenarios")),
    ("Finalize", Icon("images/compare.svg", "Finalize")),
]

root_md = """
<|toggle|theme|>
<|navbar|>
"""
pages = {
    "/": root_md,
    "Scenario-Maker": scenarioMaker_md,
    "Uncertainties": uncertanties_md,
    "Compare-Scenarios": compareScenarios_md,
    "Finalize": finalize_md,
}
if __name__ == "__main__":
    tp.Gui(pages=pages).run(use_reloader=False, host="0.0.0.0", port=5000)
