from algos.writeRoot import writeRootFiles_decomp
from taipy.gui import Markdown, notify

scenario_yr1 = None
scenario_yr2 = None
scenario_yr3 = None
scenario_yr4 = None
root_file_path = None
what_to_root = None  # Zmass, Ziso, Jpsi, Zmass lo pt, Ziso lo pt, Zcomb lo pt, Full


def runRootButton(state):
    # Need to make sure they all come from the same working point,
    # rebin method, number of bins
    wpToMatch = ""
    rebinMethToMatch = ""
    extraBinToMatch = ""
    indirs = []
    years = []
    for scenario in [
        state.scenario_yr1,
        state.scenario_yr2,
        state.scenario_yr3,
        state.scenario_yr4,
    ]:
        if scenario is not None:
            print(scenario.WorkingPointID.read())
            if wpToMatch == "":
                wpToMatch = scenario.WorkingPointID.read()
            else:
                if wpToMatch != scenario.WorkingPointID.read():
                    notify(
                        state,
                        "info",
                        f"{wpToMatch} did not match {scenario.WorkingPointID.read()}, select different scenarios",
                    )
                    return
            # if rebinMethToMatch == "":
            #    rebinMethToMatch = scenario.RebinMethod.read()
            # else:
            #    if rebinMethToMatch != scenario.RebinMethod.read():
            #        notify(
            #            state,
            #            "info",
            #            f"{rebinMethToMatch} did not match {scenario.RebinMethod.read()}, select different scenarios",
            #        )
            #        return

            if state.what_to_root == "Full":
                indirs.append(
                    {
                        "SF": scenario.Zcombhi.read(),
                        "x": scenario.Zisohix.read(),
                        "y": scenario.Zisohiy.read(),
                    }
                )
                years.append(scenario.Year.read())
                indirs.append(
                    {
                        "SF": scenario.ZJpsicomb.read(),
                        "x": scenario.Jpsi_x.read(),
                        "y": scenario.Jpsi_y.read(),
                    }
                )
                years.append(scenario.Year.read())
            elif state.what_to_root == "Zmass":
                indirs.append(
                    {
                        "SF": scenario.Zmasshi.read(),
                        "x": scenario.Zmasshix.read(),
                        "y": scenario.Zmasshiy.read(),
                    }
                )
                years.append(scenario.Year.read())
                indirs.append(
                    {
                        "SF": scenario.Zmasslo.read(),
                        "x": scenario.Zmasslox.read(),
                        "y": scenario.Zmassloy.read(),
                    }
                )
                years.append(scenario.Year.read())
            elif state.what_to_root == "Ziso":
                indirs.append(
                    {
                        "SF": scenario.Zisohi.read(),
                        "x": scenario.Zisohix.read(),
                        "y": scenario.Zisohiy.read(),
                    }
                )
                years.append(scenario.Year.read())
                indirs.append(
                    {
                        "SF": scenario.Zisolo.read(),
                        "x": scenario.Zisolox.read(),
                        "y": scenario.Zisoloy.read(),
                    }
                )
                years.append(scenario.Year.read())
            elif state.what_to_root == "Jpsi":
                indirs.append(
                    {
                        "SF": scenario.JpsiSystDecorr.read(),
                        "x": scenario.Jpsi_x.read(),
                        "y": scenario.Jpsi_y.read(),
                    }
                )
                years.append(scenario.Year.read())
            elif state.what_to_root == "Zmass_lo_pt":
                indirs.append(
                    {
                        "SF": scenario.JpsiSystDecorr.read(),
                        "x": scenario.Jpsix.read(),
                        "y": scenario.Jpsiy.read(),
                    }
                )
                years.append(scenario.Year.read())
    outfname = f"/afs/cern.ch/user/c/cbilling/tNp/tnp_taipy/algos/eff-plots/toolfiles_sully/efficiencySF.offline.{wpToMatch}.{state.what_to_root}.root"
    writeRootFiles_decomp(indirs=indirs, years=years, outfname=outfname)


finalize_md = """
# **Finalize**{: .color-primary} page
#### Here you can writeout your results with or without eigenvector decomposition
#### Select **four**{: .color-primary} scenarios, one corresponding to each year to combine
<|layout|columns=1 1 1 1|gap=50px|

<|{scenario_yr1}|scenario_selector|show_add_button=False|>
<|{scenario_yr2}|scenario_selector|show_add_button=False|>
<|{scenario_yr3}|scenario_selector|show_add_button=False|>
<|{scenario_yr4}|scenario_selector|show_add_button=False|>

What do you want to make a root file for?
<|{what_to_root}|selector|lov=Zmass;Ziso;Jpsi;Zmass_lo_pt;Ziso_lo_pt;Zcomb_lo_pt;Full|>
Folder name to save root file?
<|{root_file_path}|input|>
<|Make Root File|button|on_action=runRootButton|>
|>

<|layout|columns=1 1|gap=50px|

|>"""
