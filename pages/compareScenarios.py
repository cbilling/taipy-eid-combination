from taipy.gui import Markdown
import numpy as np


def get_variance(arr_unc):
    # usually we work with arrays, where source of uncertainties are stored in columns and are fully correlated between rows
    # sometimes it's helpful to get just the variance (i.e. quadratic sum of columns)
    return np.sum(arr_unc**2, axis=1)


def get_standarddev(arr_unc):
    # same as variance, but holding the standard deviation
    return np.sqrt(get_variance(arr_unc))


left_scenario = None
right_scenario = None

margin = dict(l=8, t=1, b=5)
l1 = {"margin": dict(t=5, b=0)}
l2 = {"margin": dict(t=0)}

#### Z plot info
rep_zdata = {"x": [0, 1], "LeftZcomb": [1, 1], "RightZcomb": [1, 1]}
rep_options1 = {
    # Create the error bar information:
    "error_y": {"type": "data", "array": [0.1, 0.1]}
}
rep_options2 = {
    # Create the error bar information:
    "error_y": {"type": "data", "array": [0.1, 0.1]}
}
rep_options3 = {
    # Create the error bar information:
    "error_y": {"type": "data", "array": [0.1, 0.1]}
}
#### Z plot info
rep_zdata_ratio = {
    "x": [0, 1],
    "LeftZcomb": [1, 1],
    "RightZcomb": [1, 1],
}

rep_zdata_err = {"x": [0, 1], "LeftZcomb": [1, 1], "RightZcomb": [1, 1]}
rep_zdata_ratio_err = {
    "x": [0, 1],
    "LeftZcomb": [1, 1],
    "RightZcomb": [1, 1],
}

rep_options1_ratio = {
    # Create the error bar information:
    "error_y": {"type": "data", "array": [0.1, 0.1]}
}
rep_options2_ratio = {
    # Create the error bar information:
    "error_y": {"type": "data", "array": [0.1, 0.1]}
}
rep_options3_ratio = {
    # Create the error bar information:
    "error_y": {"type": "data", "array": [0.1, 0.1]}
}
#### Z plot info
left_zdata = {"x": [0, 1], "Zcomb": [1, 1], "Zmass": [1, 1], "Ziso": [1, 1]}
left_options1 = {
    # Create the error bar information:
    "error_y": {"type": "data", "array": [0.1, 0.1]}
}
left_options2 = {
    # Create the error bar information:
    "error_y": {"type": "data", "array": [0.1, 0.1]}
}
left_options3 = {
    # Create the error bar information:
    "error_y": {"type": "data", "array": [0.1, 0.1]}
}

right_zdata = left_zdata
right_options1 = right_options2 = right_options3 = left_options1

comp_zdata = {
    "x_left": [0, 1],
    "x_right": [0, 1],
    "Zcomb_left": [1, 1],
    "Zcomb_right": [1, 1],
}
comp_options1 = comp_options2 = left_options1

ratio = {"x": [0, 1], "ratio": [1, 1]}
ratio_options = left_options1

#### J plot info
left_jzdata = {"x": [0, 1], "JZcomb": [1, 1], "Jpsi": [1, 1], "Zcomb": [1, 1]}
left_joptions1 = {
    # Create the error bar information:
    "error_y": {"type": "data", "array": [0.1, 0.1]}
}
left_joptions2 = {
    # Create the error bar information:
    "error_y": {"type": "data", "array": [0.1, 0.1]}
}
left_joptions3 = {
    # Create the error bar information:
    "error_y": {"type": "data", "array": [0.1, 0.1]}
}

right_jzdata = left_jzdata
right_joptions1 = right_joptions2 = right_joptions3 = left_joptions1

comp_jzdata = {
    "x_left": [0, 1],
    "x_right": [0, 1],
    "JZcomb_left": [1, 1],
    "JZcomb_right": [1, 1],
}
comp_joptions1 = comp_joptions2 = left_joptions1

ratioj = {"x": [0, 1], "ratio": [1, 1]}
ratio_joptions = left_joptions1


def compile_zerror_plots(state):
    if (state.right_scenario is not None) and (state.left_scenario is not None):
        state.rep_zdata = make_repeating_eta_plot(
            state.left_scenario, state.right_scenario
        )
        state.rep_zdata_ratio = make_repeating_eta_ratio(
            state.left_scenario, state.right_scenario
        )
        state.rep_zdata_err = make_repeating_eta_plot_err(
            state.left_scenario, state.right_scenario
        )
        state.rep_zdata_ratio_err = make_repeating_eta_ratio_err(
            state.left_scenario, state.right_scenario
        )
        (
            state.left_zdata,
            state.left_options1,
            state.left_options2,
            state.left_options3,
        ) = make_zerror_plot(state.left_scenario)
        (
            state.right_zdata,
            state.right_options1,
            state.right_options2,
            state.right_options3,
        ) = make_zerror_plot(state.right_scenario)
        (state.comp_zdata, state.comp_options1, state.comp_options2) = make_zcomp(
            state.left_scenario, state.right_scenario
        )
        ratio_plot(state)


def compile_jerror_plots(state):
    if (state.right_scenario is not None) and (state.left_scenario is not None):
        (
            state.left_jzdata,
            state.left_joptions1,
            state.left_joptions2,
            state.left_joptions3,
        ) = make_jerror_plot(state.left_scenario)
        (
            state.right_jzdata,
            state.right_joptions1,
            state.right_joptions2,
            state.right_joptions3,
        ) = make_jerror_plot(state.right_scenario)
        (state.comp_jzdata, state.comp_joptions1, state.comp_joptions2) = make_jcomp(
            state.left_scenario, state.right_scenario
        )
        ratioj_plot(state)


def make_repeating_eta_plot_err(scenario, rightscenario):
    # We will need eta*pt number of scalefactors

    x = scenario.Zisohiy.read()
    pt = scenario.Zisohix.read()
    y1 = scenario.Zcombhi.read()
    y2 = scenario.Zmasshi.read()
    y3 = scenario.Zisohi.read()
    xlo = scenario.Zisoloy.read()
    ptlo = scenario.Zisolox.read()
    y1lo = scenario.Zcomblo.read()
    y2lo = scenario.Zmasslo.read()
    y3lo = scenario.Zisolo.read()
    # We will need to repeat the number of edges we have so it matches the number of
    # pt bins we have
    y1 = np.concatenate((y1lo, y1), axis=0)
    y2 = np.concatenate((y2lo, y2), axis=0)
    y3 = np.concatenate((y3lo, y3), axis=0)
    print("shape: ", y1.shape)
    edges = list(range(221))
    centers = []
    errors_x = []
    for i in range(len(edges) - 1):
        centers.append((edges[i] + edges[i + 1]) / 2)
        errors_x.append(abs(edges[i] - edges[i + 1]) / 2)

    errors_y1 = get_standarddev(np.array(y1)[:, 1:]).tolist()
    errors_y2 = get_standarddev(np.array(y2)[:, 1:]).tolist()
    errors_y3 = get_standarddev(np.array(y3)[:, 1:]).tolist()

    print("repx: ", len(centers), centers)
    print("reperrors_x: ", len(errors_x), errors_x)
    print("reperrors_y1: ", len(errors_y1), errors_y1)
    print("repZcomb: ", len(np.array(y1)[:, 0].tolist()), np.array(y1)[:, 0].tolist())
    print("repZmass: ", np.array(y2)[:, 0].tolist())
    print("repZiso: ", np.array(y3)[:, 0].tolist())

    ry1 = rightscenario.Zcombhi.read()
    ry1lo = rightscenario.Zcomblo.read()
    ry1 = np.concatenate((ry1lo, ry1), axis=0)
    rerrors_y1 = get_standarddev(np.array(ry1)[:, 1:]).tolist()
    data = {
        "x": centers,
        "LeftZcomb": errors_y1,
        "RightZcomb": rerrors_y1,
    }
    options1 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y1},
    }
    options2 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y2},
    }
    options3 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y3},
    }
    return data  # , options1, options2, options3


def make_repeating_eta_ratio_err(scenario, rightscenario):
    # We will need eta*pt number of scalefactors

    x = scenario.Zisohiy.read()
    pt = scenario.Zisohix.read()
    y1 = scenario.Zcombhi.read()
    y2 = scenario.Zmasshi.read()
    y3 = scenario.Zisohi.read()
    xlo = scenario.Zisoloy.read()
    ptlo = scenario.Zisolox.read()
    y1lo = scenario.Zcomblo.read()
    y2lo = scenario.Zmasslo.read()
    y3lo = scenario.Zisolo.read()
    # We will need to repeat the number of edges we have so it matches the number of
    # pt bins we have
    y1 = np.concatenate((y1lo, y1), axis=0)
    y2 = np.concatenate((y2lo, y2), axis=0)
    y3 = np.concatenate((y3lo, y3), axis=0)
    print("shape: ", y1.shape)
    edges = list(range(221))
    centers = []
    errors_x = []
    for i in range(len(edges) - 1):
        centers.append((edges[i] + edges[i + 1]) / 2)
        errors_x.append(abs(edges[i] - edges[i + 1]) / 2)

    errors_y1 = get_standarddev(np.array(y1)[:, 1:]).tolist()
    errors_y2 = get_standarddev(np.array(y2)[:, 1:]).tolist()
    errors_y3 = get_standarddev(np.array(y3)[:, 1:]).tolist()

    print("repx: ", len(centers), centers)
    print("reperrors_x: ", len(errors_x), errors_x)
    print("reperrors_y1: ", len(errors_y1), errors_y1)
    print("repZcomb: ", len(np.array(y1)[:, 0].tolist()), np.array(y1)[:, 0].tolist())
    print("repZmass: ", np.array(y2)[:, 0].tolist())
    print("repZiso: ", np.array(y3)[:, 0].tolist())

    zmass_ratio = []
    ziso_ratio = []
    base = []
    ry1 = rightscenario.Zcombhi.read()
    ry1lo = rightscenario.Zcomblo.read()
    ry1 = np.concatenate((ry1lo, ry1), axis=0)
    rerrors_y1 = get_standarddev(np.array(ry1)[:, 1:]).tolist()
    for i in range(len(np.array(y1)[:, 0].tolist())):
        zmass_ratio.append(rerrors_y1[i] / errors_y1[i])
        ziso_ratio.append(
            np.array(y3)[:, 0].tolist()[i] / np.array(y1)[:, 0].tolist()[i]
        )
        base.append(1)
    data = {"x": centers, "LeftZcomb": base, "RightZcomb": zmass_ratio}
    options1 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        # "error_y": {"type": "data", "array": errors_y1},
    }
    options2 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y2},
    }
    options3 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y3},
    }
    return data  # , options1, options2, options3


def make_repeating_eta_plot(scenario, rightscenario):
    # We will need eta*pt number of scalefactors

    x = scenario.Zisohiy.read()
    pt = scenario.Zisohix.read()
    y1 = scenario.Zcombhi.read()
    y2 = scenario.Zmasshi.read()
    y3 = scenario.Zisohi.read()
    xlo = scenario.Zisoloy.read()
    ptlo = scenario.Zisolox.read()
    y1lo = scenario.Zcomblo.read()
    y2lo = scenario.Zmasslo.read()
    y3lo = scenario.Zisolo.read()
    # We will need to repeat the number of edges we have so it matches the number of
    # pt bins we have
    y1 = np.concatenate((y1lo, y1), axis=0)
    y2 = np.concatenate((y2lo, y2), axis=0)
    y3 = np.concatenate((y3lo, y3), axis=0)
    print("shape: ", y1.shape)
    edges = list(range(221))
    centers = []
    errors_x = []
    for i in range(len(edges) - 1):
        centers.append((edges[i] + edges[i + 1]) / 2)
        errors_x.append(abs(edges[i] - edges[i + 1]) / 2)

    errors_y1 = get_standarddev(np.array(y1)[:, 1:]).tolist()
    errors_y2 = get_standarddev(np.array(y2)[:, 1:]).tolist()
    errors_y3 = get_standarddev(np.array(y3)[:, 1:]).tolist()

    print("repx: ", len(centers), centers)
    print("reperrors_x: ", len(errors_x), errors_x)
    print("reperrors_y1: ", len(errors_y1), errors_y1)
    print("repZcomb: ", len(np.array(y1)[:, 0].tolist()), np.array(y1)[:, 0].tolist())
    print("repZmass: ", np.array(y2)[:, 0].tolist())
    print("repZiso: ", np.array(y3)[:, 0].tolist())

    ry1 = rightscenario.Zcombhi.read()
    ry1lo = rightscenario.Zcomblo.read()
    ry1 = np.concatenate((ry1lo, ry1), axis=0)
    data = {
        "x": centers,
        "LeftZcomb": np.array(y1)[:, 0].tolist(),
        "RightZcomb": np.array(ry1)[:, 0].tolist(),
    }
    options1 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y1},
    }
    options2 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y2},
    }
    options3 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y3},
    }
    return data  # , options1, options2, options3


def make_repeating_eta_ratio(scenario, rightscenario):
    # We will need eta*pt number of scalefactors

    x = scenario.Zisohiy.read()
    pt = scenario.Zisohix.read()
    y1 = scenario.Zcombhi.read()
    y2 = scenario.Zmasshi.read()
    y3 = scenario.Zisohi.read()
    xlo = scenario.Zisoloy.read()
    ptlo = scenario.Zisolox.read()
    y1lo = scenario.Zcomblo.read()
    y2lo = scenario.Zmasslo.read()
    y3lo = scenario.Zisolo.read()
    # We will need to repeat the number of edges we have so it matches the number of
    # pt bins we have
    y1 = np.concatenate((y1lo, y1), axis=0)
    y2 = np.concatenate((y2lo, y2), axis=0)
    y3 = np.concatenate((y3lo, y3), axis=0)
    print("shape: ", y1.shape)
    edges = list(range(221))
    centers = []
    errors_x = []
    for i in range(len(edges) - 1):
        centers.append((edges[i] + edges[i + 1]) / 2)
        errors_x.append(abs(edges[i] - edges[i + 1]) / 2)

    errors_y1 = get_standarddev(np.array(y1)[:, 1:]).tolist()
    errors_y2 = get_standarddev(np.array(y2)[:, 1:]).tolist()
    errors_y3 = get_standarddev(np.array(y3)[:, 1:]).tolist()

    print("repx: ", len(centers), centers)
    print("reperrors_x: ", len(errors_x), errors_x)
    print("reperrors_y1: ", len(errors_y1), errors_y1)
    print("repZcomb: ", len(np.array(y1)[:, 0].tolist()), np.array(y1)[:, 0].tolist())
    print("repZmass: ", np.array(y2)[:, 0].tolist())
    print("repZiso: ", np.array(y3)[:, 0].tolist())

    zmass_ratio = []
    ziso_ratio = []
    base = []
    ry1 = rightscenario.Zcombhi.read()
    ry1lo = rightscenario.Zcomblo.read()
    ry1 = np.concatenate((ry1lo, ry1), axis=0)
    for i in range(len(np.array(y1)[:, 0].tolist())):
        zmass_ratio.append(
            np.array(ry1)[:, 0].tolist()[i] / np.array(y1)[:, 0].tolist()[i]
        )
        ziso_ratio.append(
            np.array(y3)[:, 0].tolist()[i] / np.array(y1)[:, 0].tolist()[i]
        )
        base.append(1)
    data = {"x": centers, "LeftZcomb": base, "RightZcomb": zmass_ratio}
    options1 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        # "error_y": {"type": "data", "array": errors_y1},
    }
    options2 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y2},
    }
    options3 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y3},
    }
    return data  # , options1, options2, options3


def make_repeating_eta_plo_all(scenario):
    # We will need eta*pt number of scalefactors

    x = scenario.Zisohiy.read()
    pt = scenario.Zisohix.read()
    y1 = scenario.Zcombhi.read()
    y2 = scenario.Zmasshi.read()
    y3 = scenario.Zisohi.read()
    xlo = scenario.Zisoloy.read()
    ptlo = scenario.Zisolox.read()
    y1lo = scenario.Zcomblo.read()
    y2lo = scenario.Zmasslo.read()
    y3lo = scenario.Zisolo.read()
    # We will need to repeat the number of edges we have so it matches the number of
    # pt bins we have
    y1 = np.concatenate((y1lo, y1), axis=0)
    y2 = np.concatenate((y2lo, y2), axis=0)
    y3 = np.concatenate((y3lo, y3), axis=0)
    print("shape: ", y1.shape)
    edges = list(range(220))
    centers = []
    errors_x = []
    for i in range(len(edges) - 1):
        centers.append((edges[i] + edges[i + 1]) / 2)
        errors_x.append(abs(edges[i] - edges[i + 1]) / 2)

    errors_y1 = get_standarddev(np.array(y1)[:, 1:]).tolist()
    errors_y2 = get_standarddev(np.array(y2)[:, 1:]).tolist()
    errors_y3 = get_standarddev(np.array(y3)[:, 1:]).tolist()

    print("repx: ", len(centers), centers)
    print("reperrors_x: ", len(errors_x), errors_x)
    print("reperrors_y1: ", len(errors_y1), errors_y1)
    print("repZcomb: ", len(np.array(y1)[:, 0].tolist()), np.array(y1)[:, 0].tolist())
    print("repZmass: ", np.array(y2)[:, 0].tolist())
    print("repZiso: ", np.array(y3)[:, 0].tolist())

    data = {
        "x": edges,
        "xjpsi": list(range(0)),
        "Zcomb": np.array(y1)[:, 0].tolist(),
        "Zmass": np.array(y2)[:, 0].tolist(),
        "Ziso": np.array(y3)[:, 0].tolist(),
    }
    options1 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y1},
    }
    options2 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y2},
    }
    options3 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y3},
    }
    return data, options1, options2, options3


def make_repeating_eta_ratio_all(scenario):
    # We will need eta*pt number of scalefactors

    x = scenario.Zisohiy.read()
    pt = scenario.Zisohix.read()
    y1 = scenario.Zcombhi.read()
    y2 = scenario.Zmasshi.read()
    y3 = scenario.Zisohi.read()
    xlo = scenario.Zisoloy.read()
    ptlo = scenario.Zisolox.read()
    y1lo = scenario.Zcomblo.read()
    y2lo = scenario.Zmasslo.read()
    y3lo = scenario.Zisolo.read()
    # We will need to repeat the number of edges we have so it matches the number of
    # pt bins we have
    y1 = np.concatenate((y1lo, y1), axis=0)
    y2 = np.concatenate((y2lo, y2), axis=0)
    y3 = np.concatenate((y3lo, y3), axis=0)
    print("shape: ", y1.shape)
    edges = list(range(221))
    centers = []
    errors_x = []
    for i in range(len(edges) - 1):
        centers.append((edges[i] + edges[i + 1]) / 2)
        errors_x.append(abs(edges[i] - edges[i + 1]) / 2)

    errors_y1 = get_standarddev(np.array(y1)[:, 1:]).tolist()
    errors_y2 = get_standarddev(np.array(y2)[:, 1:]).tolist()
    errors_y3 = get_standarddev(np.array(y3)[:, 1:]).tolist()

    print("repx: ", len(centers), centers)
    print("reperrors_x: ", len(errors_x), errors_x)
    print("reperrors_y1: ", len(errors_y1), errors_y1)
    print("repZcomb: ", len(np.array(y1)[:, 0].tolist()), np.array(y1)[:, 0].tolist())
    print("repZmass: ", np.array(y2)[:, 0].tolist())
    print("repZiso: ", np.array(y3)[:, 0].tolist())

    zmass_ratio = []
    ziso_ratio = []
    base = []
    for i in range(len(np.array(y1)[:, 0].tolist())):
        zmass_ratio.append(
            np.array(y2)[:, 0].tolist()[i] / np.array(y1)[:, 0].tolist()[i]
        )
        ziso_ratio.append(
            np.array(y3)[:, 0].tolist()[i] / np.array(y1)[:, 0].tolist()[i]
        )
        base.append(1)
    data = {
        "x": centers,
        "Zcomb": base,
        "Zmass": zmass_ratio,
        "Ziso": ziso_ratio,
    }
    options1 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        # "error_y": {"type": "data", "array": errors_y1},
    }
    options2 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y2},
    }
    options3 = {
        # Create the error bar information:
        # "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y3},
    }
    return data, options1, options2, options3


def make_zerror_plot(scenario):
    if "ZcombyPrebin" in scenario.data_nodes:
        x = scenario.ZcombyPrebin.read()
        y1 = scenario.ZcombRebin.read()
        y2 = scenario.ZmassSystDecorrPrebin.read()
        y3 = scenario.ZisoSystDecorrPrebin.read()
    else:
        x = scenario.ZisoRebiny.read()
        y1 = scenario.ZcombRebin.read()
        y2 = scenario.ZmassRebin.read()
        y3 = scenario.ZisoRebin.read()
    edges = np.array(x).ravel().tolist()
    edges.append(2.47)
    centers = []
    errors_x = []
    for i in range(len(edges) - 1):
        centers.append((edges[i] + edges[i + 1]) / 2)
        errors_x.append(abs(edges[i] - edges[i + 1]) / 2)

    errors_y1 = get_standarddev(np.array(y1)[-len(centers) :, 1:]).tolist()
    errors_y2 = get_standarddev(np.array(y2)[-len(centers) :, 1:]).tolist()
    errors_y3 = get_standarddev(np.array(y3)[-len(centers) :, 1:]).tolist()

    # print("x: ", centers)
    # print("errors_x: ", errors_x)
    # print("errors_y1: ", errors_y1)
    # print("Zcomb: ", np.array(y1)[-len(centers) :, 0].tolist())
    # print("Zmass: ", np.array(y2)[-len(centers) :, 0].tolist())
    # print("Ziso: ", np.array(y3)[-len(centers) :, 0].tolist())
    data = {
        "x": centers,
        "Zcomb": np.array(y1)[-len(centers) :, 0].tolist(),
        "Zmass": np.array(y2)[-len(centers) :, 0].tolist(),
        "Ziso": np.array(y3)[-len(centers) :, 0].tolist(),
    }
    options1 = {
        # Create the error bar information:
        "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y1},
    }
    options2 = {
        # Create the error bar information:
        "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y2},
    }
    options3 = {
        # Create the error bar information:
        "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y3},
    }
    return data, options1, options2, options3


def make_zcomp(left_sc, right_sc):
    if "ZcombyPrebin" in left_sc.data_nodes:
        x_left = left_sc.ZcombyPrebin.read()
    else:
        x_left = left_sc.ZisoRebiny.read()

    if "ZcombyPrebin" in right_sc.data_nodes:
        x_right = right_sc.ZcombyPrebin.read()
    else:
        x_right = right_sc.ZisoRebiny.read()

    y1 = left_sc.ZcombRebin.read()
    y2 = right_sc.ZcombRebin.read()

    edges_left = np.array(x_left).ravel().tolist()
    edges_left.append(2.47)
    centers_left = []
    errors_x_left = []
    for i in range(len(edges_left) - 1):
        centers_left.append((edges_left[i] + edges_left[i + 1]) / 2)
        errors_x_left.append(abs(edges_left[i] - edges_left[i + 1]) / 2)

    errors_y1 = get_standarddev(np.array(y1)[-len(centers_left) :, 1:]).tolist()

    edges_right = np.array(x_right).ravel().tolist()
    edges_right.append(2.47)
    centers_right = []
    errors_x_right = []
    for i in range(len(edges_right) - 1):
        centers_right.append((edges_right[i] + edges_right[i + 1]) / 2)
        errors_x_right.append(abs(edges_right[i] - edges_right[i + 1]) / 2)

    errors_y2 = get_standarddev(np.array(y2)[-len(centers_right) :, 1:]).tolist()

    print("x: ", centers_left)
    print("errors_x_left: ", errors_x_left)
    print("errors_y1: ", errors_y1)
    print("Zcomb: ", np.array(y1)[-len(centers_left) :, 0].tolist())
    print("Zmass: ", np.array(y2)[-len(centers_left) :, 0].tolist())

    y_left = np.array(y1)[-len(centers_left) :, 0].tolist()
    if len(centers_left) < len(centers_right):
        centers_left = centers_right
        y_left.append(None)

    y_right = np.array(y2)[-len(centers_right) :, 0].tolist()
    if len(centers_right) < len(centers_left):
        centers_right = centers_left
        y_right.append(None)
    data = {
        "x_left": centers_left,
        "x_right": centers_right,
        "Zcomb_left": y_left,
        "Zcomb_right": y_right,
    }

    options1 = {
        # Create the error bar information:
        "error_x": {"type": "data", "array": errors_x_left},
        "error_y": {"type": "data", "array": errors_y1},
    }
    options2 = {
        # Create the error bar information:
        "error_x": {"type": "data", "array": errors_x_right},
        "error_y": {"type": "data", "array": errors_y2},
    }

    return data, options1, options2


def make_jerror_plot(scenario):
    if "ZcombyPrebin" in scenario.data_nodes:
        x = scenario.ZcombyPrebin.read()
        edges = np.array(x).ravel().tolist()
        # it has many energy zones but we only want the last
        # now it will have the same shape as the others
        y1 = scenario.ZJpsicomb.read()[-len(edges) :, :]
        y2 = scenario.Jpsi.read()[-len(edges) :, :]
        y3 = scenario.ZcombSystDecorr.read()[-len(edges) :, :]
    else:
        x = scenario.ZisoRebiny.read()
        edges = np.array(x).ravel().tolist()
        # it has many energy zones but we only want the last
        # now it will have the same shape as the others
        y1 = scenario.ZJpsicomb.read()[-len(edges) :, :]
        y2 = scenario.Jpsi.read()[-len(edges) :, :]
        y3 = scenario.ZcombRebin.read()[-len(edges) :, :]

    edges.append(2.47)
    centers = []
    errors_x = []
    for i in range(len(edges) - 1):
        centers.append((edges[i] + edges[i + 1]) / 2)
        errors_x.append(abs(edges[i] - edges[i + 1]) / 2)

    errors_y1 = get_standarddev(np.array(y1)[:, 1:]).tolist()
    errors_y2 = get_standarddev(np.array(y2)[:, 1:]).tolist()
    errors_y3 = get_standarddev(np.array(y3)[:, 1:]).tolist()
    # print("centers: ", centers)
    # print("errors_x: ", errors_x)
    # print("errors_y1: ", errors_y1)
    # print("y1: ", y1)
    # print("y2: ", y2)
    # print("y3: ", y3)
    data = {
        "x": centers,
        "JZcomb": np.array(y1)[:, 0].tolist(),
        "Jpsi": np.array(y2)[:, 0].tolist(),
        "Zcomb": np.array(y3)[:, 0].tolist(),
    }
    options1 = {
        # Create the error bar information:
        "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y1},
    }
    options2 = {
        # Create the error bar information:
        "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y2},
    }
    options3 = {
        # Create the error bar information:
        "error_x": {"type": "data", "array": errors_x},
        "error_y": {"type": "data", "array": errors_y3},
    }
    return data, options1, options2, options3


def make_jcomp(left_sc, right_sc):
    if "ZcombyPrebin" in left_sc.data_nodes:
        x_left = left_sc.ZcombyPrebin.read()
    else:
        x_left = left_sc.ZisoRebiny.read()

    if "ZcombyPrebin" in right_sc.data_nodes:
        x_right = right_sc.ZcombyPrebin.read()
    else:
        x_right = right_sc.ZisoRebiny.read()

    edges_left = np.array(x_left).ravel().tolist()
    edges_right = np.array(x_right).ravel().tolist()

    y1 = left_sc.ZJpsicomb.read()[-len(edges_left) :, :]
    y2 = right_sc.ZJpsicomb.read()[-len(edges_right) :, :]

    edges_left.append(2.47)
    centers_left = []
    errors_x_left = []
    for i in range(len(edges_left) - 1):
        centers_left.append((edges_left[i] + edges_left[i + 1]) / 2)
        errors_x_left.append(abs(edges_left[i] - edges_left[i + 1]) / 2)

    errors_y1 = get_standarddev(np.array(y1)[-len(centers_left) :, 1:]).tolist()

    edges_right.append(2.47)
    centers_right = []
    errors_x_right = []
    for i in range(len(edges_right) - 1):
        centers_right.append((edges_right[i] + edges_right[i + 1]) / 2)
        errors_x_right.append(abs(edges_right[i] - edges_right[i + 1]) / 2)

    errors_y2 = get_standarddev(np.array(y2)[-len(centers_right) :, 1:]).tolist()

    y_left = np.array(y1)[-len(centers_left) :, 0].tolist()
    if len(centers_left) < len(centers_right):
        centers_left = centers_right
        y_left.append(None)

    y_right = np.array(y2)[-len(centers_right) :, 0].tolist()
    if len(centers_right) < len(centers_left):
        centers_right = centers_left
        y_right.append(None)
    data = {
        "x_left": centers_left,
        "x_right": centers_right,
        "JZcomb_left": y_left,
        "JZcomb_right": y_right,
    }
    options1 = {
        # Create the error bar information:
        "error_x": {"type": "data", "array": errors_x_left},
        "error_y": {"type": "data", "array": errors_y1},
    }
    options2 = {
        # Create the error bar information:
        "error_x": {"type": "data", "array": errors_x_right},
        "error_y": {"type": "data", "array": errors_y2},
    }

    return data, options1, options2


def ratio_plot(state):
    if "ZcombyPrebin" in state.left_scenario.data_nodes:
        x = state.left_scenario.ZcombyPrebin.read()
    else:
        x = state.left_scenario.ZisoRebiny.read()
    edges = np.array(x).ravel().tolist()
    y1 = state.left_scenario.ZcombRebin.read()[-len(edges) :, 0].tolist()
    y2 = state.right_scenario.ZcombRebin.read()[-len(edges) :, 0].tolist()
    print(x)
    edges.append(2.47)
    centers = []
    errors_x = []
    for i in range(len(edges) - 1):
        centers.append((edges[i] + edges[i + 1]) / 2)
        errors_x.append(abs(edges[i] - edges[i + 1]) / 2)
    ratio = [y1[i] / y2[i] for i in range(len(y1))]
    state.ratio = {"x": centers, "ratio": ratio}
    state.ratio_options = {
        # Create the error bar information:
        "error_x": {"type": "data", "array": errors_x},
        # "error_y": {"type": "data", "array": errors_y},
    }


def ratioj_plot(state):
    if "ZcombyPrebin" in state.left_scenario.data_nodes:
        x = state.left_scenario.ZcombyPrebin.read()
    else:
        x = state.left_scenario.ZisoRebiny.read()
    edges = np.array(x).ravel().tolist()
    y1 = state.left_scenario.ZJpsicomb.read()[-len(edges) :, 0].tolist()
    y2 = state.right_scenario.ZJpsicomb.read()[-len(edges) :, 0].tolist()
    print(x)
    edges.append(2.47)
    centers = []
    errors_x = []
    for i in range(len(edges) - 1):
        centers.append((edges[i] + edges[i + 1]) / 2)
        errors_x.append(abs(edges[i] - edges[i + 1]) / 2)
    ratio = [y1[i] / y2[i] for i in range(len(y1))]
    state.ratioj = {"x": centers, "ratio": ratio}
    state.ratio_joptions = {
        # Create the error bar information:
        "error_x": {"type": "data", "array": errors_x},
        # "error_y": {"type": "data", "array": errors_y},
    }


layout = {
    "yaxis": {
        # Second axis overlays with the first y axis
        "range": [0.6, 1.2],
    }
}
line = {"shape": "hv"}
compareScenarios_md = Markdown("compareScenarios.md")
