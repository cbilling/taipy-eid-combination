from taipy.gui import Markdown, notify
import datetime as dt
import numpy as np
from plotting_funcs.repeating_eta import (
    make_repeating_eta_plot,
    make_repeating_ratio_plot,
    make_repeating_eta_err,
    make_repeating_eta_err_ratio,
)
from plotting_funcs.heatmap import (
    heatmap_data_jpsi,
    heatmap_data_ziso,
    heatmap_data_zmass,
)

level_to_view = "Scale Factors"
year = None
workingPoint = None
extraBin = None
rebinMethod = "Hera"
selected_data_node = None
selected_scenario = None
selected_date = None
level = 0
options = {
    "colorscale": [
        ["0.0", "rgb(159,2,67)"],
        ["0.2", "rgb(242,105,68)"],
        ["0.4", "rgb(251,216,133)"],
        ["0.6", "rgb(235,247,160)"],
        ["0.8", "rgb(121,201,165)"],
        ["1.0", "rgb(91,77,158)"],
    ]
}
marker = {"size": 5}
repeating_data = {
    "pT": [0, 1],
    "TotalComb": [1, 1],
    "GlobalComb": [1, 1],
    "BinByBinComb": [1, 1],
    "Ziso": [1, 1],
    "Zmass": [1, 1],
    "Jpsi": [1, 1],
}
repeating_ratio = {
    "pT": [0, 1],
    "TotalComb": [1, 1],
    "GlobalComb": [1, 1],
    "BinByBinComb": [1, 1],
    "Ziso": [1, 1],
    "Zmass": [1, 1],
    "Jpsi": [1, 1],
}
repeating_error = {
    "pT": [0, 1],
    "TotalCombError": [1, 1],
    "GlobalCombError": [1, 1],
    "BinByBinCombError": [1, 1],
}
repeating_err_ratio = {
    "pT": [0, 1],
    "TotalCombError": [1, 1],
    "GlobalCombError": [1, 1],
    "BinByBinCombError": [1, 1],
}
options1 = {
    # Create the error bar information:
    # "error_x": {"type": "data", "array": errors_x},
    "error_y": {"type": "data", "array": [1, 1]},
}
options2 = {
    # Create the error bar information:
    # "error_x": {"type": "data", "array": errors_x},
    "error_y": {"type": "data", "array": [1, 1]},
}
options3 = {
    # Create the error bar information:
    # "error_x": {"type": "data", "array": errors_x},
    "error_y": {"type": "data", "array": [1, 1]},
}
layout_ziso = {"annotations": []}
layout_zmass = {"annotations": []}
layout_jpsi = {"annotations": []}
edges = [
    -2.47,
    -2.37,
    -2.01,
    -1.81,
    -1.52,
    -1.37,
    -1.15,
    -0.8,
    -0.6,
    -0.1,
    0.0,
    0.1,
    0.6,
    0.8,
    1.15,
    1.37,
    1.52,
    1.81,
    2.01,
    2.37,
    2.47,
]
centersy = []
for i in range(len(edges) - 1):
    centersy.append((edges[i] + edges[i + 1]) / 2)
edgesx = [
    15000.0,
    20000.0,
    25000.0,
    30000.0,
    35000.0,
    40000.0,
    45000.0,
    50000.0,
    60000.0,
    80000.0,
    150000.0,
    200000.0,
]
centersx = []
for i in range(len(edgesx) - 1):
    centersx.append((edgesx[i] + edgesx[i + 1]) / 2)

default_zheatmap = [
    {
        "SFStatSyst": np.transpose(np.zeros((11, 20))).tolist(),
        "eta": centersy,
    },
    {"pT": centersx},
]


jedges = [0.0, 0.1, 0.8, 1.37, 1.52, 2.01, 2.37, 2.47]
jcentersy = []
for i in range(len(jedges) - 1):
    jcentersy.append((jedges[i] + jedges[i + 1]) / 2)
jedgesx = [
    4500,
    7000,
    10000,
    15000,
    20000,
]
jcentersx = []
for i in range(len(jedgesx) - 1):
    jcentersx.append((jedgesx[i] + jedgesx[i + 1]) / 2)

default_jheatmap = [
    {
        "SFStatSyst": np.transpose(np.zeros((len(jcentersx), len(jcentersy)))).tolist(),
        "eta": jcentersy,
    },
    {"pT": jcentersx},
]
ziso_SF = zmass_SF = default_zheatmap
jpsi_SF = default_jheatmap


def on_submission_change(state, submitable, details):
    if details["submission_status"] == "COMPLETED":
        state.refresh("selected_scenario")
        notify(state, "success", "Combinations ready!")
        print("Combinations ready!")
    elif details["submission_status"] == "FAILED":
        notify(state, "error", "Submission failed!")
        print("Submission failed!")
    else:
        notify(state, "info", "In progress...")
        print("In progress...")


def save(state):
    state.selected_scenario.WorkingPointID.write(state.workingPoint)
    state.selected_scenario.Year.write(state.year)
    state.selected_scenario.RebinMethod.write(state.rebinMethod)
    print("RebinMethod in state.selected_scenario.data_nodes")
    print("RebinMethod" in state.selected_scenario.data_nodes)
    print("ExtraBin" in state.selected_scenario.data_nodes)
    # print(state.selected_scenario.data_nodes)
    if "ExtraBin" in state.selected_scenario.data_nodes:
        state.selected_scenario.ExtraBin.write(state.extraBin)

    notify(state, "s", str(state.selected_scenario.get_label()))


def update_charts(state):
    notify(state, "info", "Updating charts")
    state.repeating_data, state.options1, state.options2, state.options3 = (
        make_repeating_eta_plot(state.selected_scenario)
    )
    state.repeating_ratio, _, _, _ = make_repeating_ratio_plot(state.selected_scenario)
    state.repeating_error = make_repeating_eta_err(state.selected_scenario)
    state.repeating_err_ratio = make_repeating_eta_err_ratio(state.selected_scenario)


def update_heatmap(state):

    notify(state, "info", "Updating heatmaps")
    state.ziso_SF, state.layout_ziso["annotations"] = heatmap_data_ziso(
        state.selected_scenario, state.level
    )

    state.zmass_SF, state.layout_zmass["annotations"] = heatmap_data_zmass(
        state.selected_scenario, state.level
    )
    state.jpsi_SF, state.layout_jpsi["annotations"] = heatmap_data_jpsi(
        state.selected_scenario, state.level
    )
    if level == 0:
        state.level_to_view = "Scale Factors"
    elif level == 1:
        state.level_to_view = "Statistical Error x 10"
    elif level == 2:
        state.level_to_view = "Systematic Error x 10"
    elif level == 3:
        state.level_to_view = "Total Error x 10"


scenarioMaker_md = Markdown("scenarioMaker.md")
