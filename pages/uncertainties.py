from taipy.gui import Markdown, notify
import datetime as dt
import numpy as np
from plotting_funcs.heatmap import (
    heatmap_data_jpsi,
    heatmap_data_syst,
    heatmap_data_stat,
    heatmap_data_zmass,
)

syst_level = 0
options = {
    "colorscale": [
        ["0.0", "rgb(159,2,67)"],
        ["0.2", "rgb(242,105,68)"],
        ["0.4", "rgb(251,216,133)"],
        ["0.6", "rgb(235,247,160)"],
        ["0.8", "rgb(121,201,165)"],
        ["1.0", "rgb(91,77,158)"],
    ]
}

pad = {"margin": dict(l=1, r=1, t=1, b=1)}
edges = [
    -2.47,
    -2.37,
    -2.01,
    -1.81,
    -1.52,
    -1.37,
    -1.15,
    -0.8,
    -0.6,
    -0.1,
    0.0,
    0.1,
    0.6,
    0.8,
    1.15,
    1.37,
    1.52,
    1.81,
    2.01,
    2.37,
    2.47,
]
centersy = []
for i in range(len(edges) - 1):
    centersy.append((edges[i] + edges[i + 1]) / 2)
edgesx = [
    15000.0,
    20000.0,
    25000.0,
    30000.0,
    35000.0,
    40000.0,
    45000.0,
    50000.0,
    60000.0,
    80000.0,
    150000.0,
    200000.0,
]
centersx = []
for i in range(len(edgesx) - 1):
    centersx.append((edgesx[i] + edgesx[i + 1]) / 2)

default_zheatmap = [
    {
        "SFStatSyst": np.transpose(np.zeros((11, 20))).tolist(),
        "eta": centersy,
    },
    {"pT": centersx},
]

zisoSysBef = default_zheatmap
zisoSysAft = default_zheatmap
zmassSysBef = default_zheatmap
zmassSysAft = default_zheatmap
zisoStatBef = default_zheatmap
zisoStatAft = default_zheatmap
zmassStatBef = default_zheatmap
zmassStatAft = default_zheatmap

margin = dict(l=8, t=3, b=5)
layout_zisoSysBef = {"annotations": [], "margin": margin}
layout_zisoSysAft = {"annotations": [], "margin": margin}
layout_zmassSysBef = {"annotations": [], "margin": margin}
layout_zmassSysAft = {"annotations": [], "margin": margin}
layout_zisoStatBef = {"annotations": [], "margin": margin}
layout_zisoStatAft = {"annotations": [], "margin": margin}
layout_zmassStatBef = {"annotations": [], "margin": margin}
layout_zmassStatAft = {"annotations": [], "margin": margin}

zisoSysBefarr = None
zisoSysAftarr = None
zmassSysBefarr = None
zmassSysAftarr = None
zisoStatBefarr = None
zisoStatAftarr = None
zmassStatBefarr = None
zmassStatAftarr = None

scenario = None


def set_scenario(state):
    state.zisoSysBefarr = state.scenario.ZisoDecorr.read()
    state.zisoSysAftarr = state.scenario.ZisoSystDecorr.read()
    state.zmassSysBefarr = state.scenario.ZmassDecorr.read()
    state.zmassSysAftarr = state.scenario.ZmassSystDecorr.read()
    state.zisoStatBefarr = state.scenario.ZisoDecorr.read()[:, 1]
    state.zisoStatAftarr = state.scenario.ZisoDecorr.read()[:, 2:222]
    state.zmassStatBefarr = state.scenario.ZmassDecorr.read()[:, 1]
    state.zmassStatAftarr = state.scenario.ZmassDecorr.read()[:, 2:222]

    state.zisoStatBef, state.layout_zisoStatBef["annotations"] = heatmap_data_stat(
        state.zisoStatBefarr
    )
    state.zisoStatAft, state.layout_zisoStatAft["annotations"] = heatmap_data_stat(
        state.zisoStatAftarr, doSum=True
    )
    state.zmassStatBef, state.layout_zmassStatBef["annotations"] = heatmap_data_stat(
        state.zmassStatBefarr
    )
    state.zmassStatAft, state.layout_zmassStatAft["annotations"] = heatmap_data_stat(
        state.zmassStatAftarr, doSum=True
    )


def update_heatmap(state):

    notify(state, "info", "Updating heatmaps")
    state.zisoSysBef, state.layout_zisoSysBef["annotations"] = heatmap_data_syst(
        state.zisoSysBefarr, state.syst_level
    )
    state.zisoSysAft, state.layout_zisoSysAft["annotations"] = heatmap_data_syst(
        state.zisoSysAftarr, state.syst_level
    )
    state.zmassSysBef, state.layout_zmassSysBef["annotations"] = heatmap_data_syst(
        state.zmassSysBefarr, state.syst_level
    )
    state.zmassSysAft, state.layout_zmassSysAft["annotations"] = heatmap_data_syst(
        state.zmassSysAftarr, state.syst_level
    )


uncertanties_md = Markdown("uncertainties.md")
