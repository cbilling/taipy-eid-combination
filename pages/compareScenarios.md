## Here you will be able to compare scenarios 
#### If you don't see anything yet, consider making a scenario on the scenario maker tab
<|layout|columns=2 2|gap=50px|

<left|
<|Compare|button|on_action=compile_zerror_plots|>
<|{left_scenario}|scenario_selector|show_add_button=False|>
# **Z**{: .color-primary} combination
#### Left scenario
<|{left_zdata}|chart|type=scatter|mode=markers|x=x|y[1]=Zcomb|y[2]=Zmass|y[3]=Ziso|options[1]={left_options1}|options[2]={left_options2}|options[3]={left_options3}|layout={layout}|rebuild|>
#### Right scenario
<|{right_zdata}|chart|type=scatter|mode=markers|x=x|y[1]=Zcomb|y[2]=Zmass|y[3]=Ziso|options[1]={right_options1}|options[2]={right_options2}|options[3]={right_options3}|layout={layout}|rebuild|>
#### Comparison
<|{comp_zdata}|chart|type=scatter|mode=markers|x[1]=x_left|x[2]=x_right|y[1]=Zcomb_left|y[2]=Zcomb_right|options[1]={comp_options1}|options[2]={comp_options2}|layout={layout}|rebuild|>
<|{ratio}|chart|type=scatter|mode=markers|x=x|y[1]=ratio|options[1]={ratio_options}|rebuild|>
|left>

<right|
<|Compare|button|on_action=compile_jerror_plots|>
<|{right_scenario}|scenario_selector|show_add_button=False|>
# **Jpsi Z**{: .color-primary} combination
#### Left scenario
<|{left_jzdata}|chart|type=scatter|mode=markers|x=x|y[1]=JZcomb|y[2]=Jpsi|y[3]=Zcomb|options[1]={left_joptions1}|options[2]={left_joptions2}|options[3]={left_joptions3}|layout={layout}|rebuild|>
#### Right scenario
<|{right_jzdata}|chart|type=scatter|mode=markers|x=x|y[1]=JZcomb|y[2]=Jpsi|y[3]=Zcomb|options[1]={right_joptions1}|options[2]={right_joptions2}|options[3]={right_joptions3}|layout={layout}|rebuild|>
#### Comparison
<|{comp_jzdata}|chart|type=scatter|mode=markers|x[1]=x_left|x[2]=x_right|y[1]=JZcomb_left|y[2]=JZcomb_right|options[1]={comp_joptions1}|options[2]={comp_joptions2}|layout={layout}|rebuild|>
<|{ratioj}|chart|type=scatter|mode=markers|x=x|y[1]=ratio|options[1]={ratio_joptions}|rebuild|>
|right>
|>

## Scalefactors repeating 
<|{rep_zdata}|chart|type=lines|mode[1]=lines|mode[2]=lines|line[1]={line}|line[2]={line}|x=x|y[1]=LeftZcomb|y[2]=RightZcomb|layout={l1}|rebuild|>
<|{rep_zdata_ratio}|chart|type=lines|mode[1]=lines|mode[2]=lines|line[1]={line}|line[2]={line}|x=x|y[1]=LeftZcomb|y[2]=RightZcomb|layout={l2}|rebuild|>


## Errors
<|{rep_zdata_err}|chart|type=lines|mode[1]=lines|mode[2]=lines|line[1]={line}|line[2]={line}|x=x|y[1]=LeftZcomb|y[2]=RightZcomb|layout={l1}|rebuild|>
<|{rep_zdata_ratio_err}|chart|type=lines|mode[1]=lines|mode[2]=lines|line[1]={line}|line[2]={line}|x=x|y[1]=LeftZcomb|y[2]=RightZcomb|layout={l2}|rebuild|>
