<|layout|columns=2 9|gap=50px|
<sidebar|sidebar|
**Scenario** Creation

<|{selected_scenario}|scenario_selector|>
|sidebar>

<scenario|part|render={selected_scenario}|
# **Combination**{: .color-primary} page

<|1 1 1 1|layout|
<year|
#### **Year**{: .color-primary} of combination

<|{year}|selector|lov=2015;2016;2017;2018|on_change=save|>
|year>

<working_point|
#### **Working Point**{: .color-primary} of combination

<|{workingPoint}|selector|lov=TightLLH_d0z0;MediumLLH_d0z0;LooseLLH_d0z0|on_change=save|>
|working_point>

<extra_bin|
#### Add an extra bin at high eta

<|{extraBin}|selector|lov=extra;no_extra|on_change=save|>
|extra_bin>

<rebinning|
#### How do you want to rebin Z

<|{rebinMethod}|selector|lov=hera;pre_rebin;by_hand|on_change=save|>
|rebinning>
|>

#### Start

<|{selected_scenario}|scenario|on_submission_change=on_submission_change|not expanded|don't show_tags|don't show_properties|don't show_sequences|don't show_submit|>

<|Refresh Charts|button|on_action=update_charts|>
## Scalefactors repeating 
<|{repeating_data}|chart|type=scatter|mode[4]=lines|mode[5]=lines|mode[6]=lines|mode[1]=markers|mode[2]=markers|mode[3]=markers|line[4]={line}|line[5]={line}|line[6]={line}|x=pT|y[4]=TotalComb|y[5]=GlobalComb|y[6]=BinByBinComb|y[1]=Jpsi|y[2]=Ziso|y[3]=Zmass|options[1]={options1}|options[2]={options2}|options[3]={options3}|color[1]=red|color[2]=orange|color[3]=blue|color[4]=black|marker[1]={marker}|marker[2]={marker}|marker[3]={marker}|rebuild|>
#### Difference with respect to TotalComb
<|{repeating_ratio}|chart|type=scatter|mode[4]=lines|mode[5]=lines|mode[6]=lines|mode[1]=markers|mode[2]=markers|mode[3]=markers|line[4]={line}|line[5]={line}|line[6]={line}|x=pT|y[4]=TotalComb|y[5]=GlobalComb|y[6]=BinByBinComb|y[1]=Jpsi|y[2]=Ziso|y[3]=Zmass|options[1]={options1}|options[2]={options2}|options[3]={options3}|color[1]=red|color[2]=orange|color[3]=blue|color[4]=black|marker[1]={marker}|marker[2]={marker}|marker[3]={marker}|>

## Combination Errors repeating 
<|{repeating_error}|chart|type=lines|mode[1]=lines|mode[2]=lines|mode[3]=lines|line[1]={line}|line[2]={line}|line[3]={line}|x=pT|y[1]=TotalCombError|y[2]=GlobalCombError|y[3]=BinByBinCombError|rebuild|>
#### Difference with respect to TotalComb
<|{repeating_err_ratio}|chart|type=lines|mode[1]=lines|mode[2]=lines|mode[3]=lines|line[1]={line}|line[2]={line}|line[3]={line}|x=pT|y[1]=TotalCombError|y[2]=GlobalCombError|y[3]=BinByBinCombError|rebuild|>

<|{selected_scenario}|scenario_dag|>
---------------------------------------

## **Combination**{: .color-primary} and explorer of data nodes

#### Use the number selector to chose what you are looking at: 0=SF, 1=Stat, 2=Syst, 3=Total Error (all errors are multiplied by 10)

<|{level}|number|on_change=update_heatmap|>
<|Refresh Charts|button|on_action=update_heatmap|>
#### Ziso 
<|{level_to_view}|text|>
<|{ziso_SF}|chart|type=heatmap|z=0/SFStatSyst|x=1/pT|y=0/eta|height=90vh|options={options}|layout={layout_ziso}|>
#### Zmass <|{level_to_view}|>
<|{zmass_SF}|chart|type=heatmap|z=0/SFStatSyst|x=1/pT|y=0/eta|height=90vh|options={options}|layout={layout_zmass}|>
#### JPsi <|{level_to_view}|>
<|{jpsi_SF}|chart|type=heatmap|z=0/SFStatSyst|x=1/pT|y=0/eta|height=90vh|options={options}|layout={layout_jpsi}|>
<|Data Nodes|expandable|
<|1 5|layout|
<|{selected_data_node}|data_node_selector|> 

<|{selected_data_node}|data_node|>
|>
|>

|scenario>
|>