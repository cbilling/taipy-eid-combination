# Systematics Analysis
### Zmethod Systematics 
<|{scenario}|scenario_selector|show_add_button=False|on_change=set_scenario|>
<|{syst_level}|number|on_change=update_heatmap|>
<|layout|columns=1 1|gap=50px|
<left|
#### Ziso before 
<|{zisoSysBef}|chart|type=heatmap|z=0/SFStatSyst|x=1/pT|y=0/eta|height=40vh|options={options}|layout={layout_zisoSysBef}|>
#### Zmass before
<|{zmassSysBef}|chart|type=heatmap|z=0/SFStatSyst|x=1/pT|y=0/eta|height=40vh|options={options}|layout={layout_zmassSysBef}|>
|left>

<right|
#### Ziso After
<|{zisoSysAft}|chart|type=heatmap|z=0/SFStatSyst|x=1/pT|y=0/eta|height=40vh|options={options}|layout={layout_zisoSysAft}|>
#### Zmass after
<|{zmassSysAft}|chart|type=heatmap|z=0/SFStatSyst|x=1/pT|y=0/eta|height=40vh|options={options}|layout={layout_zmassSysAft}|>
|right>
|>


### Statistics 
<|layout|columns=1 1|gap=50px|
<left|
#### Ziso before 
<|{zisoStatBef}|chart|type=heatmap|z=0/SFStatSyst|x=1/pT|y=0/eta|height=40vh|options={options}|layout={layout_zisoStatBef}|>
#### Zmass before
<|{zmassStatBef}|chart|type=heatmap|z=0/SFStatSyst|x=1/pT|y=0/eta|height=40vh|options={options}|layout={layout_zmassStatBef}|>
|left>

<right|
#### Ziso After
<|{zisoStatAft}|chart|type=heatmap|z=0/SFStatSyst|x=1/pT|y=0/eta|height=40vh|options={options}|layout={layout_zisoStatAft}|>
#### Zmass after
<|{zmassStatAft}|chart|type=heatmap|z=0/SFStatSyst|x=1/pT|y=0/eta|height=40vh|options={options}|layout={layout_zmassStatAft}|>
|right>
|>

## Scalefactors repeating 
<|{repeating_data}|chart|type=scatter|mode[4]=lines|mode[5]=lines|mode[6]=lines|mode[1]=markers|mode[2]=markers|mode[3]=markers|line[4]={line}|line[5]={line}|line[6]={line}|x=pT|y[4]=TotalComb|y[5]=GlobalComb|y[6]=BinByBinComb|y[1]=Jpsi|y[2]=Ziso|y[3]=Zmass|options[1]={options1}|options[2]={options2}|options[3]={options3}|color[1]=red|color[2]=orange|color[3]=blue|color[4]=black|marker[1]={marker}|marker[2]={marker}|marker[3]={marker}|rebuild|>
#### Difference with respect to TotalComb
<|{repeating_ratio}|chart|type=scatter|mode[4]=lines|mode[5]=lines|mode[6]=lines|mode[1]=markers|mode[2]=markers|mode[3]=markers|line[4]={line}|line[5]={line}|line[6]={line}|x=pT|y[4]=TotalComb|y[5]=GlobalComb|y[6]=BinByBinComb|y[1]=Jpsi|y[2]=Ziso|y[3]=Zmass|options[1]={options1}|options[2]={options2}|options[3]={options3}|color[1]=red|color[2]=orange|color[3]=blue|color[4]=black|marker[1]={marker}|marker[2]={marker}|marker[3]={marker}|>

## Combination Errors repeating 
<|{repeating_error}|chart|type=lines|mode[1]=lines|mode[2]=lines|mode[3]=lines|line[1]={line}|line[2]={line}|line[3]={line}|x=pT|y[1]=TotalCombError|y[2]=GlobalCombError|y[3]=BinByBinCombError|rebuild|>
#### Difference with respect to TotalComb
<|{repeating_err_ratio}|chart|type=lines|mode[1]=lines|mode[2]=lines|mode[3]=lines|line[1]={line}|line[2]={line}|line[3]={line}|x=pT|y[1]=TotalCombError|y[2]=GlobalCombError|y[3]=BinByBinCombError|rebuild|>
