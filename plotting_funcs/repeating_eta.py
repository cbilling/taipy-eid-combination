import numpy as np
import os
import pandas as pd
import math

working_dir = os.environ["WORKING_DIR"]


def get_variance(arr_unc):
    # usually we work with arrays, where source of uncertainties are stored in columns and are fully correlated between rows
    # sometimes it's helpful to get just the variance (i.e. quadratic sum of columns)
    return np.sum(arr_unc**2, axis=1)


def get_standarddev(arr_unc):
    # same as variance, but holding the standard deviation
    return np.sqrt(get_variance(arr_unc))


def make_repeating_eta_plot(scenario):

    # First we need to set the length of x axis
    # To make it consistent, we will do # of pT bins
    # multiplied by the # of eta bins in Z
    x = list(range(14 * 20))
    eta_repeated = list(scenario.Zmass_y.read()) * 14  # 14 pT bins
    jpsi = scenario.Jpsi.read()

    ziso = scenario.Ziso.read()
    zmass = scenario.Zmass.read()
    jpsi_errors = get_standarddev(np.array(jpsi)[:, 1:]).tolist()
    ziso_errors = [0] * 3 * 20
    zmass_errors = [0] * 3 * 20
    ziso_errors.extend(get_standarddev(np.array(ziso)[:, 1:]).tolist())
    zmass_errors.extend(get_standarddev(np.array(zmass)[:, 1:]).tolist())
    jzcomb = scenario.ZJpsicomb.read()
    zcomblo = scenario.Zcomblo.read()
    zcombhi = scenario.Zcombhi.read()
    zcomblo_sf = list(zcomblo[:, 0])
    zcombhi_sf = list(zcombhi[:, 0])
    zcomb = [np.nan] * 3 * 20
    zcomb.extend(zcomblo_sf)
    zcomb.extend(zcombhi_sf)

    # get Ruggeros data
    ## Give it zeros for Jpsi data
    globalComb = [np.nan] * 3 * 20
    binBinComb = [np.nan] * 3 * 20
    globalComb_readin = pd.read_csv(
        working_dir + "/combination_inputs/ruggero/global.csv"
    )["value"].values
    binBinComb_readin = pd.read_csv(
        working_dir + "/combination_inputs/ruggero/bin_by_bin.csv"
    )["value"].values
    globalComb.extend(globalComb_readin)
    binBinComb.extend(binBinComb_readin)
    ## Give it zeros for highest pT bin as well

    eta_jpsi = list(scenario.Jpsi_y.read()) * len(scenario.Jpsi_x.read())
    jpsi_sf_remapped = []
    jpsi_err_remapped = []
    found_index = 0
    for i, eta in enumerate(eta_repeated):
        if found_index < len(eta_jpsi):
            if eta == eta_jpsi[found_index]:
                jpsi_sf_remapped.append(jpsi[found_index, 0])
                jpsi_err_remapped.append(jpsi_errors[found_index])
                zcomb[i] = jzcomb[found_index, 0]
                found_index += 1
            else:
                jpsi_sf_remapped.append(np.nan)
                jpsi_err_remapped.append(0)
        else:
            jpsi_sf_remapped.append(np.nan)
            jpsi_err_remapped.append(0)

    jpsi_errors = jpsi_err_remapped
    ziso_remapped = [np.nan] * 3 * 20
    ziso_remapped.extend(ziso[:, 0])
    zmass_remapped = [np.nan] * 3 * 20
    zmass_remapped.extend(zmass[:, 0])

    data = {
        "pT": x,
        "TotalComb": zcomb,
        "GlobalComb": globalComb,
        "BinByBinComb": binBinComb,
        "Jpsi": jpsi_sf_remapped,
        "Ziso": ziso_remapped,
        "Zmass": zmass_remapped,
    }
    errors1 = {"error_y": {"type": "data", "array": jpsi_errors}}
    errors2 = {"error_y": {"type": "data", "array": ziso_errors}}
    errors3 = {"error_y": {"type": "data", "array": zmass_errors}}
    return data, errors1, errors2, errors3


def make_repeating_ratio_plot(scenario):
    data, errors1, errors2, errors3 = make_repeating_eta_plot(scenario)

    x = data["pT"]
    zcomb = data["TotalComb"]
    globalcomb = data["GlobalComb"]
    binBincomb = data["BinByBinComb"]
    jpsi = data["Jpsi"]
    ziso = data["Ziso"]
    zmass = data["Zmass"]

    globalcomb_ratio = []
    binBincomb_ratio = []
    jpsi_ratio = []
    ziso_ratio = []
    zmass_ratio = []
    zcomb_ratio = []
    for i in range(len(data["TotalComb"])):
        globalcomb_ratio.append(float(globalcomb[i]) - float(zcomb[i]))
        binBincomb_ratio.append(float(binBincomb[i]) - float(zcomb[i]))
        jpsi_ratio.append(float(jpsi[i]) - float(zcomb[i]))
        ziso_ratio.append(float(ziso[i]) - float(zcomb[i]))
        zmass_ratio.append(float(zmass[i]) - float(zcomb[i]))
        zcomb_ratio.append(float(zcomb[i]) - float(zcomb[i]))
    data = {
        "pT": x,
        "TotalComb": zcomb_ratio,
        "GlobalComb": globalcomb_ratio,
        "BinByBinComb": binBincomb_ratio,
        "Jpsi": jpsi_ratio,
        "Ziso": ziso_ratio,
        "Zmass": zmass_ratio,
    }
    return data, errors1, errors2, errors3


def make_repeating_eta_err(scenario):

    # First we need to set the length of x axis
    # To make it consistent, we will do # of pT bins
    # multiplied by the # of eta bins in Z
    x = list(range(14 * 20))
    eta_repeated = list(scenario.Zmass_y.read()) * 14  # 14 pT bins
    jzcomb = list(get_standarddev(scenario.ZJpsicomb.read()[:, 1:]))
    zcomblo = scenario.Zcomblo.read()
    zcombhi = scenario.Zcombhi.read()
    zcomblo_sf = list(get_standarddev(zcomblo[:, 1:]))
    zcombhi_sf = list(get_standarddev(zcombhi[:, 1:]))
    zcomb = [np.nan] * 3 * 20
    zcomb.extend(zcomblo_sf)
    zcomb.extend(zcombhi_sf)

    # get Ruggeros data
    ## Give it zeros for Jpsi data
    globalComb = [np.nan] * 3 * 20
    binBinComb = [np.nan] * 3 * 20
    globalComb_readin = pd.read_csv(
        working_dir + "/combination_inputs/ruggero/global.csv"
    )["error"].values
    binBinComb_readin = pd.read_csv(
        working_dir + "/combination_inputs/ruggero/bin_by_bin.csv"
    )["error"].values
    globalComb.extend(globalComb_readin)
    binBinComb.extend(binBinComb_readin)
    ## Give it zeros for highest pT bin as well

    eta_jpsi = list(scenario.Jpsi_y.read()) * len(scenario.Jpsi_x.read())

    found_index = 0
    for i, eta in enumerate(eta_repeated):
        if found_index < len(eta_jpsi):
            if eta == eta_jpsi[found_index]:
                zcomb[i] = jzcomb[found_index]
                found_index += 1

    # dont want to include the error from Jpsi section
    data = {
        "pT": x[60:],
        "TotalCombError": zcomb[60:],
        "GlobalCombError": globalComb[60:],
        "BinByBinCombError": binBinComb[60:],
    }
    return data


def make_repeating_eta_err_ratio(scenario):

    data = make_repeating_eta_err(scenario)

    zcomb = data["TotalCombError"]
    globalComb = data["GlobalCombError"]
    binBinComb = data["BinByBinCombError"]

    zcomb_ratio = []
    globalComb_ratio = []
    binBinComb_ratio = []
    for i, z in enumerate(zcomb):
        zcomb_ratio.append(0)
        globalComb_ratio.append(globalComb[i] - z)
        binBinComb_ratio.append(binBinComb[i] - z)

    # dont want to include the error from Jpsi section
    data = {
        "pT": data["pT"],
        "TotalCombError": zcomb_ratio,
        "GlobalCombError": globalComb_ratio,
        "BinByBinCombError": binBinComb_ratio,
    }
    return data
