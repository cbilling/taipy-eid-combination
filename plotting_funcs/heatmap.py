import numpy as np


def get_variance(arr_unc):
    # usually we work with arrays, where source of uncertainties are stored in columns and are fully correlated between rows
    # sometimes it's helpful to get just the variance (i.e. quadratic sum of columns)
    return np.sum(arr_unc**2, axis=1)


def get_standarddev(arr_unc):
    # same as variance, but holding the standard deviation
    return np.sqrt(get_variance(arr_unc))


edgesx = [
    15000.0,
    20000.0,
    25000.0,
    30000.0,
    35000.0,
    40000.0,
    45000.0,
    50000.0,
    60000.0,
    80000.0,
    150000.0,
    200000.0,
]
centersx = []
for i in range(len(edgesx) - 1):
    centersx.append((edgesx[i] + edgesx[i + 1]) / 2)

edges = [
    -2.47,
    -2.37,
    -2.01,
    -1.81,
    -1.52,
    -1.37,
    -1.15,
    -0.8,
    -0.6,
    -0.1,
    0.0,
    0.1,
    0.6,
    0.8,
    1.15,
    1.37,
    1.52,
    1.81,
    2.01,
    2.37,
    2.47,
]
centersy = []
for i in range(len(edges) - 1):
    centersy.append((edges[i] + edges[i + 1]) / 2)


jedges = [0.0, 0.1, 0.8, 1.37, 1.52, 2.01, 2.37, 2.47]
jcentersy = []
for i in range(len(jedges) - 1):
    jcentersy.append((jedges[i] + jedges[i + 1]) / 2)

jedgesx = [
    4500,
    7000,
    10000,
    15000,
    20000,
]
jcentersx = []
for i in range(len(jedgesx) - 1):
    jcentersx.append((jedgesx[i] + jedgesx[i + 1]) / 2)


def heatmap_data_stat(arr, doSum=False):

    if doSum:
        arr = np.sum(arr, axis=1)

    ziso_data = [
        {
            "SFStatSyst": np.transpose(
                arr.reshape(
                    len(np.ravel(centersx).tolist()),
                    len(np.ravel(centersy).tolist()),
                )
            ).tolist(),
            "eta": centersy,
        },
        {"pT": centersx},
    ]
    annotations = []
    eta = centersy
    pt = centersx
    for eta_bin in range(len(eta)):
        for pt_bin in range(len(pt)):
            SF = ziso_data[0]["SFStatSyst"][eta_bin][pt_bin]
            # Create the annotation
            annotation = {
                # The name of the season
                "x": pt[pt_bin],
                # The name of the city
                "y": eta[eta_bin],
                # The temperature, as a formatted string
                "text": f"{round(SF*100,2)}",
                # Remove the annotation arrow
                "showarrow": False,
                "font": {"size": 8, "color": "black"},
            }
            # Add the annotation to the layout's annotations array
            annotations.append(annotation)

    return ziso_data, annotations


def heatmap_data_syst(arr, level):

    level = max(0, level)
    level = min(level, arr.shape[1] - 1)
    level = int(level)

    arr = arr[:, level]
    ziso_data = [
        {
            "SFStatSyst": np.transpose(
                arr.reshape(
                    len(np.ravel(centersx).tolist()),
                    len(np.ravel(centersy).tolist()),
                )
            ).tolist(),
            "eta": centersy,
        },
        {"pT": centersx},
    ]
    annotations = []
    eta = centersy
    pt = centersx
    for eta_bin in range(len(eta)):
        for pt_bin in range(len(pt)):
            SF = ziso_data[0]["SFStatSyst"][eta_bin][pt_bin]
            # Create the annotation
            annotation = {
                # The name of the season
                "x": pt[pt_bin],
                # The name of the city
                "y": eta[eta_bin],
                # The temperature, as a formatted string
                "text": f"{round(SF*100,2)}",
                # Remove the annotation arrow
                "showarrow": False,
                "font": {"size": 8, "color": "black"},
            }
            # Add the annotation to the layout's annotations array
            annotations.append(annotation)

    return ziso_data, annotations


def heatmap_data_ziso(scenario, level):

    level = max(0, level)
    level = int(level)
    # Syst
    if level == 2:
        sf = get_standarddev(scenario.Ziso.read()[:, level:]) * 10
    # Total error
    elif level >= 3:
        level = 1
        sf = get_standarddev(scenario.Ziso.read()[:, level:]) * 10
    # SF and Stat
    else:
        sf = scenario.Ziso.read()[:, level]
        if level != 0:
            sf = sf * 10
    ziso_data = [
        {
            "SFStatSyst": np.transpose(
                sf.reshape(
                    len(np.ravel(scenario.Ziso_x.read()).tolist()),
                    len(np.ravel(scenario.Ziso_y.read()).tolist()),
                )
            ).tolist(),
            "eta": centersy,
        },
        {"pT": centersx},
    ]
    annotations = []
    eta = centersy
    pt = centersx
    for eta_bin in range(len(eta)):
        for pt_bin in range(len(pt)):
            SF = ziso_data[0]["SFStatSyst"][eta_bin][pt_bin]
            # Create the annotation
            annotation = {
                # The name of the season
                "x": pt[pt_bin],
                # The name of the city
                "y": eta[eta_bin],
                # The temperature, as a formatted string
                "text": f"{round(SF*10,2)}",
                # Remove the annotation arrow
                "showarrow": False,
                "font": {"size": 12, "color": "black"},
            }
            # Add the annotation to the layout's annotations array
            annotations.append(annotation)

    return ziso_data, annotations


def heatmap_data_zmass(scenario, level):

    level = max(0, level)
    level = int(level)
    # Syst
    if level == 2:
        sf = get_standarddev(scenario.Zmass.read()[:, level:]) * 10
    # Total error
    elif level >= 3:
        level = 1
        sf = get_standarddev(scenario.Zmass.read()[:, level:]) * 10
    # SF and Stat
    else:
        sf = scenario.Zmass.read()[:, level]
        if level != 0:
            sf = sf * 10
    zmass_data = [
        {
            "SFStatSyst": np.transpose(
                sf.reshape(
                    len(np.ravel(scenario.Zmass_x.read()).tolist()),
                    len(np.ravel(scenario.Zmass_y.read()).tolist()),
                )
            ).tolist(),
            "eta": centersy,
        },
        {"pT": centersx},
    ]

    annotations = []
    eta = centersy
    pt = centersx
    for eta_bin in range(len(eta)):
        for pt_bin in range(len(pt)):
            SF = zmass_data[0]["SFStatSyst"][eta_bin][pt_bin]
            # Create the annotation
            annotation = {
                # The name of the season
                "x": pt[pt_bin],
                # The name of the city
                "y": eta[eta_bin],
                # The temperature, as a formatted string
                "text": f"{round(SF*10,2)}",
                # Remove the annotation arrow
                "showarrow": False,
                "font": {"size": 12, "color": "black"},
            }
            # Add the annotation to the layout's annotations array
            annotations.append(annotation)
    return zmass_data, annotations


def heatmap_data_jpsi(scenario, level):

    level = max(0, level)
    level = int(level)
    # Syst
    if level == 2:
        sf = get_standarddev(scenario.Jpsi.read()[:, level:]) * 10
    # Total error
    elif level >= 3:
        level = 1
        sf = get_standarddev(scenario.Jpsi.read()[:, level:]) * 10
    # SF and Stat
    else:
        sf = scenario.Jpsi.read()[:, level]
        if level != 0:
            sf = sf * 10
    # Doing shaping

    if len(jcentersy) > len(np.ravel(scenario.Jpsi_y.read()).tolist()):
        # we may have not had the extra bin, but the website has the extra bin
        # so we will just repeat the last bin
        print("appending")
        sf = np.transpose(
            sf.reshape(
                len(jcentersx),
                len(np.ravel(scenario.Jpsi_y.read()).tolist()),
            )
        )
        sf = np.append(sf, [sf[-1, :]], axis=0)
        sf = sf.tolist()
        print(sf)

    else:

        sf = np.transpose(
            sf.reshape(
                len(jcentersx),
                len(jcentersy),
            )
        ).tolist()

    jpsi_data = [
        {
            "SFStatSyst": sf,
            "eta": jcentersy,
        },
        {"pT": jcentersx},
    ]

    annotations = []
    eta = jcentersy
    pt = jcentersx
    for eta_bin in range(len(eta)):
        for pt_bin in range(len(pt)):
            SF = jpsi_data[0]["SFStatSyst"][eta_bin][pt_bin]
            # Create the annotation
            annotation = {
                # The name of the season
                "x": pt[pt_bin],
                # The name of the city
                "y": eta[eta_bin],
                # The temperature, as a formatted string
                "text": f"{round(SF*10,2)}",
                # Remove the annotation arrow
                "showarrow": False,
                "font": {"size": 15, "color": "black"},
            }
            # Add the annotation to the layout's annotations array
            annotations.append(annotation)
    return jpsi_data, annotations
